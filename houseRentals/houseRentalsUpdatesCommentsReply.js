const mongoose=require('mongoose');

const SchemaCUpdatesCommentsReply= new mongoose.Schema({
 
    descriptions:String,
   
    houseRentalsClientsForUpdatesCommentsReply:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'houseRentalsUpdatesComments'
}
    
},
{timestamps:true});
const   updatesCommentsReply=mongoose.model('houseRentalsUpdatesCommentsReply',SchemaCUpdatesCommentsReply);

module.exports=updatesCommentsReply;

