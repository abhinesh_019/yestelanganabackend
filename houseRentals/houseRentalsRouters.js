const express= require('express');
var router = express.Router();
const userControllers = require('../houseRentals/houseRentalsControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

 


//  ************ Locations ***********

router.route('/houseRentalsLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/houseRentalsAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/houseRentalsClientsget/:AreaId')
.get(userControllers.index)

router.route('/houseRentalsClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/houseRentalsClientsAllCount') 
.get(userControllers.totalclients)

router.route('/houseRentalsClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/houseRentalsClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/houseRentalsClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/houseRentalsfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/houseRentalsfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/houseRentalsfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************
// ******************* Clients Services ******************
router.route('/houseRentalsServicesGet/:clientId')
.get(userControllers.servicesget)
router.route('/houseRentalsServicesGetCounts/:clientId')
.get(userControllers.servicesgetCounts)
router.route('/houseRentalsServicesPost/:clientId')
.post(multipartMiddleware,userControllers.servicesPost);

// *******************************************************
// ****************** only updates details ****************
router.route('/houseRentalsupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/houseRentalsupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/houseRentalsupdatesgetpostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/houseRentalsupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/houseRentalsupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/houseRentalsupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/houseRentalsupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/houseRentalsupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/houseRentalseuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/houseRentalsCeuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)
module.exports = router;