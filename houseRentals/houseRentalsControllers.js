var locations = require('../houseRentals/houseRentalsLocations')
var areas = require('../houseRentals/houseRentalsAreas')
var clients = require('../houseRentals/houseRentalsClients')
 var clientsUpdates = require('../houseRentals/houseRentalsUpdates')
var clientsUpdatesComments = require('../houseRentals/houseRentalsUpdatesComments')
var clientsUpdatesCommentsReply = require('../houseRentals/houseRentalsUpdatesCommentsReply')
var clientsOnlyComments = require('../houseRentals/houseRentalsOnlyComments')
var fecilities = require('../houseRentals/houseRentalsFecilities')
var typeOfRealEstate = require('../houseRentals/houseRentalsTypes')
var typeOfRealEstat = require('../houseRentals/houseRentalsTypesDes')
const fileService = require('../houseRentals/houseRentalsServer');
  
var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 
var async = require('async');
  
module.exports = {

 
// ********************* Locations *********************

locationspost: async(req,res,next)=>{
    const locs=  req.body;
    await locations.create(locs, function (err, data) {
        if (err) {
            console.log('Error in Saving user: ' + err);
        } else {

            res.status(201).json({ status: true, message: "user added sucessfully"});
        }
    });
 },



locationsget: async(req,res,next)=>{
    const users = await locations.find({}).sort({"locations":1});
       res.status(200).json(users);
 },
// ******************************************

// *********************** Areas *******************

Areasposts : async(req,res,next)=>{
    const { LocationsId } = req.params;
    req.body.LocationForAreas = LocationsId
     const userscomm = new areas(req.body);
    await userscomm.save();
        res.status(201).json(userscomm);
},
 
Areasget:async (req, res, next) => {
     const { LocationsId } = req.params;
     const usersposts = await areas.find({LocationForAreas:LocationsId}).sort({"area":-1})
    res.status(200).json(usersposts);
 },

// ******************************************

// ************************************************ Clients Updates ***************************************
 
newUser: (req, res, next) => {
    const { AreaId } = req.params;
    req.body.areaForClients = AreaId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null,data.Location);
            })
        },
        function(callback) {
            fileService.uploadImage(req.files.images4, function (err, data) {
                allData.images4 = data.Location;
                callback(null, data.Location);
            })
        },
 
        function (callback) {
            fileService.uploadImage(req.files.images5, function (err, data) {
                allData.images5 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images6, function (err, data) {
                allData.images6 = data.Location;
                callback(null, data.Location);
            })
        },
         

    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clients.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                }
            });
        }
 });
},



index:async (req, res, next) => {

    const { AreaId } = req.params;
    let search=req.query.search;

    const usersposts = await clients.find({areaForClients:AreaId,$or:[
        {subArea:new RegExp(search, "gi")},
        {pincode:new RegExp(search, "gi")}, 
        {instName:new RegExp(search, "gi")},
        {danceCat:new RegExp(search, "gi")},
        {place:new RegExp(search, "gi")},
    ]
}).sort({"subArea":-1})
    res.status(200).json(usersposts);
 },

usersgetcoutarea: async (req, res, next) => {
    const { AreaId } = req.params;

    const users = await clients.aggregate([{$match:{areaForClients:ObjectId(AreaId)}},

        {"$group" : {_id:{subArea:"$subArea"}, count:{$sum:1}}},{$sort:{"subArea":1}}
     ])
    
    res.status(200).json(users);
 },
 
tuserscount:async (req, res, next) => {
     const { AreaId } = req.params;
     const usersposts = await clients.find({areaForClients:AreaId}).count()
    res.status(200).json(usersposts);
 },


totalclients:async (req, res, next) => {
  const usersposts = await clients.find({}).count()
    res.status(200).json(usersposts);
  
},
 
indexs:async (req, res, next) => {

    const { clientsId } = req.params;
    const usersposts = await clients.findById(clientsId) 
    res.status(200).json(usersposts);
  
},
 
// ***********************************************************************


// ************************************************ Clients fecilities ***************************************
 
fecilitiespost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.ClientsForFecilities = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images, function (err, data) {
                allData.images = data.Location;
                callback(null, data.Location);
            })
        },
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            fecilities.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
fecilitiesget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await fecilities.find({ClientsForFecilities:clientId}).sort({"importantsKey":-1})
    res.status(200).json(usersposts);
 },
 fecilitiesgetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await fecilities.find({ClientsForFecilities:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************



// ************************************************ Clients services ***************************************
 
servicesPost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.ClientsForServices = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images, function (err, data) {
                allData.images = data.Location;
                callback(null, data.Location);
            })
        },
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            services.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
servicesget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await services.find({ClientsForServices:clientId}).sort({"danceType":-1})
    res.status(200).json(usersposts);
 },
 servicesgetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await services.find({ClientsForServices:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************



// ****************users updates*************

UsersUpdates: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.UpdatedDetails = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.viedoes,"req.files.viedoes 302");
            if(req.files.viedoes){
                fileService.uploadImage(req.files.viedoes, function (err, data) {
                    allData.viedoes = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clientsUpdates.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

UsersGet:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({UpdatedDetails:ClientsId}).sort({"createdAt:":-1})

        res.status(200).json(users);
       console.log("users****", users);
},

UsersGetCounts:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({UpdatedDetails:ClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
//admin users updates comments
updatesCommentsposts:async (req, res, next) => {

const { updatesId } = req.params;
  req.body.updatesForComments = updatesId 
const userscomm = new clientsUpdatesComments(req.body);

await userscomm.save();
console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
 res.status(201).json(userscomm);
},

updatesCommentsGet:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({updatesForComments:updatesId})
    res.status(200).json(users); 
},
updatesCommentsGetcounts:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({updatesForComments:updatesId}).count()
    res.status(200).json(users);
},

updatesCommentsReplyposts:async (req, res, next) => {
const { updatesCommId } = req.params;
  req.body.updatesCommentsForReply = updatesCommId 
const userscomm = new clientsUpdatesCommentsReply(req.body);
await userscomm.save();
 res.status(201).json(userscomm);
},

updatesCommentsReplyGet:async (req, res, next) => {

const { updatesCommId } = req.params;

    const users = await clientsUpdatesCommentsReply.find({updatesCommentsForReply:updatesCommId})

    res.status(200).json(users);
   console.log("users**** 399", users);
},

// ****************only comments *******************
usersCommentsposts : async (req,res) => {
const { ClientsId } = req.params;
req.body.clientsForOnlyComments = ClientsId
const generalcomm = new clientsOnlyComments(req.body);
await generalcomm.save();
res.status(201).json(generalcomm);
},
usersCommentsget:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await clientsOnlyComments.find({clientsForOnlyComments:ClientsId})
res.status(200).json(users);   
},
usersCommentsgetCounts:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await clientsOnlyComments.find({clientsForOnlyComments:ClientsId}).count()
    res.status(200).json(users);   
    },

}