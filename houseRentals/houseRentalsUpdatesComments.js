const mongoose=require('mongoose');

const SchemaCUpdatesComments= new mongoose.Schema({
 
    descriptions:String,
   
    houseRentalsClientsForUpdatesComments:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'houseRentalsUpdates'
}
    
},
{timestamps:true});
const   updatesComments=mongoose.model('houseRentalsUpdatesComments',SchemaCUpdatesComments);

module.exports=updatesComments;

