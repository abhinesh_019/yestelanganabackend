const mongoose=require('mongoose');

const SchemaFecilities= new mongoose.Schema({

    name:{type:String},
    importantsKey:{type:String},
    descriptions:{type:String},
    images:{type:String},
 
    houseRentalsClientsForFecilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'houseRentalsClients'
}
    
},
{timestamps:true});
const  fecilities=mongoose.model('houseRentalsFecilities',SchemaFecilities);

module.exports=fecilities;

