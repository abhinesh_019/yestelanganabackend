const mongoose=require('mongoose');

const SchemaTypeDescriptions= new mongoose.Schema({

    caste:{type:String},
    sub:{type:String},
    subTypes:{type:String},
    relatedTocaste:{type:String},
    trackRecords:{type:String},
    descriptions:{type:String},

 
    houseRentalsClientsForTypeDescriptions:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'houseRentalsType'
}
    
},
{timestamps:true});
const   TypeDescriptions=mongoose.model('houseRentalsTypeDescriptions',SchemaTypeDescriptions);

module.exports=TypeDescriptions;

