const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    houseRentalsLocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'houseRentalsLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('houseRentalsAreas',areaSchema);
module.exports=areas;