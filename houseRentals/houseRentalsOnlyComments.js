const mongoose=require('mongoose');

const SchemaCommenlsOnly= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,
 
    houseRentalsClientsForCommenlsOnly:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'houseRentalsClients'
}
    
},
{timestamps:true});
const   commenlsOnly=mongoose.model('houseRentalsOnlyComments',SchemaCommenlsOnly);

module.exports=commenlsOnly;

