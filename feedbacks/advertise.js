const mongoose=require('mongoose');
const locationSchema= new mongoose.Schema({

    companyName:String,
    firstName:String,
    lastName:String,
    city:String,
    mobileNo:String,
    landLineNo:String,
},
{timestamps:true});
const locations=mongoose.model('advertises',locationSchema);
module.exports=locations;