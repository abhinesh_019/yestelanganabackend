const express= require('express');
var router = express.Router();
const userControllers = require('../feedbacks/feedbackyesController');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ adverises ***********

router.route('/advertiseOnly')
.get(userControllers.advertisekOnlyget)
 .post(userControllers.advertiseOnlypost);

 router.route('/advertiseOnlyc')
.get(userControllers.advertisekOnlygetc)
 //  ***********************

//  ************ feedbacks ***********

router.route('/feedbackOnly')
.get(userControllers.feedbackOnlyget)
 .post(userControllers.feedbackOnlypost);

 router.route('/feedbackOnlyc')
.get(userControllers.feedbackOnlygetc)
 //  ***********************

 //  ************ advertiseOne ***********
router.route('/advertiseOne')
.get(userControllers.advertiseOneget)
router.route('/advertiseOnep')
 .post(multipartMiddleware,userControllers.advertiseOneposts);
 
 router.route('/advertiseOned/:advertiseOne')
.delete(userControllers.advertiseOned)
//  ***********************
 //  ************ advertiseTwo ***********
 router.route('/advertiseTwo')
 .get(userControllers.advertiseTwoget)
 router.route('/advertiseTwop')
  .post(multipartMiddleware,userControllers.advertiseTwoposts);
  router.route('/advertisetwod/:advertiseOne')
  .delete(userControllers.advertisetwod)
 //  ***********************
  //  ************ advertiseThree ***********
router.route('/advertiseThree')
.get(userControllers.advertisethreeget)
router.route('/advertiseThreep')
 .post(multipartMiddleware,userControllers.advertiseThreeposts);
 router.route('/advertisethreed/:advertiseOne')
 .delete(userControllers.advertisethreed)
//  ***********************
 //  ************ advertisefour ***********
 router.route('/advertiseFour')
 .get(userControllers.advertiseFourget)
 router.route('/advertiseFourp')
  .post(multipartMiddleware,userControllers.advertiseFourposts);
  router.route('/advertisefourd/:advertiseOne')
  .delete(userControllers.advertisefourd)
 //  ***********************
  //  ************ advertiseFive ***********
router.route('/advertiseFive')
.get(userControllers.advertiseFiveget)
router.route('/advertiseFivep')
 .post(multipartMiddleware,userControllers.advertiseFiveposts);
 router.route('/advertisefived/:advertiseOne')
 .delete(userControllers.advertisefived)
//  ***********************
 //  ************ advertiseSix ***********
 router.route('/advertiseSix')
 .get(userControllers.advertiseSixget)
 router.route('/advertiseSixp')
  .post(multipartMiddleware,userControllers.advertiseSixposts);
  router.route('/advertiseSixd/:advertiseOne')
  .delete(userControllers.advertiseSixd)
 //  ***********************
  //  ************ advertiseSeven ***********
router.route('/advertiseSeven')
.get(userControllers.advertiseSevenget)
router.route('/advertiseSevenp')
 .post(multipartMiddleware,userControllers.advertiseSevenposts);
 router.route('/advertiseSevend/:advertiseSeven')
 .delete(userControllers.advertiseSevend)
//  ***********************
 //  ************ advertiseeight ***********
 router.route('/advertiseEight')
 .get(userControllers.advertiseEightget)
 router.route('/advertiseEightp')
  .post(multipartMiddleware,userControllers.advertiseEightposts);
  router.route('/advertiseeightd/:advertiseOne')
  .delete(userControllers.advertiseeightd)
 //  ***********************
  //  ************ advertisenine ***********
router.route('/advertiseNine')
.get(userControllers.advertiseNineget)
router.route('/advertiseNinep')
 .post(multipartMiddleware,userControllers.advertiseNineposts);
 router.route('/advertisenined/:advertiseOne')
 .delete(userControllers.advertisenined)
//  ***********************
 //  ************ advertiseten ***********
 router.route('/advertiseTen')
 .get(userControllers.advertiseTenget)
 router.route('/advertiseTenp')
  .post(multipartMiddleware,userControllers.advertiseTenposts);
  router.route('/advertisetend/:advertiseOne')
  .delete(userControllers.advertisetend)
 //  ***********************

module.exports = router;
