const mongoose=require('mongoose');
 
const babycaresSchema= new mongoose.Schema(
    {
        addsTwoLinkLoc:{type:String},
        addsTwoImgLoc:{type:String},   
           babycaresAreaForAddsLocTwo:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'babycarelocations'
                     }
 },

{timestamps:true}

);

const babycares=mongoose.model('babycaresAddsLocTwo',babycaresSchema);
module.exports=babycares;