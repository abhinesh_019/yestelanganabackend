const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    information:String,
    descriptions:String,
    images:String,

    babyCareServicesTwo:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'babycareServices'
    }
},
{timestamps:true});
const comm=mongoose.model('babycareServicesTwo',commentsSchema);
module.exports=comm;