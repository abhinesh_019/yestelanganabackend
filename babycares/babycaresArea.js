const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    babycaresLocation:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'babycarelocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('babycaresarea',areaSchema);
module.exports=areas;