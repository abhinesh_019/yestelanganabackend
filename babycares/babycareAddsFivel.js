const mongoose=require('mongoose');
 
const babycaresSchema= new mongoose.Schema(
    {
        addsFiveLinkLoc:{type:String},
        addsFiveImgLoc:{type:String},   
           babycaresAreaForAddsLocFive:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'babycarelocations'
                     }
 },

{timestamps:true}

);

const babycares=mongoose.model('babycaresAddsLocFive',babycaresSchema);
module.exports=babycares;