const mongoose=require('mongoose');
const babycareupdatesSchema= new mongoose.Schema({
    images:String,
    title:String,
    description:String,
    viedoes:String,
    babyCareCenterUpdatedDetails:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'babycares'
    }
},{timestamps:true});
const bcupdates=mongoose.model('babycareupdates',babycareupdatesSchema);
module.exports=bcupdates;