const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,

    babyCareCenterDetails:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'babycares'
    }
},
{timestamps:true});
const comm=mongoose.model('babycarecomments',commentsSchema);
module.exports=comm;