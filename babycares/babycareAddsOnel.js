const mongoose=require('mongoose');
 
const babycaresSchema= new mongoose.Schema(
    {
        addsOneLinkLoc:{type:String},
        addsOneImgLoc:{type:String},   
           babycaresAreaForAddsLocOne:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'babycarelocations'
                     }
 },

{timestamps:true}

);

const babycares=mongoose.model('babycaresAddsLocOne',babycaresSchema);
module.exports=babycares;