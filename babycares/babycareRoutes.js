const express= require('express');
var router = express.Router();
const userControllers = require('../babycares/babycarecontroller');
 
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
// **************************************    locations **********************************************************

router.route('/babycarelocations')
.get(userControllers.bclocationsget)
.post(multipartMiddleware,userControllers.bclocationsposts);
 
// **************************************    areas **********************************************************


router.route('/babycareArea/:babycareslocationsId')
.get(userControllers.bcareaget)
router.route('/babycareArea/:babycareslocations')
.post(multipartMiddleware,userControllers.bcareasposts);

router.route('/babycare/:babyAreaId')
.get(userControllers.index)
router.route('/babycareCounts/:babyAreaId')
.get(userControllers.indexCounts)
router.route('/babycares/:babyAreaId')
.post(multipartMiddleware,userControllers.newUser);



router.route('/babycares') // grouping counts .....
.get(userControllers.babycare)

router.route('/babycaredist')
.get(userControllers.babycares)

router.route('/babycares/:babycaresId')
.get(userControllers.getUser)
 
router.route('/babycaresLocationCounts')
.get(userControllers.getUserLocations)


// ****** users  only comments***********
router.route('/babycarecomments/:babycaresId')
.get(userControllers.getUserComments)
.post(userControllers.newcomments)
 
// ****** services***********
 router.route('/babyCareServices/:babyCareClientsId')
.get(userControllers.babyCareTypesGet)
.post(userControllers.babyCareTypesPost);

 
// ******only admin updates***********
router.route('/babyCareupdates/:babycaresId')
.get(userControllers.getupdates) //done with angular completed

router.route('/babyCareupdatespost/:babycaresId')
.post(multipartMiddleware,userControllers.postupdates) //done
 // edit and delete pending
 
router.route('/babyCareupdatescounts/:babycaresId')
.get(userControllers.getupdatescount) //done with angular completed



// ******only users comments***********
router.route('/babyCareupdatesuserscomments/:babycaresId')
.get(userControllers.getupdatescomments)  //done with angular completed
.post(userControllers.postupdatescomments)  //done


// ******only users comments***********
router.route('/babyCareupdatesuserscommentscount/:babycaresId')
.get(userControllers.getupdatescommentscounts)  //done with angular completed
 

// ******only users comments***********
router.route('/babycareupdatesuserscommentsreply/:babycaresId')
.get(userControllers.getupdatescommentsreply)  //done with angular completed
.post(userControllers.postupdatescommentsreply)  //done


// ******************* ServcesTwo******************
router.route('/babyCareServicesTwoGet/:clientId')
.get(userControllers.servicesTwoget)
router.route('/babyCareServicesTwoGetCounts/:clientId')
.get(userControllers.servicesTwogetCounts)
router.route('/babyCareServicesTwoPost/:clientId')
.post(multipartMiddleware,userControllers.servicesTwoPost);

// ******************* Clients Fecilities ******************
router.route('/babyCarefecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/babyCarefecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/babyCarefecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);
// *************************************** AddsOne ************************************* 
router.route('/babycaresAddsOneGeta/:babycaresAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/babycaresAddsOneDeletea/:babycaresAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/babycaresAddsOnePtsa/:babycareAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/babycaresAddsTwoGeta/:babycaresAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/babycaresAddsTwoDeletea/:babycaresAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/babycaresAddsTwoPtsa/:babycaresAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/babycaresAddsThreeGeta/:babycaresAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/babycaresAddsThreeDeletea/:babycaresAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/babycaresAddsThreePtsa/:babycaresAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/babycaresAddsFourGeta/:babycaresAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/babycaresAddsFourDeletea/:babycaresAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/babycaresAddsFourPtsa/:babycaresAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 

// *************************************** AddsOne ************************************* 
router.route('/babycaresAddsOneLocGet/:babycaresAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/babycaresAddsOneLocDelete/:babycaresAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/babycaresAddsOneLocPts/:babycaresAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/babycaresAddsTwoLocGet/:babycaresAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/babycaresAddsTwoLocDelete/:babycaresAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/babycaresAddsTwoLocPts/:babycaresAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/babycaresAddsThreeGetLoc/:babycaresAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/babycaresAddsThreeDeleteLoc/:babycaresAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/babycaresAddsThreePtsLoc/:babycaresAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/babycaresAddsFourLocGet/:babycaresAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/babycaresAddsFourLocDelete/:babycaresAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/babycaresAddsFourLocPts/:babycaresAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/babycaresAddsFiveLocGet/:babycaresAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/babycaresAddsFiveLocDelete/:babycaresAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/babycaresAddsFiveLocPts/:babycaresAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 
module.exports = router;