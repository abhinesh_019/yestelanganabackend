var locations = require('../mobileSalesServices/mobileSalesServicesLocations')
var areas = require('../mobileSalesServices/mobileSalesServicesArea')
var clients = require('../mobileSalesServices/mobileSalesServicesClient')
//  var clientsUpdates = require('../mobileSalesServices/fireWorksUpdates')
// var clientsUpdatesComments = require('../mobileSalesServices/fireWorksUpdatesComments')
// var clientsUpdatesCommentsReply = require('../mobileSalesServices/fireWorksUpdatesCommentsReply')
// var clientsOnlyComments = require('../mobileSalesServices/fireWorksOnlyComments')
//  var serviceType = require('../mobileSalesServices/fireWorksServicesCatageirsOne')
// var serviceTypeTwo = require('../mobileSalesServices/fireWorksServicesCatageirsTwoTypes')
// var serviceCname = require('../mobileSalesServices/fireWorksServicesNoOfCustomers')
const fileService = require('../mobileSalesServices/mobileSalesServicesFileServer');
var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
var async = require('async');
  
 
module.exports = {

    // ********************* Locations *********************
    
    locationspost: async(req,res,next)=>{
        const locs=  req.body;
        await locations.create(locs, function (err, data) {
            if (err) {
                console.log('Error in Saving user: ' + err);
            } else {
    
                res.status(201).json({ status: true, message: "user added sucessfully"});
            }
        });
     },
    
    
    
    locationsget: async(req,res,next)=>{
        const users = await locations.find({}).sort({"locations":1});
           res.status(200).json(users);
     },
    // ******************************************
    
    // *********************** Areas *******************
    
    Areasposts : async(req,res,next)=>{
        const { LocationsId } = req.params;
        req.body.LocationForAreas = LocationsId
         const userscomm = new areas(req.body);
        await userscomm.save();
            res.status(201).json(userscomm);
    },
     
    Areasget:async (req, res, next) => {
         const { LocationsId } = req.params;
         const usersposts = await areas.find({LocationForAreas:LocationsId}).sort({"area":-1})
        res.status(200).json(usersposts);
     },
    
    // ******************************************
    // ************************************************ Clients Updates ***************************************
     
    newUser: (req, res, next) => {
    
            
        const { AreaId } = req.params;
        req.body.areaForClients = AreaId
        let allData = req.body
     
        async.parallel([
            function (callback) { 
                fileService.uploadImage(req.files.images1, function (err, data) {
                    allData.images1 = data.Location;
                    callback(null, data.Location);
                })
            },
    
            function(callback) {
                fileService.uploadImage(req.files.images2, function (err, data) {
                    allData.images2 = data.Location;
                    callback(null, data.Location);
                })
            },
    
            function(callback) {
                fileService.uploadImage(req.files.images3, function (err, data) {
                    allData.images3 = data.Location;
                    callback(null,data.Location);
                })
            },
            function(callback) {
                fileService.uploadImage(req.files.images4, function (err, data) {
                    allData.images4 = data.Location;
                    callback(null, data.Location);
                })
            },
     
            function (callback) {
                fileService.uploadImage(req.files.images5, function (err, data) {
                    allData.images5 = data.Location;
                    callback(null, data.Location);
                })
            },
            function (callback) {
                fileService.uploadImage(req.files.images6, function (err, data) {
                    allData.images6 = data.Location;
                    callback(null, data.Location);
                })
            },
             
    
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                clients.create(allData, function (err, data) {
                if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                         res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                    }
                });
            }
     });
    },
    
    
    
    index:async (req, res, next) => {
    
        const { AreaId } = req.params;
        let search=req.query.search;
    
        const usersposts = await clients.find({areaForClients:AreaId,$or:[
            {subArea:new RegExp(search, "gi")},
            {pincode:new RegExp(search, "gi")}, 
            {instName:new RegExp(search, "gi")},
            {indoorCat:new RegExp(search, "gi")},
            {place:new RegExp(search, "gi")},
        ]
    }).sort({"subArea":-1})
        res.status(200).json(usersposts);
     },
    
    usersgetcoutarea: async (req, res, next) => {
        const { AreaId } = req.params;
    
        const users = await clients.aggregate([{$match:{areaForClients:ObjectId(AreaId)}},
    
            {"$group" : {_id:{subArea:"$subArea"}, count:{$sum:1}}},{$sort:{"subArea":1}}
         ])
        
        res.status(200).json(users);
     },
     
    tuserscount:async (req, res, next) => {
         const { AreaId } = req.params;
         const usersposts = await clients.find({areaForClients:AreaId}).count()
        res.status(200).json(usersposts);
     },
    
    
    totalclients:async (req, res, next) => {
      const usersposts = await clients.find({}).count()
        res.status(200).json(usersposts);
      
    },
     
    indexs:async (req, res, next) => {
    
        const { clientsId } = req.params;
        const usersposts = await clients.findById(clientsId) 
        res.status(200).json(usersposts);
      
    },
    tuserscountAreas: async (req, res, next) => {
        const { AreaId } = req.params;
    
        const users = await clients.aggregate([{$match:{areaForClients:ObjectId(AreaId)}},
    
            {"$group" : {_id:{mainArea:"$mainArea"}, count:{$sum:1}}},{$sort:{"mainArea":1}}
         ])
        
        res.status(200).json(users);
     },
    
     tuserscountDist: async (req, res, next) => {
         
    
        const users = await clients.aggregate([
    
            {"$group" : {_id:{distict:"$distict"}, count:{$sum:1}}},{$sort:{"distict":1}}
         ])
        
        res.status(200).json(users);
     },
    // ***********************************************************************
 
}