const express= require('express');
var router = express.Router();
const userControllers = require('../mobileSalesServices/mobileSalesServicesController');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ Locations ***********

router.route('/mobileServicesLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/mobileServicesareas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/mobileServicesClientsget/:AreaId')
.get(userControllers.index)

router.route('/mobileServicesClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/mobileServicesClientsAllCount') 
.get(userControllers.totalclients)


router.route('/mobileServicesClientsCountAreas/:AreaId')  
.get(userControllers.tuserscountAreas)


router.route('/mobileServicesClientsCountDist')  
.get(userControllers.tuserscountDist)


router.route('/mobileServicesClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/mobileServicesClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/mobileServicesClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************
  
module.exports = router;