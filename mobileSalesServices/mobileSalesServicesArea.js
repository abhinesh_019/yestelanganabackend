const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'mobileSalesServicesLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('mobileSalesServicesArea',areaSchema);
module.exports=areas;