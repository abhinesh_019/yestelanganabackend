const express= require('express');
var router = express.Router();
const userControllers = require('../fireWorks/fireWorksControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ Locations ***********

router.route('/fireworksLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/fireworksareas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);
 router.route('/fireworksClientsCountAreas/:AreaId')  
 .get(userControllers.tuserscountAreas)
//  ***********************

// ******************* Clients details ******************
router.route('/fireworksClientsget/:AreaId')
.get(userControllers.index)

router.route('/fireworksClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/fireworksClientsAllCount') 
.get(userControllers.totalclients)





router.route('/fireworksClientsCountDist')  
.get(userControllers.tuserscountDist)


router.route('/fireworksClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/fireworksClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/fireworksClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************
 

// ****************** only updates details ****************
router.route('/fireworksupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/fireworksupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/fireworksupdatespostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/fireworksupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/fireworksupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/fireworksupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/fireworksupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/fireworksupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/fireworksUsersComments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/fireworksUsersCommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

 // ******************* Clients Services services one ******************
router.route('/fireworksServicesGet/:clientId')
.get(userControllers.servicesget)
router.route('/fireworksServicesGetCounts/:clientId')
.get(userControllers.servicesgetCounts)
router.route('/fireworksServicesPost/:clientId')
.post(multipartMiddleware,userControllers.servicesPost);

// *******************************************************
 // ******************* Clients Services Two ******************
 router.route('/fireworksServicesTwo/:clientId')
 .get(userControllers.servicesTwosget)
 router.route('/fireworksServicesTwoCounts/:clientId')
 .get(userControllers.servicesgetTwoCounts)
 router.route('/fireworksServicesTwoPost/:clientId')
 .post(multipartMiddleware,userControllers.servicesTwoPost);
 
 // *******************************************************
 // ******************* Clients Services for No of customers ******************
 router.route('/fireworkServicesCustomers/:clientId')
 .get(userControllers.servicesNoOfCustomersget)
 router.route('/fireworkServicesCustomersCounts/:clientId')
 .get(userControllers.servicesNoOfCustomersgetCounts)
 router.route('/fireworkServicesCustomersPost/:clientId')
 .post(multipartMiddleware,userControllers.servicesNoOfCustomersPost);
 
 // *******************************************************
 

// *************************************** AddsOne ************************************* 
router.route('/fireworksAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/fireworksAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/fireworksAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/fireworksAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/fireworksAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/fireworksAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/fireworksAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/fireworksAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/fireworksAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/fireworksAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/fireworksAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/fireworksAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/fireworksAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/fireworksAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/fireworksAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/fireworksAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/fireworksAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/fireworksAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/fireworksAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/fireworksAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/fireworksAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/fireworksAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/fireworksAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/fireworksAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/fireworksAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/fireworksAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/fireworksAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
