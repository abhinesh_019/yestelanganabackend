const mongoose=require('mongoose');

const SchemaThree= new mongoose.Schema({
    
     customerName:{type:String},
      place:{type:String},
     itemsDescriptions:{type:String},
     itemsImg:{type:String},
     customersImg:{type:String},
 
    servicesThreeForNoOfCustomers:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'fireWorksServiceTwoTypes'
}
    
},
{timestamps:true});
const serviceServicesThree=mongoose.model('fireworksNoOfCustomers',SchemaThree);

module.exports=serviceServicesThree;

