const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'fireWorksLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('fireWorksArea',areaSchema);
module.exports=areas;