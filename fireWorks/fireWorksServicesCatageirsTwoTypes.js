const mongoose=require('mongoose');

const SchemaVehicle= new mongoose.Schema({
    
    nameOfFireWorks:{type:String},
     fireWorksDescriptions:{type:String},
    fireWorksPrice:{type:String},
    fireWorksImg:{type:String},
 
 
    serviceOneForServicesTwo:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'fireworksServiceCatOne'
}
    
},
{timestamps:true});
const serviceVehicle=mongoose.model('fireWorksServiceTwoTypes',SchemaVehicle);

module.exports=serviceVehicle;

