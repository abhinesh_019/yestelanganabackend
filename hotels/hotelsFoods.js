const mongoose=require('mongoose');

const SchemaAreas= new mongoose.Schema({

    foodName:{type:String},
    keyWord:{type:String},
    descriptions:{type:String},
    images:{type:String},

    hotelsClientsForFoodDescriptions:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hotelsClients'
}
    
},
{timestamps:true});
const Area=mongoose.model('hotelsFoodType',SchemaAreas);

module.exports=Area;

