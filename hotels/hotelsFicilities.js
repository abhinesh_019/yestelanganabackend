const mongoose=require('mongoose');

const SchemaFecilities= new mongoose.Schema({

    name:{type:String},
    importantsKey:{type:String},
    descriptions:{type:String},
    images:{type:String},
 
    hotelsClientsForFecilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hotelsClients'
}
    
},
{timestamps:true});
const fecilities=mongoose.model('hotelsFecilities',SchemaFecilities);

module.exports=fecilities;

