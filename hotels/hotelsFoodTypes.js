const mongoose=require('mongoose');

const SchemaAreas= new mongoose.Schema({

    foodType:{type:String},
  
    hotelsClientsForFoodTypes:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hotelsClients'
}
    
},
{timestamps:true});
const Area=mongoose.model('hotelsFoodType',SchemaAreas);

module.exports=Area;

