const express= require('express');
var router = express.Router();
const userControllers = require('../hotels/hotelsControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

 


//  ************ Locations ***********

router.route('/hotelsLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/hotelsAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/hotelsClientsget/:AreaId')
.get(userControllers.index)

router.route('/hotelsClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/hotelsClientsAllCount') 
.get(userControllers.totalclients)

router.route('/hotelsClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/hotelsClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/hotelsClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/hotelsfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/hotelsfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/hotelsfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************

//  ************ Service Type ***********
router.route('/hotelServicesType/:clientId')
.get(userControllers.ServicesType)
 .post(userControllers.ServicesTypeP);

//  ***********************

// ******************* Clients Services ******************
router.route('/hotelsServicesGet/:serviceId')
.get(userControllers.servicesget)
router.route('/hotelsServicesGetCounts/:serviceId')
.get(userControllers.servicesgetCounts)
router.route('/hotelsServicesPost/:serviceId')
.post(multipartMiddleware,userControllers.servicesPost);

// *******************************************************
// ****************** only updates details ****************
router.route('/hotelsupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/hotelsupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/hotelsupdatesgetpostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/hotelsupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/hotelsupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/hotelsupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/hotelsupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/hotelsupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/hotelseuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/hotelsCeuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)
module.exports = router;