const mongoose=require('mongoose');

const SchemaAreas= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,
  
    hotelsClientsForOnlyComments:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hotelsClients'
}
    
},
{timestamps:true});
const Area=mongoose.model('hotelsOnlyComments',SchemaAreas);

module.exports=Area;

