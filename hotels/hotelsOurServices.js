const mongoose=require('mongoose');

const SchemaRoom= new mongoose.Schema({

    keyWord:{type:String},
    prices:{type:String},
    fecilities:{type:String},
    descriptions:{type:String},
    images1:{type:String},
    images2:{type:String},
    images3:{type:String},
    images4:{type:String},

    hotelsClientsForOurServices:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hotelsRoomType'
}
    
},
{timestamps:true});
const room=mongoose.model('hotelsOurServices',SchemaRoom);

module.exports=room;

