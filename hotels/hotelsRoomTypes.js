const mongoose=require('mongoose');

const SchemaRoom= new mongoose.Schema({

    roomType:{type:String},
  
    hotelsClientsForRoomTypes:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hotelsClients'
}
    
},
{timestamps:true});
const room=mongoose.model('hotelsRoomType',SchemaRoom);

module.exports=room;

