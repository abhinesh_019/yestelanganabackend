const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    hotelsLocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hotelsLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('hotelsAreas',areaSchema);
module.exports=areas;