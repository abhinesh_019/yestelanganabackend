const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    gameName:String,
    aboutGame:String,
    NoOfPlayers:String,
    trainersName:String,
    trainersYearsOfExp:String,
    trainersImg:String,
     gameImg:String,


    clientsForServices:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'indoorgamesClient'
    }
},
{timestamps:true});
const comm=mongoose.model('indoorgamesServices',commentsSchema);
module.exports=comm;