const mongoose=require('mongoose');

const SchemaAreas= new mongoose.Schema({

    fecilitiesName:String,
    fecilitiesImg:String,
    fecilitiesDescriptions:String,
 
    ClientsForFecilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'indoorgamesClient'
}
    
},
{timestamps:true});
const Area=mongoose.model('indoorgamesFecilities',SchemaAreas);

module.exports=Area;

