const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'indoorgamesLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('indoorgamesArea',areaSchema);
module.exports=areas;