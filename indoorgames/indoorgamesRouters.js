const express= require('express');
var router = express.Router();
const userControllers = require('../indoorgames/indoorgamesControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ Locations ***********

router.route('/indoorgamesLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/indoorareas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/indoorgamesClientsget/:AreaId')
.get(userControllers.index)

router.route('/indoorgamesClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/indoorgamesClientsAllCount') 
.get(userControllers.totalclients)


router.route('/indoorgamesClientsCountAreas/:AreaId')  
.get(userControllers.tuserscountAreas)


router.route('/indoorgamesClientsCountDist')  
.get(userControllers.tuserscountDist)


router.route('/indoorgamesClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/indoorgamesClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/indoorgamesClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/indoorgamesfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/indoorgamesfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/indoorgamesfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************


// ******************* Clients service Type ******************
router.route('/indoorgamesServiceGet/:clientId')
.get(userControllers.serviceget)
 
router.route('/indoorgamesServiceGetc/:clientId')
.get(userControllers.servicegetc)

router.route('/indoorgamesServiceCatPost/:clientId')
.post(multipartMiddleware,userControllers.servicespostType);

// *******************************************************
// ******************* Clients Services ******************
router.route('/indoorgamesServicesGet/:clientId')
.get(userControllers.servicesget)
router.route('/indoorgamesServicesGetCounts/:clientId')
.get(userControllers.servicesgetCounts)
router.route('/indoorgamesServicesPost/:clientId')
.post(multipartMiddleware,userControllers.servicesPost);

// *******************************************************
// ****************** only updates details ****************
router.route('/indoorgamesupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/indoorgamesupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/indoorgamesupdatespostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/indoorgamesupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/indoorgamesupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/indoorgamesupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/indoorgamesupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/indoorgamesupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/indoorgamesUsersComments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/indoorGamesUsersCommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

 

// *************************************** AddsOne ************************************* 
router.route('/indoorgamesAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/indoorgamesAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/indoorgamesAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/indoorgamesAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/indoorgamesAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/indoorgamesAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/indoorgamesAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/indoorgamesAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/indoorgamesAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/indoorgamesAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/indoorgamesAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/indoorgamesAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/indoorgamesAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/indoorgamesAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/indoorgamesAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/indoorgamesAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/indoorgamesAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/indoorgamesAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/indoorgamesAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/indoorgamesAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/indoorgamesAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/indoorgamesAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/indoorgamesAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/indoorgamesAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/indoorgamesAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/indoorgamesAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/indoorgamesAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
