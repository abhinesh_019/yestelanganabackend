const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    plantsLocation:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'plantslocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('plantsArea',areaSchema);
module.exports=areas;