const express= require('express');
var router = express.Router();
const userControllers = require('../plants/plantscontroller');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();


router.route('/plantsLocations')
.get(userControllers.plantsLocationsget)
.post(multipartMiddleware,userControllers.plantsLocationsPosts);
 
router.route('/plantsAreas/:plantslocationsId')
.get(userControllers.plantsareaget)
router.route('/plantsArea/:plantslocationsId')
.post(multipartMiddleware,userControllers.plantsareasposts);


// *************************************** Clients ************************************* 
router.route('/plantsClients/:plantsAreaId')
.get(userControllers.plantsClientsGet)

router.route('/plantsClientIdInd/:plantsClientId')
.get(userControllers.plantsClientsIdGet)

router.route('/plantsClientsCountsByArea/:plantsAreaId')
.get(userControllers.plantsClientsGetCountsByArea)

router.route('/plantsCountsAreas/:plantsAreaId') 
.get(userControllers.plantsFamousAreas)

router.route('/plantsCountsLocations') 
.get(userControllers.plantsFamousLocations)

router.route('/plantsClientsAll')
.get(userControllers.plantsGetonlyAll)
 
  router.route('/plantsClientspostsdata/:plantsAreaId')
  .post(multipartMiddleware,userControllers.plantsClientsPost);
// **************************************************************************** 

// *************************************** plants types ************************************* 
router.route('/plantsTypes/:plantsClientsId')
.get(userControllers.plantsTypesGet)
.post(userControllers.plantsTypesPost);

router.route('/plantsType/:plantsTypesId')
.get(userControllers.plantsTypesGetId)
  
   
// **************************************************************************** 
// *************************************** plants types descriptions ************************************* 
router.route('/plantsTypesdes/:plantsTypeId')
.get(userControllers.plantsTypesdesGet)
router.route('/plantsTypesdesCont/:plantsTypeId')
.get(userControllers.plantsTypesdesGetCont)
router.route('/plantsTypesdesConta/:plantsTypeId')
.get(userControllers.plantsTypesdesGetContDetails)
router.route('/plantsTypesdesc/:plantsTypeId')
.post(multipartMiddleware,userControllers.plantsTypesdescPost);

router.route('/plantsTypedesInd/:plantsTypesdesId')
.get(userControllers.plantsTypesDesGetId)
  
   
// **************************************************************************** 

// ****************** only updates details ****************
router.route('/plantsupdatesget/:plantsClientsId')
.get(userControllers.plantsUsersGet)

router.route('/plantsupdatesgetCounts/:plantsClientsId')
.get(userControllers.plantsUsersGetCounts)

router.route('/plantsupdatesgetpostsdata/:plantsClientsId')
.post(multipartMiddleware,userControllers.plantsUsersUpdates);



//updates comments and reply 

router.route('/plantsupdatesCommentsget/:plantsupdatesId')
.get(userControllers.plantsupdatesCommentsGet)
router.route('/plantsupdatesCommentsgetcounts/:plantsupdatesId')
.get(userControllers.plantsupdatesCommentsGetcounts)

router.route('/plantsupdatesCommentspost/:plantsupdatesId')
.post(userControllers.plantsupdatesCommentsposts)

router.route('/plantsupdatesCommentReplysPost/:plantsupdatesCommId')
.post(userControllers.plantsupdatesCommentsReplyposts)

 router.route('/plantsupdatesCommentReplysGet/:plantsupdatesCommId')
.get(userControllers.plantsupdatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/plantsuserscomments/:plantsClientsId')
.get(userControllers.plantsusersCommentsget)
.post(userControllers.plantsusersCommentsposts);

router.route('/plantsuserscommentsCounts/:plantsClientsId')
.get(userControllers.plantsusersCommentsgetCounts)

// ****************** PlantsNecessaryMaintainces ****************

router.route('/PlantsNecessaryMaintaincesget/:plantsClientsId')
.get(userControllers.PlantsNecessaryMaintaincesGet)

router.route('/PlantsNecessaryMaintaincesgetCounts/:plantsClientsId')
.get(userControllers.PlantsNecessaryMaintaincesGetCounts)

router.route('/PlantsNecessaryMaintaincespostsdata/:plantsClientsId')
.post(multipartMiddleware,userControllers.PlantsNecessaryMaintaincesPosts);

// ****************** PlantsFertilizers ****************

router.route('/plantsPlantsFertilizersget/:plantsClientsId')
.get(userControllers.plantsPlantsFertilizersGet)

router.route('/plantsPlantsFertilizersgetCounts/:plantsClientsId')
.get(userControllers.plantsPlantsFertilizersGetCounts)

router.route('/plantsPlantsFertilizersgetpostsdata/:plantsClientsId')
.post(multipartMiddleware,userControllers.plantsPlantsFertilizersPosts);


// *************************************** AddsOne ************************************* 
router.route('/plantsAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/plantsAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/plantsAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/plantsAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/plantsAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/plantsAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/plantsAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/plantsAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/plantsAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/plantsAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/plantsAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/plantsAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/plantsAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/plantsAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/plantsAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/plantsAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/plantsAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/plantsAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/plantsAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/plantsAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/plantsAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/plantsAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/plantsAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/plantsAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/plantsAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/plantsAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/plantsAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
