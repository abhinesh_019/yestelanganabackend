const mongoose=require('mongoose');
 
const plantsSchema= new mongoose.Schema(
    {
             shopname:{type:String},
                 name:{type:String},
                aboutUsDescriptionOne:{type:String},
                aboutUsDescriptionTwo:{type:String},
                day1:{type:String},
                day2:{type:String},
                day3:{type:String},
                day4:{type:String},
                day5:{type:String},
                day6:{type:String},
                day7:{type:String},
                totaltiming:{type:String},
                hno:{type:String},
                area:{type:String},
                landmark:{type:String},
                 city:{type:String},
                distict:{type:String},
                state:{type:String},
                pincode:{type:String},
                 officeno:{type:String},
                mobileno:{type:String},
                whatsupno:{type:String},
                emailid:{type:String},
            images1:{type:String},
            images2:{type:String},
            images3:{type:String},
            images4:{type:String},
            images5:{type:String},
            images6:{type:String},
            
           plantsAreaForClients:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'plantsArea'
                     }
 },

{timestamps:true}

);

const plants=mongoose.model('plantsClients',plantsSchema);
module.exports=plants;