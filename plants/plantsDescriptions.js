const mongoose=require('mongoose');
const plantsDesSchema= new mongoose.Schema({

    name:String,
    age:String,
    precautions:String,
    descriptions:String,
    price:String,
    images1:String,
    images2:String,
    images3:String,
   


    plantsTypesForDescriptions:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'plantsTypes'
}
   
},
{timestamps:true});
const description=mongoose.model('plantsDescriptions',plantsDesSchema);
module.exports=description;

