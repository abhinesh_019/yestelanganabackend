const mongoose=require('mongoose');
const plantsTpesSchema= new mongoose.Schema({

    plantsTypes:String,

    plantsClientsForTypes:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'plantsClients'
}
   
},
{timestamps:true});
const typesd=mongoose.model('plantsTypes',plantsTpesSchema);
module.exports=typesd;