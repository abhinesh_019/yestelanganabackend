const mongoose=require('mongoose');
const plantsSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,

    plantsClientsForGeneralComments:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'plantsClients'
    }
},
{timestamps:true});
const comm=mongoose.model('plantsComments',plantsSchema);
module.exports=comm;