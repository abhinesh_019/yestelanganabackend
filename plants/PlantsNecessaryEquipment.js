const mongoose=require('mongoose');
const plantsDesSchema= new mongoose.Schema({

    EquipmentName:String,
    precautions:String,
    descriptions:String,
    price:String,
    images1:String,
    images2:String,
    images3:String,
  


    plantsTypesForPlantsNecessaryEquipment:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'plantsClients'
}
   
},
{timestamps:true});
const description=mongoose.model('PlantsNecessaryEquipments',plantsDesSchema);
module.exports=description;

