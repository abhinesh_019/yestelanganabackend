const mongoose=require('mongoose');
const plantsDesSchema= new mongoose.Schema({

    fertilizersName:String,
    fertilizersPrecautions:String,
    descriptions:String,
    price:String,
    images1:String,
    images2:String,
    images3:String,
   
    plantsTypesForFertilizers:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'plantsClients'
}
   
},
{timestamps:true});
const description=mongoose.model('plantsFertilizers',plantsDesSchema);
module.exports=description;

