var plantsL = require('../plants/plantsLocations')  
var async = require('async')
var plantsA = require('../plants/plantsArea')  
var plantsT = require('../plants/plantsTypes')
var plantsTD = require('../plants/plantsDescriptions')    
var plantsUpdates = require('../plants/plantsUpdates') 
var plantsGenenralComments = require('../plants/plantsOnlyComments')
var plantsUpdatesComments = require('../plants/plantsUpdatesComments') 
var plantsUpdatesCommentsReply = require('../plants/plantsUpdatesCommentsReply') 
var plantsClients = require('../plants/plantsClients') 
var plantsNecessaryEquipments = require('../plants/PlantsNecessaryEquipment')   
var PlantsFertilizers= require('../plants/PlantsFertilizersMaintainces')   
const fileService = require('../plants/plantsServer');
var babycaresAddsOne=require('../plants/fightsAddsOnea');
var babycaresAddsTwo=require('../plants/fightsAddsTwoa');
var babycaresAddsThree=require('../plants/fightsAddsThreea');
var babycaresAddsFour=require('../plants/fightsAddsFoura');
 
var babycaresAddsOneLoc=require('../plants/fightsAddsOnel');
var babycaresAddsTwoLoc=require('../plants/fightsAddsTwol');
var babycaresAddsThreeLoc=require('../plants/fightsAddsThreel');
var babycaresAddsFourLoc=require('../plants/fightsAddsFourl');
var babycaresAddsFiveLoc=require('../plants/fightsAddsFivel');
var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 
module.exports = {

    plantsLocationsget: async (req, res, next) => {

         const users = await plantsL.aggregate([
          {$sort:{"locations":1}}
                 ])
 
         res.status(200).json(users);
        console.log("users****", users);

    },

    plantsLocationsPosts: async (req,res,next)=>{
        const locs=  req.body;
        await plantsL.create(locs, function (err, data) {

            console.log(this.locs, "this.alldata");


            if (err) {
                console.log('Error in Saving user: ' + err);
            } else {

                res.status(201).json({ status: true, message: "user added sucessfully"});
            }

        });
    
    },
    // ******************* areas **********************
    plantsareasposts : async(req,res,next)=>{
        const { plantslocationsId } = req.params;
        req.body.plantsLocation = plantslocationsId
         const userscomm = new plantsA(req.body);
        await userscomm.save();
        console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
        
           res.status(201).json(userscomm);
  },


  plantsareaget:async(req,res,next)=>{
    const { plantslocationsId } = req.params;
    req.body.plantsLocation = plantslocationsId
    const users = await plantsA.find({plantsLocation:plantslocationsId})

     res.status(200).json(users);
    console.log("users****", users);

},

// **************************************************************************

// **************************************** Clients **********************************


plantsClientsPost: async (req, res, next) => {
 
    const { plantsAreaId } = req.params;
    req.body.plantsAreaForClients = plantsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },
       function (callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images4, function (err, data) {
                allData.images4 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images5, function (err, data) {
                allData.images5 = data.Location;
                callback(null, data.Location);
            })
        },
 
        function (callback) {
            fileService.uploadImage(req.files.images6, function (err, data) {
                allData.images6 = data.Location;
                callback(null, data.Location);
            })
        },
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            plantsClients.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},


plantsClientsGet:async (req, res, next) => {

    const { plantsAreaId } = req.params;
    let search=req.query.search;

    const usersposts = await plantsClients.find({plantsAreaForClients:plantsAreaId,$or:[
        {area:new RegExp(search, "gi")},
        {pincode:new RegExp(search, "gi")}, 
        {shopname:new RegExp(search, "gi")},
        {landmark:new RegExp(search, "gi")},
        {place:new RegExp(search, "gi")},
    ]
}).sort({"area":1})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

 
plantsClientsIdGet:async (req, res, next) => {
    const { plantsClientId } = req.params;
    const usersposts = await plantsClients.findById(plantsClientId) 
    res.status(200).json(usersposts);
    console.log(usersposts,"usersposts");
},

plantsClientsGetCountsByArea:async (req, res, next) => {
    const { plantsAreaId } = req.params;
    const usersposts = await plantsClients.find({plantsAreaForClients:plantsAreaId}).count()
    res.status(200).json(usersposts);
 },

plantsGetonlyAll:async (req, res, next) => {
     const usersposts = await plantsClients.find({}).count()
    res.status(200).json(usersposts);
 },
 plantsFamousAreas: async (req, res, next) => {
    const { plantsAreaId } = req.params;

    const users = await plantsClients.aggregate([{$match:{plantsAreaForClients:ObjectId(plantsAreaId)}},

        {"$group" : {_id:{area:"$area"}, count:{$sum:1}}},{$sort:{"area":1}}])
    
    res.status(200).json(users);
   console.log("users****", users);

},
plantsFamousLocations: async (req, res, next) => {
 
    const users = await plantsClients.aggregate([
		{"$group" : {_id:{distict:"$distict"}, count:{$sum:1}}},{$sort : {"distict":1}}])
	
    res.status(200).json(users);
   console.log("users****", users);

},
// **************************************************************************
// ***************************************************** plants types *******************************************************

plantsTypesGet:async(req,res,next)=>{
    const { plantsClientsId } = req.params;
    req.body.plantsClientsForTypes= plantsClientsId
    const users = await plantsT.find({plantsClientsForTypes : plantsClientsId}).sort({"plantsTypes":1})
     res.status(200).json(users);
 },
 plantsTypesGetId:async (req, res, next) => {

    const { plantsTypesId } = req.params;
 
    const usersposts = await plantsT.findById(plantsTypesId)
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},
plantsTypesPost: async(req,res,next)=>{
    const { plantsClientsId } = req.params;
    req.body.plantsClientsForTypes= plantsClientsId
     const userscomm = new plantsT(req.body);
    await userscomm.save();
       res.status(201).json(userscomm);
},

// ********************************************************************************************************

 // ********************* post plants descriptions *******************
 plantsTypesdescPost: async (req, res, next) => {
 
    const { plantsTypeId } = req.params;
    req.body.plantsTypesForDescriptions = plantsTypeId
    let allData = req.body
     async.parallel([

        function (callback) {
            if(req.files.images1){
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
               })
        } else{
            callback(null, null);   
        }
        },

        function (callback) {
            if(req.files.images2){
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        } else{
            callback(null, null);   
        }
        },

        function (callback) {
            if(req.files.images3){
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null, data.Location);
            })
        } else{
            callback(null, null);   
        }
        },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            plantsTD.create(allData, function (err, data) {
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully" });
                }

            });
        }
  });
},

plantsTypesdesGet: async (req, res, next) => {
     const { plantsTypeId } = req.params;
        const users = await plantsTD.find({plantsTypesForDescriptions:plantsTypeId}).sort({"name":1})
        res.status(200).json(users);
 },
 plantsTypesdesGetCont: async (req, res, next) => {
    const { plantsTypeId } = req.params;
       const users = await plantsTD.find({plantsTypesForDescriptions:plantsTypeId}).count()
       res.status(200).json(users);
},
plantsTypesdesGetContDetails: async (req, res, next) => {
    const { plantsTypeId } = req.params;
       const users = await plantsTD.aggregate([{$match:{plantsTypesForDescriptions:ObjectId(plantsTypeId)}},

        {"$group" : {_id:{name:"$name"}, count:{$sum:1}}},{$sort:{"area":1}}])
       res.status(200).json(users);
},
plantsTypesDesGetId: async (req, res, next) => {
     const { plantsTypesdesId } = req.params;
      const users = await plantsTD.findById(plantsTypesdesId)
        res.status(200).json(users);
 },
 
// *************************************
    // ****************users updates*************

    plantsUsersUpdates: async (req, res, next) => {
 
        const { plantsClientsId } = req.params;
        req.body.plantsClientsForUpdates = plantsClientsId
        let allData = req.body 
 
        async.parallel([
    
    
            function (callback) {
                 if(req.files.images){
                    fileService.uploadImage(req.files.images, function (err, data) {
                        allData.images = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
                
            },
            
            function (callback) {
                 if(req.files.viedoes){
                    fileService.uploadImage(req.files.viedoes, function (err, data) {
                        allData.viedoes = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
              },
     
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                plantsUpdates.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }  });  }  });
    },

    plantsUsersGet:async (req, res, next) => {
    
        const { plantsClientsId } = req.params;
    
            const users = await plantsUpdates.find({plantsClientsForUpdates:plantsClientsId}).sort({"createdAt:":1})
    
            res.status(200).json(users);
           console.log("users****", users);
    },

    plantsUsersGetCounts:async (req, res, next) => {
    
        const { plantsClientsId } = req.params;
    
            const users = await plantsUpdates.find({plantsClientsForUpdates:plantsClientsId}).count()
    
            res.status(200).json(users);
           console.log("users****", users);
    },
//admin users updates comments
plantsupdatesCommentsposts:async (req, res, next) => {
 
    const { plantsupdatesId } = req.params;
      req.body.plantsUpdatesForComments = plantsupdatesId 
    const userscomm = new plantsUpdatesComments(req.body);

    await userscomm.save();
 console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
     res.status(201).json(userscomm);
 },

 plantsupdatesCommentsGet:async (req, res, next) => {
     const { plantsupdatesId } = req.params;
       const users = await plantsUpdatesComments.find({plantsUpdatesForComments:plantsupdatesId})
        res.status(200).json(users); 
},
plantsupdatesCommentsGetcounts:async (req, res, next) => {
     const { plantsupdatesId } = req.params;
       const users = await plantsUpdatesComments.find({plantsUpdatesForComments:plantsupdatesId}).count()
        res.status(200).json(users);
},

plantsupdatesCommentsReplyposts:async (req, res, next) => {
   const { plantsupdatesCommId } = req.params;
      req.body.plantsUpdatesCommForReply = plantsupdatesCommId 
    const userscomm = new plantsUpdatesCommentsReply(req.body);
 await userscomm.save();
 console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
   res.status(201).json(userscomm);
},

plantsupdatesCommentsReplyGet:async (req, res, next) => {
    
    const { plantsupdatesCommId } = req.params;

        const users = await plantsUpdatesCommentsReply.find({plantsUpdatesCommForReply:plantsupdatesCommId})

        res.status(200).json(users);
       console.log("users**** 399", users);
},

// ****************only comments *******************
plantsusersCommentsposts : async (req,res) => {
    const { plantsClientsId } = req.params;
    req.body.plantsClientsForGeneralComments = plantsClientsId
    const generalcomm = new plantsGenenralComments(req.body);
    await generalcomm.save();
  res.status(201).json(generalcomm);
},
plantsusersCommentsget:async (req, res, next) => {
 const { plantsClientsId } = req.params;
 const users = await plantsGenenralComments.find({plantsClientsForGeneralComments:plantsClientsId})
 res.status(200).json(users);   
},
plantsusersCommentsgetCounts:async (req, res, next) => {
  const { plantsClientsId } = req.params;
 const users = await plantsGenenralComments.find({plantsClientsForGeneralComments:plantsClientsId}).count()
 res.status(200).json(users); 
},

// ****************** plantsNecessaryEquipments ****************

PlantsNecessaryMaintaincesPosts: async (req, res, next) => {
 
    const { plantsClientsId } = req.params;
    req.body.plantsTypesForPlantsNecessaryEquipment = plantsClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
             if(req.files.images1){
                fileService.uploadImage(req.files.images1, function (err, data) {
                    allData.images1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
             if(req.files.images2){
                fileService.uploadImage(req.files.images2, function (err, data) {
                    allData.images2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            if(req.files.images3){
               fileService.uploadImage(req.files.images3, function (err, data) {
                   allData.images3 = data.Location;
                   callback(null, data.Location);
               })
             }
           else{
               callback(null, null);   
           }
         },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            plantsNecessaryEquipments.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

PlantsNecessaryMaintaincesGet:async (req, res, next) => {

    const { plantsClientsId } = req.params;

        const users = await plantsNecessaryEquipments.find({plantsTypesForPlantsNecessaryEquipment:plantsClientsId}).sort({"EquipmentName":1})

        res.status(200).json(users);
       console.log("users****", users);
},

PlantsNecessaryMaintaincesGetCounts:async (req, res, next) => {

    const { plantsClientsId } = req.params;

        const users = await plantsNecessaryEquipments.find({plantsTypesForPlantsNecessaryEquipment:plantsClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
// ****************** PlantsFertilizers ****************

plantsPlantsFertilizersPosts: async (req, res, next) => {
 
    const { plantsClientsId } = req.params;
    req.body.plantsTypesForFertilizers = plantsClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
             if(req.files.images1){
                fileService.uploadImage(req.files.images1, function (err, data) {
                    allData.images1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
             if(req.files.images2){
                fileService.uploadImage(req.files.images2, function (err, data) {
                    allData.images2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            if(req.files.images3){
               fileService.uploadImage(req.files.images3, function (err, data) {
                   allData.images3 = data.Location;
                   callback(null, data.Location);
               })
             }
           else{
               callback(null, null);   
           }
         },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            PlantsFertilizers.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

plantsPlantsFertilizersGet:async (req, res, next) => {

    const { plantsClientsId } = req.params;

        const users = await PlantsFertilizers.find({plantsTypesForFertilizers:plantsClientsId}).sort({"fertilizersName":1})

        res.status(200).json(users);
       console.log("users****", users);
},

plantsPlantsFertilizersGetCounts:async (req, res, next) => {

    const { plantsClientsId } = req.params;

        const users = await PlantsFertilizers.find({plantsTypesForFertilizers:plantsClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},


    // *************************************AddsOne*****************
 

    babycareAddsOneP: async (req, res, next) => {
 
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsOnea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImg, function (err, data) {
                    allData.addsOneImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOne.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOne.find({babycareAreaForAddsOnea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOne.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsTwo*****************
     
     
    babycareAddsTwoP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsTwoa = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImg, function (err, data) {
                    allData.addsTwoImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwo.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
    babycareAddsTwoGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwo.find({babycareAreaForAddsTwoa:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwo.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsThree*****************
    
    
    babycareAddsThreeP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsThreea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImg, function (err, data) {
                    allData.addsThreeImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThree.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThree.find({babycareAreaForAddsThreea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThree.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsFour*****************
    
     
     
    babycareAddsFourP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsFoura = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImg, function (err, data) {
                    allData.addsFourImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFour.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFour.find({babycareAreaForAddsFoura:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFour.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
     
    // *************************************AddsOneLoc*****************
    
    
    babycareAddsOneLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocOne = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImgLoc, function (err, data) {
                    allData.addsOneImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOneLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOneLoc.find({babycaresAreaForAddsLocOne:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOneLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsTwoLoc*****************
    
    
    babycareAddsTwoLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocTwo = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImgLoc, function (err, data) {
                    allData.addsTwoImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwoLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsTwoLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwoLoc.find({babycaresAreaForAddsLocTwo:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwoLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsThreeLoc*****************
    
    
    babycareAddsThreeLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocThree = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImgLoc, function (err, data) {
                    allData.addsThreeImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThreeLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThreeLoc.find({babycaresAreaForAddsLocThree:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThreeLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFourLoc*****************
    
    
    babycareAddsFourLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFour = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImgLoc, function (err, data) {
                    allData.addsFourImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFourLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFourLoc.find({babycaresAreaForAddsLocFour:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFourLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFiveLoc*****************
    
    
    babycareAddsFiveLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFive = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFiveImgLoc, function (err, data) {
                    allData.addsFiveImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFiveLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFiveLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFiveLoc.find({babycaresAreaForAddsLocFive:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFiveDeleteLoc: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFiveLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    }, 
    }
    

