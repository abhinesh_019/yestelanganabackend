const mongoose=require('mongoose');

const SchemaVehicle= new mongoose.Schema({
 
    learningPeriods1:String,
    learningPeriods2:String,
    learningPeriods3:String,
    providence:String,
    shifts:String,
    timmings:String,
    hoursOfOperations:String,
    routesCoveredFrom:String,
    routesCoveredTo:String,
    trainerName:String,
    trainerExp:String,
    trainerImg:String,
 

 
    ClientsVehicleForCustomers:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'drivingServiceVehicle'
}
    
},
{timestamps:true});
const serviceVehicle=mongoose.model('drivingServiceVehicleCustomers',SchemaVehicle);

module.exports=serviceVehicle;

