const express= require('express');
var router = express.Router();
const userControllers = require('../drivingSchools/drivingControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ Locations ***********

router.route('/drivingSchoolsLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/drivingSchoolsArea/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/drivingSchoolsClientsget/:AreaId')
.get(userControllers.index)

router.route('/drivingSchoolsClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/drivingSchoolsClientsAllCount') 
.get(userControllers.totalclients)


router.route('/drivingSchoolsClientsCountAreas/:AreaId')  
.get(userControllers.tuserscountAreas)


router.route('/drivingSchoolsClientsCountDist')  
.get(userControllers.tuserscountDist)


router.route('/drivingSchoolsClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/drivingSchoolsClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/drivingSchoolsClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/drivingSchoolsfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/drivingSchoolsfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/drivingSchoolsfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************

// ****************** only updates details ****************
router.route('/drivingSchoolsupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/drivingSchoolsupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/drivingSchoolsupdatespostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/drivingSchoolsupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/drivingSchoolsupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/drivingSchoolsupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/drivingSchoolsupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/drivingSchoolsupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/drivingSchoolsUsersComments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/drivingSchoolsUsersCommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

 // ******************* Clients Services for vehicle ******************
router.route('/drivingSchoolsServicesGet/:clientId')
.get(userControllers.servicesget)
router.route('/drivingSchoolsServicesGetCounts/:clientId')
.get(userControllers.servicesgetCounts)
router.route('/drivingSchoolsServicesPost/:clientId')
.post(multipartMiddleware,userControllers.servicesPost);
// *******************************************************
 // ******************* Clients Services for CustomersDetails ******************
 router.route('/drivingSchoolsServicesCustomers/:clientId')
 .get(userControllers.servicesCustomersDetailsget)
 router.route('/drivingSchoolsServicesCustomersCounts/:clientId')
 .get(userControllers.servicesgetCustomersDetailsCounts)
 router.route('/drivingSchoolsServicesCustomersPost/:clientId')
 .post(multipartMiddleware,userControllers.servicesCustomersDetailsPost);
 
 // *******************************************************
 // ******************* Clients Services for No of customers ******************
 router.route('/drivingSchoolsTotalCustomers/:clientId')
 .get(userControllers.servicesNoOfCustomersget)
 router.route('/drivingSchoolsTotalCustomersCounts/:clientId')
 .get(userControllers.servicesNoOfCustomersgetCounts)
 router.route('/drivingSchoolsTotalCustomersPost/:clientId')
 .post(multipartMiddleware,userControllers.servicesNoOfCustomersPost);
 
 // *******************************************************
  // ******************* Clients Services trackRecords ******************
  router.route('/drivingSchoolsServicestrackRecords/:clientId')
  .get(userControllers.serviceTrackRecords)
  router.route('/drivingSchoolsServicestrackRecordsCounts/:clientId')
  .get(userControllers.serviceTrackRecordsCounts)
  router.route('/drivingSchoolsServicestrackRecordsPost/:clientId')
  .post(multipartMiddleware,userControllers.serviceTrackRecordsPost);
  
  // *******************************************************


// *************************************** AddsOne ************************************* 
router.route('/drivingSchoolsAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/drivingSchoolsAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/drivingSchoolsAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/drivingSchoolsAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/drivingSchoolsAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/drivingSchoolsAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/drivingSchoolsAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/drivingSchoolsAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/drivingSchoolsAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/drivingSchoolsAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/drivingSchoolsAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/drivingSchoolsAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 

// *************************************** AddsOne ************************************* 
router.route('/drivingSchoolsAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/drivingSchoolsAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/drivingSchoolsAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/drivingSchoolsAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/drivingSchoolsAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/drivingSchoolsAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/drivingSchoolsAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/drivingSchoolsAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/drivingSchoolsAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/drivingSchoolsAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/drivingSchoolsAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/drivingSchoolsAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/drivingSchoolsAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/drivingSchoolsAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/drivingSchoolsAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
