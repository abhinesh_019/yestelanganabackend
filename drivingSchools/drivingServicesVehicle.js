const mongoose=require('mongoose');

const SchemaVehicle= new mongoose.Schema({
    
    vehicleName:String,
    vehicleType:String,
    vehicleImg:String,
 
    ClientsForVehicle:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'drivingClient'
}
    
},
{timestamps:true});
const serviceVehicle=mongoose.model('drivingServiceVehicle',SchemaVehicle);

module.exports=serviceVehicle;

