const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'drivingLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('drivingArea',areaSchema);
module.exports=areas;