var locations = require('../drivingSchools/drivingLocations')
var areas = require('../drivingSchools/drivingArea')
var clients = require('../drivingSchools/drivingClients')
 var clientsUpdates = require('../drivingSchools/drivingUpdates')
var clientsUpdatesComments = require('../drivingSchools/drivingUpdatesComments')
var clientsUpdatesCommentsReply = require('../drivingSchools/drivingUpdatesCommentsReply')
var clientsOnlyComments = require('../drivingSchools/drivingOnlyCOmments')
var fecilities = require('../drivingSchools/drivingFecilities')
var serviceType = require('../drivingSchools/drivingServicesVehicle')
var serviceTypeTwo = require('../drivingSchools/drivingServicesCustomersDetails')
var serviceCname = require('../drivingSchools/drivingServicesNoOfCustomers')
var tracks = require('../drivingSchools/drivingServicesTrackRecords')
const fileService = require('../drivingSchools/drivingFileServer');
  

var babycaresAddsOne=require('../drivingSchools/drivingSchoolsAddsOnea');
var babycaresAddsTwo=require('../drivingSchools/drivingSchoolsAddsTwoa');
var babycaresAddsThree=require('../drivingSchools/drivingSchoolsAddsThreea');
var babycaresAddsFour=require('../drivingSchools/drivingSchoolsAddsFoura');
 
var babycaresAddsOneLoc=require('../drivingSchools/drivingSchoolsAddsOnel');
var babycaresAddsTwoLoc=require('../drivingSchools/drivingSchoolsAddsTwol');
var babycaresAddsThreeLoc=require('../drivingSchools/drivingSchoolsAddsThreel');
var babycaresAddsFourLoc=require('../drivingSchools/drivingSchoolsAddsFourl');
var babycaresAddsFiveLoc=require('../drivingSchools/drivingSchoolsAddsFivel');

var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 
var async = require('async');
  
 
module.exports = {

 
    // ********************* Locations *********************
    
    locationspost: async(req,res,next)=>{
        const locs=  req.body;
        await locations.create(locs, function (err, data) {
            if (err) {
                console.log('Error in Saving user: ' + err);
            } else {
    
                res.status(201).json({ status: true, message: "user added sucessfully"});
            }
        });
     },
    
    
    
    locationsget: async(req,res,next)=>{
        const users = await locations.find({}).sort({"locations":1});
           res.status(200).json(users);
     },
    // ******************************************
    
    // *********************** Areas *******************
    
    Areasposts : async(req,res,next)=>{
        const { LocationsId } = req.params;
        req.body.LocationForAreas = LocationsId
         const userscomm = new areas(req.body);
        await userscomm.save();
            res.status(201).json(userscomm);
    },
     
    Areasget:async (req, res, next) => {
         const { LocationsId } = req.params;
         const usersposts = await areas.find({LocationForAreas:LocationsId}).sort({"area":-1})
        res.status(200).json(usersposts);
     },
    
    // ******************************************
    // ************************************************ Clients Updates ***************************************
     
     
    newUser: (req, res, next) => {
    
            
        const { AreaId } = req.params;
        req.body.areaForClients = AreaId
        let allData = req.body
     
        async.parallel([
            function (callback) { 
                fileService.uploadImage(req.files.images1, function (err, data) {
                    allData.images1 = data.Location;
                    callback(null, data.Location);
                })
            },
    
            function(callback) {
                fileService.uploadImage(req.files.images2, function (err, data) {
                    allData.images2 = data.Location;
                    callback(null, data.Location);
                })
            },
    
            function(callback) {
                fileService.uploadImage(req.files.images3, function (err, data) {
                    allData.images3 = data.Location;
                    callback(null,data.Location);
                })
            },
            function(callback) {
                fileService.uploadImage(req.files.images4, function (err, data) {
                    allData.images4 = data.Location;
                    callback(null, data.Location);
                })
            },
     
            function (callback) {
                fileService.uploadImage(req.files.images5, function (err, data) {
                    allData.images5 = data.Location;
                    callback(null, data.Location);
                })
            },
            function (callback) {
                fileService.uploadImage(req.files.images6, function (err, data) {
                    allData.images6 = data.Location;
                    callback(null, data.Location);
                })
            },
             
    
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                clients.create(allData, function (err, data) {
                if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                         res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                    }
                });
            }
     });
    },
    
    
    
    index:async (req, res, next) => {
    
        const { AreaId } = req.params;
        let search=req.query.search;
    
        const usersposts = await clients.find({areaForClients:AreaId,$or:[
            {subArea:new RegExp(search, "gi")},
            {pincode:new RegExp(search, "gi")}, 
            {instName:new RegExp(search, "gi")},
            {indoorCat:new RegExp(search, "gi")},
            {place:new RegExp(search, "gi")},
        ]
    }).sort({"subArea":-1})
        res.status(200).json(usersposts);
     },
    
    usersgetcoutarea: async (req, res, next) => {
        const { AreaId } = req.params;
    
        const users = await clients.aggregate([{$match:{areaForClients:ObjectId(AreaId)}},
    
            {"$group" : {_id:{subArea:"$subArea"}, count:{$sum:1}}},{$sort:{"subArea":1}}
         ])
        
        res.status(200).json(users);
     },
     
    tuserscount:async (req, res, next) => {
         const { AreaId } = req.params;
         const usersposts = await clients.find({areaForClients:AreaId}).count()
        res.status(200).json(usersposts);
     },
    
    
    totalclients:async (req, res, next) => {
      const usersposts = await clients.find({}).count()
        res.status(200).json(usersposts);
      
    },
     
    indexs:async (req, res, next) => {
    
        const { clientsId } = req.params;
        const usersposts = await clients.findById(clientsId) 
        res.status(200).json(usersposts);
      
    },
    tuserscountAreas: async (req, res, next) => {
        const { AreaId } = req.params;
    
        const users = await clients.aggregate([{$match:{areaForClients:ObjectId(AreaId)}},
    
            {"$group" : {_id:{mainArea:"$mainArea"}, count:{$sum:1}}},{$sort:{"mainArea":1}}
         ])
        
        res.status(200).json(users);
     },
    
     tuserscountDist: async (req, res, next) => {
         
    
        const users = await clients.aggregate([
    
            {"$group" : {_id:{distict:"$distict"}, count:{$sum:1}}},{$sort:{"distict":1}}
         ])
        
        res.status(200).json(users);
     },
    // ***********************************************************************
    
    
    // ************************************************ Clients fecilities ***************************************
     
    fecilitiespost: (req, res, next) => {
        const { clientId } = req.params;
        req.body.ClientsForFecilities = clientId
        let allData = req.body
      
        async.parallel([
            function (callback) { 
                fileService.uploadImage(req.files.fecilitiesImg, function (err, data) {
                    allData.fecilitiesImg = data.Location;
                    callback(null, data.Location);
                })
            },
     ],function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                fecilities.create(allData, function (err, data) {
                if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                         res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
                });
            }
     });
    },
     
    fecilitiesget:async (req, res, next) => {
    
        const { clientId } = req.params;
        
        const usersposts = await fecilities.find({ClientsForFecilities:clientId}).sort({"fecilitiesName":-1})
        res.status(200).json(usersposts);
     },
     fecilitiesgetCounts:async (req, res, next) => {
    
        const { clientId } = req.params;
        
        const usersposts = await fecilities.find({ClientsForFecilities:clientId}).count()
        res.status(200).json(usersposts);
     },
    // ***********************************************************************

// ****************users updates*************

UsersUpdates: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.UpdatedDetails = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.viedoes,"req.files.viedoes 302");
            if(req.files.viedoes){
                fileService.uploadImage(req.files.viedoes, function (err, data) {
                    allData.viedoes = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clientsUpdates.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

UsersGet:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({UpdatedDetails:ClientsId}).sort({"createdAt:":-1})

        res.status(200).json(users);
       console.log("users****", users);
},

UsersGetCounts:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({UpdatedDetails:ClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
//admin users updates comments
updatesCommentsposts:async (req, res, next) => {

const { updatesId } = req.params;
  req.body.updatesForComments = updatesId 
const userscomm = new clientsUpdatesComments(req.body);

await userscomm.save();
console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
 res.status(201).json(userscomm);
},

updatesCommentsGet:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({updatesForComments:updatesId})
    res.status(200).json(users); 
},
updatesCommentsGetcounts:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({updatesForComments:updatesId}).count()
    res.status(200).json(users);
},

updatesCommentsReplyposts:async (req, res, next) => {
const { updatesCommId } = req.params;
  req.body.updatesCommentsForReply = updatesCommId 
const userscomm = new clientsUpdatesCommentsReply(req.body);
await userscomm.save();
 res.status(201).json(userscomm);
},

updatesCommentsReplyGet:async (req, res, next) => {

const { updatesCommId } = req.params;

    const users = await clientsUpdatesCommentsReply.find({updatesCommentsForReply:updatesCommId})

    res.status(200).json(users);
   console.log("users**** 399", users);
},

// ****************only comments *******************
usersCommentsposts : async (req,res) => {
const { ClientsId } = req.params;
req.body.clientsForOnlyComments = ClientsId
const generalcomm = new clientsOnlyComments(req.body);
await generalcomm.save();
res.status(201).json(generalcomm);
},
usersCommentsget:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await clientsOnlyComments.find({clientsForOnlyComments:ClientsId})
res.status(200).json(users);   
},
usersCommentsgetCounts:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await clientsOnlyComments.find({clientsForOnlyComments:ClientsId}).count()
    res.status(200).json(users);   
    },
// / ************************************************ Clients services Type for vehicle  ***************************************
   
servicesPost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.ClientsForVehicle = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.vehicleImg, function (err, data) {
                allData.vehicleImg = data.Location;
                callback(null, data.Location);
            })
        },
        
        
  ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            serviceType.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
  
servicesget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await serviceType.aggregate([{$match:{ClientsForVehicle:ObjectId(clientId)}},{$sort:{"vehicleName":1}}
     ])
    res.status(200).json(usersposts);
 },
 servicesgetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await serviceType.find({ClientsForVehicle:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************
// / ************************************************ Clients services for Customers Details ***************************************
   
servicesCustomersDetailsPost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.ClientsVehicleForCustomers = clientId
    let allData = req.body
  
    async.parallel([
         function (callback) {
             if(req.files.trainerImg){
                fileService.uploadImage(req.files.trainerImg, function (err, data) {
                    allData.trainerImg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
         },
         
        
  ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            serviceTypeTwo.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
  
servicesCustomersDetailsget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await serviceTypeTwo.aggregate([{$match:{ClientsVehicleForCustomers:ObjectId(clientId)}},{$sort:{"trainerName":1}}
     ])
    res.status(200).json(usersposts);
 },
 servicesgetCustomersDetailsCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await serviceTypeTwo.find({ClientsVehicleForCustomers:clientId}).count()
    res.status(200).json(usersposts);
 },

 // / ************************************************ Clients services for No of customers ***************************************
   
 servicesNoOfCustomersPost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.ClientsVehicleForTotalNoOfCustomers = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.customersImg, function (err, data) {
                allData.customersImg = data.Location;
                callback(null, data.Location);
            })
        },
        
        
  ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            serviceCname.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
  
servicesNoOfCustomersget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await serviceCname.aggregate([{$match:{ClientsVehicleForTotalNoOfCustomers:ObjectId(clientId)}},{$sort:{"customerName":1}}
     ])
    res.status(200).json(usersposts);
 },
 servicesNoOfCustomersgetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await serviceCname.find({ClientsVehicleForTotalNoOfCustomers:clientId}).count()
    res.status(200).json(usersposts);
 },
 // / ************************************************ Clients services Track Records ***************************************
   
 serviceTrackRecordsPost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.ClientsVehicleForTrackRecords = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.customersImg, function (err, data) {
                allData.customersImg = data.Location;
                callback(null, data.Location);
            })
        },
        
        
  ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            tracks.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
  
serviceTrackRecords:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await tracks.aggregate([{$match:{ClientsVehicleForTrackRecords:ObjectId(clientId)}},{$sort:{"customerName":1}}
     ])
    res.status(200).json(usersposts);
 },
 serviceTrackRecordsCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await tracks.find({ClientsVehicleForTrackRecords:clientId}).count()
    res.status(200).json(usersposts);
 },

    // *************************************AddsOne*****************
 

babycareAddsOneP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycareAreaForAddsOnea = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsOneImg, function (err, data) {
                allData.addsOneImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsOne.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsOneGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsOne.find({babycareAreaForAddsOnea:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsOneDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsOne.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},

// *************************************AddsTwo*****************
 
 
babycareAddsTwoP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycareAreaForAddsTwoa = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsTwoImg, function (err, data) {
                allData.addsTwoImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsTwo.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

babycareAddsTwoGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsTwo.find({babycareAreaForAddsTwoa:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsTwoDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsTwo.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},

// *************************************AddsThree*****************


babycareAddsThreeP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycareAreaForAddsThreea = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsThreeImg, function (err, data) {
                allData.addsThreeImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsThree.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsThreeGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsThree.find({babycareAreaForAddsThreea:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsThreeDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsThree.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},

// *************************************AddsFour*****************

 
 
babycareAddsFourP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycareAreaForAddsFoura = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsFourImg, function (err, data) {
                allData.addsFourImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsFour.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsFourGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsFour.find({babycareAreaForAddsFoura:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsFourDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsFour.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},

 
// *************************************AddsOneLoc*****************


babycareAddsOneLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocOne = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsOneImgLoc, function (err, data) {
                allData.addsOneImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsOneLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsOneLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsOneLoc.find({babycaresAreaForAddsLocOne:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsOneLocDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsOneLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},
// *************************************AddsTwoLoc*****************


babycareAddsTwoLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocTwo = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsTwoImgLoc, function (err, data) {
                allData.addsTwoImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsTwoLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsTwoLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsTwoLoc.find({babycaresAreaForAddsLocTwo:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsTwoLocDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsTwoLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},
// *************************************AddsThreeLoc*****************


babycareAddsThreeLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocThree = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsThreeImgLoc, function (err, data) {
                allData.addsThreeImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsThreeLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsThreeLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsThreeLoc.find({babycaresAreaForAddsLocThree:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsThreeLocDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsThreeLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},
// *************************************AddsFourLoc*****************


babycareAddsFourLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocFour = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsFourImgLoc, function (err, data) {
                allData.addsFourImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsFourLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsFourLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsFourLoc.find({babycaresAreaForAddsLocFour:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsFourLocDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsFourLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},
// *************************************AddsFiveLoc*****************


babycareAddsFiveLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocFive = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsFiveImgLoc, function (err, data) {
                allData.addsFiveImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsFiveLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsFiveLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsFiveLoc.find({babycaresAreaForAddsLocFive:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsFiveDeleteLoc: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsFiveLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
}, 
}