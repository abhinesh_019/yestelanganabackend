const mongoose=require('mongoose');

const SchemaVehicle= new mongoose.Schema({
    
     customerName:String,
     dateOfJoin:String,
     shift:String,
     timmings:String,
     selectedAsVehicle:String,
     place:String,
     customersImg:String,

 
    ClientsVehicleForTrackRecords:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'drivingServiceVehicle'
}
    
},
{timestamps:true});
const serviceVehicle=mongoose.model('drivingTrackRecords',SchemaVehicle);

module.exports=serviceVehicle;

