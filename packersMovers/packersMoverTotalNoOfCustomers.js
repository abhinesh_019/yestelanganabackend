const mongoose=require('mongoose');

const SchemaThree= new mongoose.Schema({
    
     customerName:String,
     place:String,
     customersImg:String,
 
    servicesThreeForTotalNoOfCustomers:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'packersMoversClient'
}
    
},
{timestamps:true});
const serviceServicesThree=mongoose.model('packersMoversTotalNoOfCustomers',SchemaThree);

module.exports=serviceServicesThree;

