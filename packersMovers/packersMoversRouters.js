const express= require('express');
var router = express.Router();
const userControllers = require('../packersMovers/packersMoversControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ Locations ***********

router.route('/packersMoversLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/packersMoversareas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/packersMoversClientsget/:AreaId')
.get(userControllers.index)

router.route('/packersMoversClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/packersMoversClientsAllCount') 
.get(userControllers.totalclients)


router.route('/packersMoversClientsCountAreas/:AreaId')  
.get(userControllers.tuserscountAreas)


router.route('/packersMoversClientsCountDist')  
.get(userControllers.tuserscountDist)


router.route('/packersMoversClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/packersMoversClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/packersMoversClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************
 // ******************* Clients Fecilities ******************
router.route('/packersMoversfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/packersMoversfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/packersMoversfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);


// ****************** only updates details ****************
router.route('/packersMoversupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/packersMoversupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/packersMoversupdatespostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/packersMoversupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/packersMoversupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/packersMoversupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/packersMoversupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/packersMoversupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/packersMoversUsersComments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/packersMoversUsersCommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

 // ******************* Clients Services one ******************
router.route('/packersMoversServicesGet/:clientId')
.get(userControllers.servicesget)
router.route('/packersMoversServicesGetCounts/:clientId')
.get(userControllers.servicesgetCounts)
router.route('/packersMoversServicesPost/:clientId')
.post(multipartMiddleware,userControllers.servicesPost);

// *******************************************************
 // ******************* Clients Services for CustomersDetails ******************
 router.route('/packersMoversServicesTwoGet/:clientId')
 .get(userControllers.servicesCustomersTwoDetailsget)
 router.route('/packersMoversServicesTwoGetCounts/:clientId')
 .get(userControllers.servicesgetCustomersTwoDetailsCounts)
 router.route('/packersMoversServicesTwoPost/:clientId')
 .post(multipartMiddleware,userControllers.servicesCustomersTwoDetailsPost);
 
 // *******************************************************
  
 // *******************************************************
  // ******************* Clients Services Track Records ******************
  router.route('/packersMoversServicesTrackRecordsGet/:clientId')
  .get(userControllers.serviceTrackRecords)
  router.route('/packersMoversServicesTrackRecordsGetCounts/:clientId')
  .get(userControllers.serviceTrackRecordsCounts)
  router.route('/packersMoversServicesTrackRecordsPost/:clientId')
  .post(multipartMiddleware,userControllers.serviceTrackRecordsPost);
  
  // *******************************************************

// *************************************** AddsOne ************************************* 
router.route('/packersMoversAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/packersMoversAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/packersMoversAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/packersMoversAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/packersMoversAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/packersMoversAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/packersMoversAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/packersMoversAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/packersMoversAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/packersMoversAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/packersMoversAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/packersMoversAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/packersMoversAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/packersMoversAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/packersMoversAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/packersMoversAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/packersMoversAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/packersMoversAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/packersMoversAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/packersMoversAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/packersMoversAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/packersMoversAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/packersMoversAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/packersMoversAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/packersMoversAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/packersMoversAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/packersMoversAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
