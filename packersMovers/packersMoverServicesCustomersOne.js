const mongoose=require('mongoose');

const SchemaAreas= new mongoose.Schema({

    vehicleName:String,
    vehicleCapacity:String,
    vehicleDescriptions:String,
    driverName:String,
    driverYearOfExp:String,
    DriverImg:String,
    vehicleImg:String,
    
    ClientsForServicesCustomersOne:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'packersMoversClient'
}
    
},
{timestamps:true});
const Area=mongoose.model('packersMoversServicesCustomersOne',SchemaAreas);

module.exports=Area;

