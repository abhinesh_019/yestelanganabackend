const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,

    clientsForOnlyComments:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'packersMoversClient'
    }
},
{timestamps:true});
const comm=mongoose.model('packersMoversComments',commentsSchema);
module.exports=comm;