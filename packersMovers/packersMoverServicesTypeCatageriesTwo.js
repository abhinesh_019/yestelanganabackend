const mongoose=require('mongoose');

const SchemaAreas= new mongoose.Schema({

    vehicleName:String,
    CustomerName:String,
    CustomerPlace:String,
    customerStartingPlace:String,
    customerEndingPlace:String,
    totalDistance:String,
    totalTimeTaken:String,
    itemName:String,
    totalPrice:String,
    customerImg:String,
    itemgLoadingImg1:String,
    itemgLoadingImg2:String,
    itemgLoadingImg3:String,
    itemgLoadingImg4:String,

    ClientsForServicesCustomersTwo:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'packersMoversServicesCustomersOne'
}
    
},
{timestamps:true});
const Area=mongoose.model('packersMoversServicesCustomersTwo',SchemaAreas);

module.exports=Area;

