const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'packersMoversLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('packersMoversArea',areaSchema);
module.exports=areas;