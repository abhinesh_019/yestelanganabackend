const mongoose=require('mongoose');

const gymSchema= new mongoose.Schema({

    genders:{type:String},
    gymClientsForGenders:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'gymClients'
}
},
{timestamps:true});

const hostelCatageriou=mongoose.model('gymGenders',gymSchema);
module.exports=hostelCatageriou;