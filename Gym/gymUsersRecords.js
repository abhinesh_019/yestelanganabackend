const mongoose=require('mongoose');

const gymSchema= new mongoose.Schema({

    name:{type:String}, //
    area:{type:String}, //
    coachName:{type:String},   
    aboutCoach:{type:String},
    goal:{type:String},  // 
    joinDate:{type:String},
    shortDescp1:{type:String},
    descriptions1:{type:String},
    presentDate:{type:String},
    shortDescp2:{type:String},
    descriptions2:{type:String},
    images1:{type:String},
   
 
    gymClientsForusersRecords:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'gymGenders'
}
},
{timestamps:true});

const hostelCatageriou=mongoose.model('gymUsersRecords',gymSchema);
module.exports=hostelCatageriou;