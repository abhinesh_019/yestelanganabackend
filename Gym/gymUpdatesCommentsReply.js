const mongoose=require('mongoose');

const SchemaCUpdatesCommentsReply= new mongoose.Schema({
 
    descriptions:String,
   
    gymClientsForUpdatesCommentsReply:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'gymUpdatesComments'
}
    
},
{timestamps:true});
const   updatesCommentsReply=mongoose.model('gymUpdatesCommentsReply',SchemaCUpdatesCommentsReply);

module.exports=updatesCommentsReply;

