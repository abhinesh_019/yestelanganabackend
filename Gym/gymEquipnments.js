const mongoose=require('mongoose');

const gymSchema= new mongoose.Schema({

    equiName:{type:String},
    trainerName:{type:String},
    yearsOfExp:{type:String},
    height:{type:String},
    weight:{type:String},
    description1:{type:String},
    description2:{type:String},
    images1:{type:String},
    images2:{type:String},
 
    gymClientsForEquipment:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'gymGenders'
}
},
{timestamps:true});

const hostelCatageriou=mongoose.model('gymEquipments',gymSchema);
module.exports=hostelCatageriou;