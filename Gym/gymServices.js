const mongoose=require('mongoose');

const gymSchema= new mongoose.Schema({

    serviceName:{type:String},
    trainerName:{type:String},
    yearsOfExp:{type:String},
    timmings:{type:String},
    trainerdes:{type:String},
    generalDescription:{type:String},
     images1:{type:String},
    images2:{type:String},
  
    gymClientsForServices:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'gymGenders'
}
},
{timestamps:true});

const hostelCatageriou=mongoose.model('gymServices',gymSchema);
module.exports=hostelCatageriou;