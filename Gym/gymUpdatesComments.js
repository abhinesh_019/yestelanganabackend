const mongoose=require('mongoose');

const SchemaCUpdatesComments= new mongoose.Schema({
 
    descriptions:String,
   
    gymClientsForUpdatesComments:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'gymUpdates'
}
    
},
{timestamps:true});
const   updatesComments=mongoose.model('gymUpdatesComments',SchemaCUpdatesComments);

module.exports=updatesComments;

