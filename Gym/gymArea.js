const mongoose=require('mongoose');

const gymSchemaAreas= new mongoose.Schema({

    area:String,
    
    GymLocations:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'gymLocations'
}
    
},
{timestamps:true});

const gymArea=mongoose.model('GymAreas',gymSchemaAreas);
module.exports=gymArea;