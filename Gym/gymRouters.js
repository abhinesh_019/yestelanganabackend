const express= require('express');
var router = express.Router();
const userControllers = require('../Gym/gymControllers');
var middleware = require('../middleware/middleware')

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();



//  *************** LOCATIONS ********
router.route('/gymlocations')
.get(userControllers.gymlocationsget)
 .post(userControllers.gymlocationspost);
//  ***********************

//  ************ Area ***********
router.route('/gymAreas/:locationsId')
.get(userControllers.gymAreasget)

router.route('/gymAreas/:gymLocationsId')
 .post(userControllers.gymAreasposts);

//  ***********************


// ******************* Clients details ******************
router.route('/gymClientsget/:AreaId') //done-----------
.get(userControllers.index)

router.route('/gymClientsAreas/:AreaId') //done------
.get(userControllers.usersgetcoutarea)
 
router.route('/gymClientsAreasAll/:AreaId')
.get(userControllers.usersgetcoutareaAll)


router.route('/gymClientsAllCount')  //done
.get(userControllers.totalclients)  

router.route('/gymClientsCount/:AreaId')  //done
.get(userControllers.userscount)


router.route('/gymClientsGetind/:clientsId') //done
.get(userControllers.indexs)


router.route('/gymClientsPost/:AreaId') //done-------
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************


//  ************ categories ***********
router.route('/gymCategorie/:gymClientsId')
.get(userControllers.gymCategoriesget)
router.route('/gymCategoriesp/:gymClientsId')
 .post(userControllers.gymCategoriesposts);
//  ***********************


//  ************ services ***********
router.route('/gymServices/:gymCatId')
.get(userControllers.gymServicesget)
router.route('/gymServicesCount/:gymCatId')
.get(userControllers.gymServicesgetCount)
router.route('/gymServicesind/:gymServiceId')
.get(userControllers.gymServicesgetind)
router.route('/gymServicesP/:gymCatId')
 .post(multipartMiddleware,userControllers.gymServicesposts);
//  ***********************

//  ************ EQUIPMENTS  ***********
router.route('/gymEquipments/:gymCatId')
.get(userControllers.gymEquipmentsget)
router.route('/gymEquipmentsind/:gymEquiId')
.get(userControllers.gymEquipmentsEquiget)
router.route('/gymEquipmentsCount/:gymCatId')
.get(userControllers.gymEquipmentsCount)
router.route('/gymEquipmentsP/:gymCatId')
 .post(multipartMiddleware,userControllers.gymEquipmentsposts);

//  ***********************

//  ************ Ficilties  ***********
router.route('/gymFicilties/:gymCatId')
.get(userControllers.gymFiciltiesget)
router.route('/gymFiciltiesCount/:gymCatId')
.get(userControllers.gymFiciltiesCount)
router.route('/gymFiciltiesP/:gymCatId')
 .post(multipartMiddleware,userControllers.gymFiciltiesposts);

//  ***********************

//  ************ users  ***********
router.route('/gymusers/:gymCatId')
.get(userControllers.gymusersget)
router.route('/gymusersid/:gymusersId')
.get(userControllers.gymusersidget)
router.route('/gymusersCount/:gymCatId')
.get(userControllers.gymusersCount)
router.route('/gymusersP/:gymCatId')
 .post(multipartMiddleware,userControllers.gymusersposts);

//  ***********************

// ****************** only updates details ****************
router.route('/gymupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/gymupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/gymupdatesgetpostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/gymupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/gymupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/gymupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/gymupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/gymupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/gymeuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/gymCeuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)


// *************************************** AddsOne ************************************* 
router.route('/gymAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/gymAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/gymAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/gymAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/gymAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/gymAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/gymAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/gymAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/gymAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/gymAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/gymAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/gymAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/gymAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/gymAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/gymAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/gymAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/gymAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/gymAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/gymAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/gymAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/gymAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/gymAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/gymAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/gymAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/gymAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/gymAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/gymAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
