const mongoose=require('mongoose');

const SchemaCUpdates= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,
   
 
    gymClientsForGeneralComments:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'gymClients'
}
    
},
{timestamps:true});
const   updates=mongoose.model('gymGeneralComments',SchemaCUpdates);

module.exports=updates;

