var gymC = require('../Gym/gymCatageries')
var gymL = require('../Gym/gymLocations')
var gymA = require('../Gym/gymArea')
var clientServices=require('../Gym/gymServices')
var clientsUpdates=require('../Gym/gymUpdates')
var clientsUpdatesComm=require('../Gym/gymUpdatesComments')
var clientsUpdatesCommReply=require('../Gym/gymUpdatesCommentsReply')
var clientsOnlyComments=require('../Gym/gymOnlyCommnets')
var equip=require('../Gym/gymEquipnments')
var users=require('../Gym/gymUsersRecords')
var fecit=require('../Gym/gymFicilities')
var clients = require('../Gym/gymClients')
var babycaresAddsOne=require('../Gym/fightsAddsOnea');
var babycaresAddsTwo=require('../Gym/fightsAddsTwoa');
var babycaresAddsThree=require('../Gym/fightsAddsThreea');
var babycaresAddsFour=require('../Gym/fightsAddsFoura');
 
var babycaresAddsOneLoc=require('../Gym/fightsAddsOnel');
var babycaresAddsTwoLoc=require('../Gym/fightsAddsTwol');
var babycaresAddsThreeLoc=require('../Gym/fightsAddsThreel');
var babycaresAddsFourLoc=require('../Gym/fightsAddsFourl');
var babycaresAddsFiveLoc=require('../Gym/fightsAddsFivel');

 var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
const fileService = require('../Gym/gymFielServer');

var async = require('async');
 
 
module.exports = {
// ********************categories*****************
gymCategoriesposts:async (req,res) => {
    const { gymClientsId } = req.params;
    req.body.gymClientsForGenders = gymClientsId
    const generalcomm = new gymC(req.body);
    await generalcomm.save();
  res.status(201).json(generalcomm);
},
gymCategoriesget:async (req, res, next) => {
    const { gymClientsId } = req.params;
    const users = await gymC.find({gymClientsForGenders:gymClientsId})
    res.status(200).json(users);   
   },
 

// ******************************************

    gymlocationspost: async(req,res,next)=>{
        const { gymCatId } = req.params;
        req.body.gymCatagerie = gymCatId
         const userscomm = new gymL(req.body);
        await userscomm.save();
        console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
        
           res.status(201).json(userscomm);
  },

    gymlocationsget:async (req, res, next) => {

        const { gymCatId } = req.params;
        const usersposts = await gymL.find({gymCatagerie:gymCatId}).sort({"area":-1})
        res.status(200).json(usersposts);
     },

    gymAreasposts : async(req,res,next)=>{
        const { gymLocationsId } = req.params;
        req.body.GymLocations = gymLocationsId
         const userscomm = new gymA(req.body);
        await userscomm.save();
        console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
        
           res.status(201).json(userscomm);
  },

  
  
    gymAreasget:async (req, res, next) => {

        const { locationsId } = req.params;
        

        const usersposts = await gymA.find({GymLocations:locationsId}).sort({"area":1})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },

// ************************************************ Clients Updates ***************************************
 
newUser: (req, res, next) => {
    const { AreaId } = req.params;
    req.body.gymAreaForClients = AreaId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null,data.Location);
            })
        },
        function(callback) {
            fileService.uploadImage(req.files.images4, function (err, data) {
                allData.images4 = data.Location;
                callback(null, data.Location);
            })
        },
 
        function (callback) {
            fileService.uploadImage(req.files.images5, function (err, data) {
                allData.images5 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images6, function (err, data) {
                allData.images6 = data.Location;
                callback(null, data.Location);
            })
        },
         

    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clients.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                }
            });
        }
 });
},




index:async (req, res, next) => {

    const { AreaId } = req.params;
    let search=req.query.search;

    const usersposts = await clients.find({gymAreaForClients:AreaId,$or:[
        {subArea:new RegExp(search, "gi")},
        {pincode:new RegExp(search, "gi")}, 
        {gymName:new RegExp(search, "gi")},
        {landmark:new RegExp(search, "gi")},
        {keyWords:new RegExp(search, "gi")},
    ]
}).sort({"subArea":1})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

usersgetcoutarea: async (req, res, next) => {
    const { AreaId } = req.params;

    const users = await clients.aggregate([{$match:{gymAreaForClients:ObjectId(AreaId)}},

        {"$group" : {_id:{subArea:"$subArea"}, count:{$sum:1}}},{$sort:{"subArea":1}}
     ])
    
    res.status(200).json(users);
 },
 
userscount:async (req, res, next) => {
     const { AreaId } = req.params;
     const usersposts = await clients.find({gymAreaForClients:AreaId}).count()
    res.status(200).json(usersposts);
 },
 usersgetcoutareaAll:async (req, res, next) => {
    const { AreaId } = req.params;
    const usersposts = await clients.aggregate([{$match:{gymAreaForClients:ObjectId(AreaId)}},

        {"$group" : {_id:{distict:"$distict"}, count:{$sum:1}}},{$sort:{"subArea":1}}
     ])
   res.status(200).json(usersposts);
},


totalclients:async (req, res, next) => {
  const usersposts = await clients.find({}).count()
    res.status(200).json(usersposts);
  
},
 
indexs:async (req, res, next) => {

    const { clientsId } = req.params;
    const usersposts = await clients.findById(clientsId) 
    res.status(200).json(usersposts);
  
},
 
// ***********************************************************************
// ************************************************ Clients SERVICES ***************************************
 
gymServicesposts: (req, res, next) => {
    const { gymCatId } = req.params;
    req.body.gymClientsForServices = gymCatId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },
     ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clientServices.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                }
            });
        }
 });
},
 
gymServicesget:async (req, res, next) => {

    const { gymCatId } = req.params;
    const usersposts = await clientServices.find({gymClientsForServices:gymCatId}).sort({"serviceName":1})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

gymServicesgetCount:async (req, res, next) => {

    const { gymCatId } = req.params;
    const usersposts = await clientServices.find({gymClientsForServices:gymCatId}).count()
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},
gymServicesgetind:async (req, res, next) => {

    const { gymServiceId } = req.params;
    const usersposts = await clientServices.findById(gymServiceId)
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},
  
// ***********************************************************************

// ************************************************ Clients EQUIPENTS ***************************************
 
gymEquipmentsposts: (req, res, next) => {
    const { gymCatId } = req.params;
    req.body.gymClientsForEquipment = gymCatId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },
     ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            equip.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                }
            });
        }
 });
},
 
gymEquipmentsget:async (req, res, next) => {

    const { gymCatId } = req.params;
    const usersposts = await equip.find({gymClientsForEquipment:gymCatId}).sort({"serviceName":1})
    res.status(200).json(usersposts);
 
 },
gymEquipmentsEquiget:async (req, res, next) => {

    const { gymEquiId } = req.params;
    const usersposts = await equip.findById(gymEquiId)
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

gymEquipmentsCount:async (req, res, next) => {

    const { gymCatId } = req.params;
    const usersposts = await equip.find({gymClientsForEquipment:gymCatId}).count()
    res.status(200).json(usersposts);
 
 },
  
// ***********************************************************************

// ************************************************ Clients Ficilities ***************************************
 
gymFiciltiesposts: (req, res, next) => {
    const { gymCatId } = req.params;
    req.body.gymClientsForFicilitites = gymCatId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },
      ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            fecit.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                }
            });
        }
 });
},
 
gymFiciltiesget:async (req, res, next) => {

    const { gymCatId } = req.params;
    const usersposts = await fecit.find({gymClientsForFicilitites:gymCatId}).sort({"serviceName":1})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

gymFiciltiesCount:async (req, res, next) => {

    const { gymCatId } = req.params;
    const usersposts = await fecit.find({gymClientsForFicilitites:gymCatId}).count()
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},
  
// ***********************************************************************


// ************************************************ users ***************************************
 
gymusersposts: (req, res, next) => {
    const { gymCatId } = req.params;
    req.body.gymClientsForusersRecords = gymCatId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

        

      ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            users.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                }
            });
        }
 });
},
 
gymusersget:async (req, res, next) => {

    const { gymCatId } = req.params;
    const usersposts = await users.find({gymClientsForusersRecords:gymCatId}).sort({"serviceName":1})
    res.status(200).json(usersposts);
},
gymusersidget:async (req, res, next) => {

    const { gymusersId } = req.params;
    const usersposts = await users.findById(gymusersId)
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

gymusersCount:async (req, res, next) => {
 const { gymCatId } = req.params;
    const usersposts = await users.find({gymClientsForusersRecords:gymCatId}).count()
    res.status(200).json(usersposts);
 },
  
// ***********************************************************************


// ****************users updates*************

UsersUpdates: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.gymClientsForUpdates = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.viedoes,"req.files.viedoes 302");
            if(req.files.viedoes){
                fileService.uploadImage(req.files.viedoes, function (err, data) {
                    allData.viedoes = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clientsUpdates.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

UsersGet:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({gymClientsForUpdates:ClientsId}).sort({"createdAt:":1})

        res.status(200).json(users);
       console.log("users****", users);
},

UsersGetCounts:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({gymClientsForUpdates:ClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
//admin users updates comments
updatesCommentsposts:async (req, res, next) => {

const { updatesId } = req.params;
  req.body.gymClientsForUpdatesComments = updatesId 
const userscomm = new clientsUpdatesComm(req.body);

await userscomm.save();
console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
 res.status(201).json(userscomm);
},

updatesCommentsGet:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComm.find({gymClientsForUpdatesComments:updatesId}).sort({"createdAt:":1})
    res.status(200).json(users); 
},
updatesCommentsGetcounts:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComm.find({gymClientsForUpdatesComments:updatesId}).count()
    res.status(200).json(users);
},

updatesCommentsReplyposts:async (req, res, next) => {
const { updatesCommId } = req.params;
  req.body.gymClientsForUpdatesCommentsReply = updatesCommId 
const userscomm = new clientsUpdatesCommReply(req.body);
await userscomm.save();
 res.status(201).json(userscomm);
},

updatesCommentsReplyGet:async (req, res, next) => {

const { updatesCommId } = req.params;

    const users = await clientsUpdatesCommReply.find({gymClientsForUpdatesCommentsReply:updatesCommId})

    res.status(200).json(users);
   console.log("users**** 399", users);
},

// ****************only comments *******************
usersCommentsposts : async (req,res) => {
const { ClientsId } = req.params;
req.body.gymClientsForGeneralComments = ClientsId
const generalcomm = new clientsOnlyComments(req.body);
await generalcomm.save();
res.status(201).json(generalcomm);
},
usersCommentsget:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await clientsOnlyComments.find({gymClientsForGeneralComments:ClientsId})
res.status(200).json(users);   
},
usersCommentsgetCounts:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await clientsOnlyComments.find({gymClientsForGeneralComments:ClientsId}).count()
    res.status(200).json(users);   
    },


    // *************************************AddsOne*****************
 

    babycareAddsOneP: async (req, res, next) => {
 
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsOnea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImg, function (err, data) {
                    allData.addsOneImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOne.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOne.find({babycareAreaForAddsOnea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOne.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsTwo*****************
     
     
    babycareAddsTwoP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsTwoa = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImg, function (err, data) {
                    allData.addsTwoImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwo.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
    babycareAddsTwoGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwo.find({babycareAreaForAddsTwoa:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwo.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsThree*****************
    
    
    babycareAddsThreeP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsThreea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImg, function (err, data) {
                    allData.addsThreeImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThree.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThree.find({babycareAreaForAddsThreea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThree.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsFour*****************
    
     
     
    babycareAddsFourP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsFoura = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImg, function (err, data) {
                    allData.addsFourImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFour.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFour.find({babycareAreaForAddsFoura:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFour.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
     
    // *************************************AddsOneLoc*****************
    
    
    babycareAddsOneLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocOne = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImgLoc, function (err, data) {
                    allData.addsOneImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOneLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOneLoc.find({babycaresAreaForAddsLocOne:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOneLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsTwoLoc*****************
    
    
    babycareAddsTwoLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocTwo = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImgLoc, function (err, data) {
                    allData.addsTwoImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwoLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsTwoLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwoLoc.find({babycaresAreaForAddsLocTwo:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwoLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsThreeLoc*****************
    
    
    babycareAddsThreeLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocThree = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImgLoc, function (err, data) {
                    allData.addsThreeImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThreeLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThreeLoc.find({babycaresAreaForAddsLocThree:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThreeLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFourLoc*****************
    
    
    babycareAddsFourLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFour = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImgLoc, function (err, data) {
                    allData.addsFourImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFourLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFourLoc.find({babycaresAreaForAddsLocFour:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFourLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFiveLoc*****************
    
    
    babycareAddsFiveLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFive = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFiveImgLoc, function (err, data) {
                    allData.addsFiveImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFiveLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFiveLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFiveLoc.find({babycaresAreaForAddsLocFive:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFiveDeleteLoc: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFiveLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    }, 
    }
    