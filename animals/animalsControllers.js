var animalsL = require('../animals/animalsLocations'); 
var animalsA = require('../animals/animalsArea');
var animalsT = require('../animals/animalsTypes');
var animalsTD = require('../animals/animalsDescriptions');
var animalsUpdates = require('../animals/animalsUpdates');
var animalsGenenralComments = require('../animals/animalsOnlyComments');
var animalsUpdatesComments = require('../animals/animalsUpdatesComments');
var animalsUpdatesCommentsReply = require('../animals/animalsUpdatesCommentsReply');
var animalsClients = require('../animals/animalsClients'); 
var animalsNecessaries=require('../animals/animalsNecessaries');
var animalsFoods = require('../animals/animalsFoods');
var animalsAddsOne=require('../animals/animalsAddsOne');
var animalsAddsTwo=require('../animals/animalsAddsTwo');
var animalsAddsThree=require('../animals/animalsAddsThree');
var animalsAddsFour=require('../animals/animalsAddsFour');
 
var animalsAddsOneLoc=require('../animals/animalsAddsLocOne');
var animalsAddsTwoLoc=require('../animals/animalsAddsLocTwo');
var animalsAddsThreeLoc=require('../animals/animalsAddsLocThree');
var animalsAddsFourLoc=require('../animals/animalsAddsLocFour');
var animalsAddsFiveLoc=require('../animals/animalsAddsLocFive');
 
const fileService = require('../animals/animalsServer');
var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 
var async = require('async')

 
module.exports = {

    animalsLocationsget: async (req, res, next) => {

         const users = await animalsL.aggregate([

            {$sort:{"locations":1}}
            
    ])
 
         res.status(200).json(users);
        console.log("users****", users);

    },

    animalsLocationsPosts: async (req,res,next)=>{
        const locs=  req.body;
        await animalsL.create(locs, function (err, data) {

            console.log(this.locs, "this.alldata");


            if (err) {
                console.log('Error in Saving user: ' + err);
            } else {

                res.status(201).json({ status: true, message: "user added sucessfully"});
            }

        });
    
    },

    // ******************* areas **********************
    animalsareasposts : async(req,res,next)=>{
        const { animalslocationsId } = req.params;
        req.body.animalsLocation= animalslocationsId
         const userscomm = new animalsA(req.body);
        await userscomm.save(); 
           res.status(201).json(userscomm);
  },


  animalsareaget:async(req,res,next)=>{
    const { animalslocationsId } = req.params;
    req.body.animalsLocation= animalslocationsId
    const users = await animalsA.find({animalsLocation :animalslocationsId})
     res.status(200).json(users);
 },

// **************************************************************************

// **************************************** Clients **********************************


animalsClientsPost: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForClients = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },
       function (callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images4, function (err, data) {
                allData.images4 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images5, function (err, data) {
                allData.images5 = data.Location;
                callback(null, data.Location);
            })
        },
 
        function (callback) {
            fileService.uploadImage(req.files.images6, function (err, data) {
                allData.images6 = data.Location;
                callback(null, data.Location);
            })
        },
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsClients.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},


animalsClientsGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
    let search=req.query.search;

    const usersposts = await animalsClients.find({animalsAreaForClients:animalsAreaId,$or:[
        {area:new RegExp(search, "gi")},
        {pincode:new RegExp(search, "gi")}, 
        {shopname:new RegExp(search, "gi")},
        {landmark:new RegExp(search, "gi")},
        
    ]
}).sort({"area":1})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

 
animalsClientsIdGet:async (req, res, next) => {
    const { animalsClientId } = req.params;
    const usersposts = await animalsClients.findById(animalsClientId) 
    res.status(200).json(usersposts);
    console.log(usersposts,"usersposts");
},

 
animalsClientsGetCountsByArea:async (req, res, next) => {
    const { animalsAreaId } = req.params;
    const usersposts = await animalsClients.find({animalsAreaForClients:animalsAreaId}).count()
    res.status(200).json(usersposts);
 },

animalsGetonlyAll:async (req, res, next) => {
     const usersposts = await animalsClients.find({}).count()
    res.status(200).json(usersposts);
 },
 animalsFamousAreas: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const users = await animalsClients.aggregate([{$match:{animalsAreaForClients:ObjectId(animalsAreaId)}},

        {"$group" : {_id:{area:"$area"}, count:{$sum:1}}},{$sort:{"area":1}}])
    
    res.status(200).json(users);
   console.log("users****", users);

},
animalsFamousLocations: async (req, res, next) => {
 
    const users = await animalsClients.aggregate([
		{"$group" : {_id:{distict:"$distict"}, count:{$sum:1}}},{$sort : {"distict":1}}])
	
    res.status(200).json(users);
   console.log("users****", users);

},
// **************************************************************************
// ***************************************************** animals types *******************************************************

animalsTypesGet:async(req,res,next)=>{
    const { animalsClientsId } = req.params;
    req.body.animalsClientsForTypes= animalsClientsId
    const users = await animalsT.find({animalsClientsForTypes : animalsClientsId}).sort({"animalsTypes":1})
     res.status(200).json(users);
 },
 animalsTypesGetId:async (req, res, next) => {

    const { animalsTypesId } = req.params;
 
    const usersposts = await animalsT.findById(animalsTypesId)
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},
animalsTypesPost: async(req,res,next)=>{
    const { animalsClientsId } = req.params;
    req.body.animalsClientsForTypes= animalsClientsId
     const userscomm = new animalsT(req.body);
    await userscomm.save();
       res.status(201).json(userscomm);
},

// ********************************************************************************************************

 // ********************* post animals descriptions *******************
 animalsTypesdescPost: async (req, res, next) => {
 
    const { animalsTypeId } = req.params;
    req.body.animalsTypesForDescriptions = animalsTypeId
    let allData = req.body
     async.parallel([

        function (callback) {
            if(req.files.images1){
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
               })
        } else{
            callback(null, null);   
        }
        },

        function (callback) {
            if(req.files.images2){
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        } else{
            callback(null, null);   
        }
        },

        function (callback) {
            if(req.files.images3){
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null, data.Location);
            })
        } else{
            callback(null, null);   
        }
        },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsTD.create(allData, function (err, data) {
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully" });
                }

            });
        }
  });
},

animalsTypesdesGet: async (req, res, next) => {
     const { animalsTypeId } = req.params;
        const users = await animalsTD.find({animalsTypesForDescriptions:animalsTypeId}).sort({"name":1})
        res.status(200).json(users);
 },
 animalsTypesdesGetCont: async (req, res, next) => {
    const { animalsTypeId } = req.params;
       const users = await animalsTD.find({animalsTypesForDescriptions:animalsTypeId}).count()
       res.status(200).json(users);
},
animalsTypesdesGetContDetails: async (req, res, next) => {
    const { animalsTypeId } = req.params;
       const users = await animalsTD.aggregate([{$match:{animalsTypesForDescriptions:ObjectId(animalsTypeId)}},

        {"$group" : {_id:{name:"$name"}, count:{$sum:1}}},{$sort:{"area":1}}])
       res.status(200).json(users);
},
animalsTypesDesGetId: async (req, res, next) => {
     const { animalsTypesdesId } = req.params;
      const users = await animalsTD.findById(animalsTypesdesId)
        res.status(200).json(users);
 },
 
// *************************************
    // ****************users updates*************

    animalsUsersUpdates: async (req, res, next) => {
 
        const { animalsClientsId } = req.params;
        req.body.animalsClientsForUpdates = animalsClientsId
        let allData = req.body 
 
        async.parallel([
    
    
            function (callback) {
                console.log(req.files.images,"req.files.images 273");
                if(req.files.images){
                    fileService.uploadImage(req.files.images, function (err, data) {
                        allData.images = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
                
            },
            
            function (callback) {
                console.log(req.files.viedoes,"req.files.viedoes 302");
                if(req.files.viedoes){
                    fileService.uploadImage(req.files.viedoes, function (err, data) {
                        allData.viedoes = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
              },
     
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                animalsUpdates.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }  });  }  });
    },

    animalsUsersGet:async (req, res, next) => {
    
        const { animalsClientsId } = req.params;
    
            const users = await animalsUpdates.find({animalsClientsForUpdates:animalsClientsId})
    
            res.status(200).json(users);
           console.log("users****", users);
    },

    animalsUsersGetCounts:async (req, res, next) => {
    
        const { animalsClientsId } = req.params;
    
            const users = await animalsUpdates.find({animalsClientsForUpdates:animalsClientsId}).count()
    
            res.status(200).json(users);
           console.log("users****", users);
    },
//admin users updates comments
animalsupdatesCommentsposts:async (req, res, next) => {
 
    const { animalsupdatesId } = req.params;
      req.body.animalsUpdatesForComments = animalsupdatesId 
    const userscomm = new animalsUpdatesComments(req.body);

    await userscomm.save();
 console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
     res.status(201).json(userscomm);
 },

 animalsupdatesCommentsGet:async (req, res, next) => {
     const { animalsupdatesId } = req.params;
       const users = await animalsUpdatesComments.find({animalsUpdatesForComments:animalsupdatesId})
        res.status(200).json(users); 
},
animalsupdatesCommentsGetcounts:async (req, res, next) => {
     const { animalsupdatesId } = req.params;
       const users = await animalsUpdatesComments.find({animalsUpdatesForComments:animalsupdatesId}).count()
        res.status(200).json(users);
},

animalsupdatesCommentsReplyposts:async (req, res, next) => {
   const { animalsupdatesCommId } = req.params;
      req.body.animalsUpdatesCommForReply = animalsupdatesCommId 
    const userscomm = new animalsUpdatesCommentsReply(req.body);
 await userscomm.save();
 console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
   res.status(201).json(userscomm);
},

animalsupdatesCommentsReplyGet:async (req, res, next) => {
    
    const { animalsupdatesCommId } = req.params;

        const users = await animalsUpdatesCommentsReply.find({animalsUpdatesCommForReply:animalsupdatesCommId})

        res.status(200).json(users);
       console.log("users**** 399", users);
},

// ****************only comments *******************
animalsusersCommentsposts : async (req,res) => {
    const { animalsClientsId } = req.params;
    req.body.animalsClientsForGeneralComments = animalsClientsId
    const generalcomm = new animalsGenenralComments(req.body);
    await generalcomm.save();
  res.status(201).json(generalcomm);
},
animalsusersCommentsget:async (req, res, next) => {
 const { animalsClientsId } = req.params;
 const users = await animalsGenenralComments.find({animalsClientsForGeneralComments:animalsClientsId})
 res.status(200).json(users);   
},
animalsusersCommentsgetCounts:async (req, res, next) => {
  const { animalsClientsId } = req.params;
 const users = await animalsGenenralComments.find({animalsClientsForGeneralComments:animalsClientsId}).count()
 res.status(200).json(users); 
},
// ***************************** animals food *****************
 
animalsfoodPosts: async (req, res, next) => {
 
    const { animalsClientsId } = req.params;
    req.body.animalsForFood = animalsClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
             if(req.files.foodImages1){
                fileService.uploadImage(req.files.foodImages1, function (err, data) {
                    allData.foodImages1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
             if(req.files.foodImages2){
                fileService.uploadImage(req.files.foodImages2, function (err, data) {
                    allData.foodImages2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            if(req.files.foodImages3){
               fileService.uploadImage(req.files.foodImages3, function (err, data) {
                   allData.foodImages3 = data.Location;
                   callback(null, data.Location);
               })
             }
           else{
               callback(null, null);   
           }
         },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsFoods.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},
animalsfoodGet:async (req, res, next) => {

    const { animalsClientsId } = req.params;

        const users = await animalsFoods.find({animalsForFood:animalsClientsId}).sort({"foodType":1})

        res.status(200).json(users);
       console.log("users****", users);
},

animalsfoodGetCounts:async (req, res, next) => {

    const { animalsClientsId } = req.params;

        const users = await animalsFoods.find({animalsForFood:animalsClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},

// ***************************** animals food *****************
 
animalsNecessariesPosts: async (req, res, next) => {
 
    const { animalsClientsId } = req.params;
    req.body.animalsForNecessaries = animalsClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
             if(req.files.nessariesImages){
                fileService.uploadImage(req.files.nessariesImages, function (err, data) {
                    allData.nessariesImages = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
         
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsNecessaries.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},
animalsNecessariesGets:async (req, res, next) => {

    const { animalsClientsId } = req.params;

        const users = await animalsNecessaries.find({animalsForNecessaries:animalsClientsId}).sort({"foodType":1})

        res.status(200).json(users);
       console.log("users****", users);
},

animalsNecessariesCounts:async (req, res, next) => {

    const { animalsClientsId } = req.params;

        const users = await animalsNecessaries.find({animalsForNecessaries:animalsClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},

// *************************************AddsOne*****************


animalsAddsOneP: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForAddsOne = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsOneImg, function (err, data) {
                allData.addsOneImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsAddsOne.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
animalsAddsOneGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
 
    const usersposts = await animalsAddsOne.find({animalsAreaForAddsOne:animalsAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

animalsAddsOneDelete: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const result = await animalsAddsOne.findByIdAndRemove(animalsAreaId);

    res.status(200).json(result)
},

// *************************************AddsTwo*****************


animalsAddsTwoP: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForAddsTwo = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsTwoImg, function (err, data) {
                allData.addsTwoImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsAddsTwo.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
animalsAddsTwoGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
 
    const usersposts = await animalsAddsTwo.find({animalsAreaForAddsTwo:animalsAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

animalsAddsTwoDelete: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const result = await animalsAddsTwo.findByIdAndRemove(animalsAreaId);

    res.status(200).json(result)
},

// *************************************AddsThree*****************


animalsAddsThreeP: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForAddsThree = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsThreeImg, function (err, data) {
                allData.addsThreeImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsAddsThree.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
animalsAddsThreeGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
 
    const usersposts = await animalsAddsThree.find({animalsAreaForAddsThree:animalsAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

animalsAddsThreeDelete: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const result = await animalsAddsThree.findByIdAndRemove(animalsAreaId);

    res.status(200).json(result)
},

// *************************************AddsFour*****************


animalsAddsFourP: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForAddsFour = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsFourImg, function (err, data) {
                allData.addsFourImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsAddsFour.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
animalsAddsFourGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
 
    const usersposts = await animalsAddsFour.find({animalsAreaForAddsFour:animalsAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

animalsAddsFourDelete: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const result = await animalsAddsFour.findByIdAndRemove(animalsAreaId);

    res.status(200).json(result)
},

 
// *************************************AddsOneLoc*****************


animalsAddsOneLocP: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForAddsLocOne = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsOneImgLoc, function (err, data) {
                allData.addsOneImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsAddsOneLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
animalsAddsOneLocGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
 
    const usersposts = await animalsAddsOneLoc.find({animalsAreaForAddsLocOne:animalsAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

animalsAddsOneLocDelete: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const result = await animalsAddsOneLoc.findByIdAndRemove(animalsAreaId);

    res.status(200).json(result)
},
// *************************************AddsTwoLoc*****************


animalsAddsTwoLocP: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForAddsLocTwo = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsTwoImgLoc, function (err, data) {
                allData.addsTwoImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsAddsTwoLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
animalsAddsTwoLocGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
 
    const usersposts = await animalsAddsTwoLoc.find({animalsAreaForAddsLocTwo:animalsAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

animalsAddsTwoLocDelete: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const result = await animalsAddsTwoLoc.findByIdAndRemove(animalsAreaId);

    res.status(200).json(result)
},
// *************************************AddsThreeLoc*****************


animalsAddsThreeLocP: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForAddsLocThree = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsThreeImgLoc, function (err, data) {
                allData.addsThreeImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsAddsThreeLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
animalsAddsThreeLocGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
 
    const usersposts = await animalsAddsThreeLoc.find({animalsAreaForAddsLocThree:animalsAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

animalsAddsThreeLocDelete: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const result = await animalsAddsThreeLoc.findByIdAndRemove(animalsAreaId);

    res.status(200).json(result)
},
// *************************************AddsFourLoc*****************


animalsAddsFourLocP: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForAddsLocFour = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsFourImgLoc, function (err, data) {
                allData.addsFourImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsAddsFourLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
animalsAddsFourLocGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
 
    const usersposts = await animalsAddsFourLoc.find({animalsAreaForAddsLocFour:animalsAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

animalsAddsFourLocDelete: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const result = await animalsAddsFourLoc.findByIdAndRemove(animalsAreaId);

    res.status(200).json(result)
},
// *************************************AddsFiveLoc*****************


animalsAddsFiveLocP: async (req, res, next) => {
 
    const { animalsAreaId } = req.params;
    req.body.animalsAreaForAddsLocFive = animalsAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsFiveImgLoc, function (err, data) {
                allData.addsFiveImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animalsAddsFiveLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
animalsAddsFiveLocGet:async (req, res, next) => {

    const { animalsAreaId } = req.params;
 
    const usersposts = await animalsAddsFiveLoc.find({animalsAreaForAddsLocFive:animalsAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

animalsAddsFiveDeleteLoc: async (req, res, next) => {
    const { animalsAreaId } = req.params;

    const result = await animalsAddsFiveLoc.findByIdAndRemove(animalsAreaId);

    res.status(200).json(result)
}, 
}