const mongoose=require('mongoose');
 
const animalsSchema= new mongoose.Schema(
    {
        addsThreeLink:{type:String},
        addsThreeImg:{type:String},   
           animalsAreaForAddsThree:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'animalsArea'
                     }
 },

{timestamps:true}

);

const animals=mongoose.model('animalsAddsThree',animalsSchema);
module.exports=animals;