const mongoose=require('mongoose');
 
const animalsSchema= new mongoose.Schema(
    {
        addsTwoLinkLoc:{type:String},
        addsTwoImgLoc:{type:String},   
           animalsAreaForAddsLocTwo:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'animalslocations'
                     }
 },

{timestamps:true}

);

const animals=mongoose.model('animalsAddsLocTwo',animalsSchema);
module.exports=animals;