const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    foodType:String,
    foodQuantity:String,
    foodTimmings:String,
    foodPrecations:String,
    foodStorages:String,
    foodImages1:String,
    foodImages2:String,
    foodImages3:String,
    foodImagesDes1:String,
    foodImagesDes2:String,
    foodImagesDes3:String,

    animalsForFood:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'animalsTypes'
    }
},
{timestamps:true});
const comm=mongoose.model('animalsFoods',commentsSchema);
module.exports=comm;