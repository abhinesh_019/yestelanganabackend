const mongoose=require('mongoose');
const animalsDesSchema= new mongoose.Schema({

    name:String,
    age:String,
    food:String,
    precautions:String,
    descriptions:String,
    gender:String,
    price:String,
    images1:String,
    images2:String,
    images3:String,
    images1des:String,
    images2des:String,
    images3des:String,


    animalsTypesForDescriptions:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'animalsTypes'
}
   
},
{timestamps:true});
const description=mongoose.model('animalsDescriptions',animalsDesSchema);
module.exports=description;