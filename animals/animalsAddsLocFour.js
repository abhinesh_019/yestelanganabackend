const mongoose=require('mongoose');
 
const animalsSchema= new mongoose.Schema(
    {
        addsFourLinkLoc:{type:String},
        addsFourImgLoc:{type:String},   
           animalsAreaForAddsLocFour:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'animalslocations'
                     }
 },

{timestamps:true}

);

const animals=mongoose.model('animalsAddsLocFour',animalsSchema);
module.exports=animals;