const mongoose=require('mongoose');
 
const animalsSchema= new mongoose.Schema(
    {
        addsTwoLink:{type:String},
        addsTwoImg:{type:String},   
           animalsAreaForAddsTwo:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'animalsArea'
                     }
 },

{timestamps:true}

);

const animals=mongoose.model('animalsAddsTwo',animalsSchema);
module.exports=animals;