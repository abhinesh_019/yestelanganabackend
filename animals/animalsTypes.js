const mongoose=require('mongoose');
const animalsTpesSchema= new mongoose.Schema({

    animalsTypes:String,

    animalsClientsForTypes:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'animalsClients'
}
   
},
{timestamps:true});
const typesd=mongoose.model('animalsTypes',animalsTpesSchema);
module.exports=typesd;