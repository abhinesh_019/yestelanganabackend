const mongoose=require('mongoose');
 
const animalsSchema= new mongoose.Schema(
    {
        addsOneLinkLoc:{type:String},
        addsOneImgLoc:{type:String},   
           animalsAreaForAddsLocOne:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'animalslocations'
                     }
 },

{timestamps:true}

);

const animals=mongoose.model('animalsAddsLocOne',animalsSchema);
module.exports=animals;