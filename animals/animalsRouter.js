const express= require('express');
var router = express.Router();
const userControllers = require('../animals/animalsControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

// *************************************** locations ************************************* 

router.route('/animalsLocations')
.get(userControllers.animalsLocationsget)
.post(userControllers.animalsLocationsPosts);
 // **************************************************************************** 

// *************************************** area ************************************* 

router.route('/animalsAreas/:animalslocationsId')
.get(userControllers.animalsareaget)
router.route('/animalsArea/:animalslocationsId')
.post(userControllers.animalsareasposts);
// **************************************************************************** 

// *************************************** Clients ************************************* 
router.route('/animalsClients/:animalsAreaId')
.get(userControllers.animalsClientsGet)

router.route('/animalsClientInd/:animalsClientId')
.get(userControllers.animalsClientsIdGet)

router.route('/animalsClientsCountsByArea/:animalsAreaId')
.get(userControllers.animalsClientsGetCountsByArea)

router.route('/animalsCountsAreas/:animalsAreaId') 
.get(userControllers.animalsFamousAreas)

router.route('/animalsCountsLocations') 
.get(userControllers.animalsFamousLocations)

router.route('/animalsClientsAll')
.get(userControllers.animalsGetonlyAll)
 
  router.route('/animalsClientspostsdata/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsClientsPost);
// **************************************************************************** 
// *************************************** AddsOne ************************************* 
router.route('/animalsAddsOneGeta/:animalsAreaId')
.get(userControllers.animalsAddsOneGet)

router.route('/animalsAddsOneDeletea/:animalsAreaId')
.delete(userControllers.animalsAddsOneDelete)
 
  router.route('/animalsAddsOnePtsa/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/animalsAddsTwoGeta/:animalsAreaId')
.get(userControllers.animalsAddsTwoGet)

router.route('/animalsAddsTwoDeletea/:animalsAreaId')
.delete(userControllers.animalsAddsTwoDelete)
 
  router.route('/animalsAddsTwoPtsa/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/animalsAddsThreeGeta/:animalsAreaId')
.get(userControllers.animalsAddsThreeGet)

router.route('/animalsAddsThreeDeletea/:animalsAreaId')
.delete(userControllers.animalsAddsThreeDelete)
 
  router.route('/animalsAddsThreePtsa/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/animalsAddsFourGeta/:animalsAreaId')
.get(userControllers.animalsAddsFourGet)

router.route('/animalsAddsFourDeletea/:animalsAreaId')
.delete(userControllers.animalsAddsFourDelete)
 
  router.route('/animalsAddsFourPtsa/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsAddsFourP);
// ****************************************************************************
 

// *************************************** AddsOne ************************************* 
router.route('/animalsAddsOneLocGet/:animalsAreaId')
.get(userControllers.animalsAddsOneLocGet)

router.route('/animalsAddsOneLocDelete/:animalsAreaId')
.delete(userControllers.animalsAddsOneLocDelete)
 
  router.route('/animalsAddsOneLocPts/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/animalsAddsTwoLocGet/:animalsAreaId')
.get(userControllers.animalsAddsTwoLocGet)

router.route('/animalsAddsTwoLocDelete/:animalsAreaId')
.delete(userControllers.animalsAddsTwoLocDelete)
 
  router.route('/animalsAddsTwoLocPts/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/animalsAddsThreeGetLoc/:animalsAreaId')
.get(userControllers.animalsAddsThreeLocGet)

router.route('/animalsAddsThreeDeleteLoc/:animalsAreaId')
.delete(userControllers.animalsAddsThreeLocDelete)
 
  router.route('/animalsAddsThreePtsLoc/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/animalsAddsFourLocGet/:animalsAreaId')
.get(userControllers.animalsAddsFourLocGet)

router.route('/animalsAddsFourLocDelete/:animalsAreaId')
.delete(userControllers.animalsAddsFourLocDelete)
 
  router.route('/animalsAddsFourLocPts/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/animalsAddsFiveLocGet/:animalsAreaId')
.get(userControllers.animalsAddsFiveLocGet)

router.route('/animalsAddsFiveLocDelete/:animalsAreaId')
.delete(userControllers.animalsAddsFiveDeleteLoc)
 
  router.route('/animalsAddsFiveLocPts/:animalsAreaId')
  .post(multipartMiddleware,userControllers.animalsAddsFiveLocP);
// ****************************************************************************
 
// *************************************** animals types ************************************* 
router.route('/animalsTypes/:animalsClientsId')
.get(userControllers.animalsTypesGet)
.post(userControllers.animalsTypesPost);

router.route('/animalsType/:animalsTypesId')
.get(userControllers.animalsTypesGetId)
  
   
// **************************************************************************** 
// *************************************** animals types descriptions ************************************* 
router.route('/animalsTypesdes/:animalsTypeId')
.get(userControllers.animalsTypesdesGet)
router.route('/animalsTypesdesCont/:animalsTypeId')
.get(userControllers.animalsTypesdesGetCont)
router.route('/animalsTypesdesConta/:animalsTypeId')
.get(userControllers.animalsTypesdesGetContDetails)
router.route('/animalsTypesdesc/:animalsTypeId')
.post(multipartMiddleware,userControllers.animalsTypesdescPost);

router.route('/animalsTypedesInd/:animalsTypesdesId')
.get(userControllers.animalsTypesDesGetId)
  
   
// **************************************************************************** 

// ****************** only updates details ****************
router.route('/animalsupdatesget/:animalsClientsId')
.get(userControllers.animalsUsersGet)

router.route('/animalsupdatesgetCounts/:animalsClientsId')
.get(userControllers.animalsUsersGetCounts)

router.route('/animalsupdatesgetpostsdata/:animalsClientsId')
.post(multipartMiddleware,userControllers.animalsUsersUpdates);



//updates comments and reply 

router.route('/animalsupdatesCommentsget/:animalsupdatesId')
.get(userControllers.animalsupdatesCommentsGet)
router.route('/animalsupdatesCommentsgetcounts/:animalsupdatesId')
.get(userControllers.animalsupdatesCommentsGetcounts)

router.route('/animalsupdatesCommentspost/:animalsupdatesId')
.post(userControllers.animalsupdatesCommentsposts)

router.route('/animalsupdatesCommentReplysPost/:animalsupdatesCommId')
.post(userControllers.animalsupdatesCommentsReplyposts)

 router.route('/animalsupdatesCommentReplysGet/:animalsupdatesCommId')
.get(userControllers.animalsupdatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/animalsuserscomments/:animalsClientsId')
.get(userControllers.animalsusersCommentsget)
.post(userControllers.animalsusersCommentsposts);

router.route('/animalsuserscommentsCounts/:animalsClientsId')
.get(userControllers.animalsusersCommentsgetCounts)

// ****************** animals food ****************

router.route('/animalsfoodget/:animalsClientsId')
.get(userControllers.animalsfoodGet)

router.route('/animalsfoodgetCounts/:animalsClientsId')
.get(userControllers.animalsfoodGetCounts)

router.route('/animalsfoodpostsdata/:animalsClientsId')
.post(multipartMiddleware,userControllers.animalsfoodPosts);

// ****************** animalsNecessaries ****************

router.route('/animalsNecessariesGet/:animalsClientsId')
.get(userControllers.animalsNecessariesGets)

router.route('/animalsNecessariesGetCounts/:animalsClientsId')
.get(userControllers.animalsNecessariesCounts)

router.route('/animalsNecessariesPosts/:animalsClientsId')
.post(multipartMiddleware,userControllers.animalsNecessariesPosts);

module.exports = router;