const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    animalsLocation:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'animalslocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('animalsArea',areaSchema);
module.exports=areas;