const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,

    animalsClientsForGeneralComments:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'animalsClients'
    }
},
{timestamps:true});
const comm=mongoose.model('animalsGComments',commentsSchema);
module.exports=comm;