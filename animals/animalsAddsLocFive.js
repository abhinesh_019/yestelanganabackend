const mongoose=require('mongoose');
 
const animalsSchema= new mongoose.Schema(
    {
        addsFiveLinkLoc:{type:String},
        addsFiveImgLoc:{type:String},   
           animalsAreaForAddsLocFive:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'animalslocations'
                     }
 },

{timestamps:true}

);

const animals=mongoose.model('animalsAddsLocFive',animalsSchema);
module.exports=animals;