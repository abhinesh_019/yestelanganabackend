const mongoose=require('mongoose');
 
const animalsSchema= new mongoose.Schema(
    {
        addsFourLink:{type:String},
        addsFourImg:{type:String},   
           animalsAreaForAddsFour:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'animalsArea'
                     }
 },

{timestamps:true}

);

const animals=mongoose.model('animalsAddsFour',animalsSchema);
module.exports=animals;