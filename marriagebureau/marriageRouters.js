const express= require('express');
var router = express.Router();
const userControllers = require('../marriagebureau/marriageControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

 


//  ************ Locations ***********

router.route('/marriagesLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/marriagesAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/marriagesClientsget/:AreaId')
.get(userControllers.index)

router.route('/marriagesClientsgetAreaSingleCnt/:AreaId')
.get(userControllers.indexAs)

router.route('/marriagesClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/marriagesClientsAllCount') 
.get(userControllers.totalclients)

router.route('/marriagesClientsAllCountLocations') 
.get(userControllers.totalclientsLocations)

router.route('/marriagesClientsAllCountMainArea/:AreaId') 
.get(userControllers.totalclientsArea)


router.route('/marriagesClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/marriagesClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

 
// ******************* marriage type  ******************
router.route('/marriagesServicesTypeGet/:clientId')
.get(userControllers.servicesTypeget)
 
router.route('/marriagesServicesTypePost/:clientId')
.post(userControllers.servicesTypePost);

// *******************************************************
// ******************* marriage type main caste ******************
router.route('/marriagesServicesTypemainGet/:clientId')
.get(userControllers.servicesTypemainget)
 
router.route('/marriagesServicesTypemainPost/:clientId')
.post(userControllers.servicesTypemainPost);

// *******************************************************
// ******************* Clients Services ******************
router.route('/marriagesServicesGet/:clientId')
.get(userControllers.servicesget)
router.route('/marriagesServicesGetCounts/:clientId')
.get(userControllers.servicesgetCounts)
router.route('/marriagesServicesPost/:clientId')
.post(userControllers.servicesPost);

// *******************************************************
// ******************* Clients Track Records ******************
router.route('/marriagesTrackRecordsGet/:clientId')
.get(userControllers.trackRecordsget)
router.route('/marriagesTrackRecordsGetCounts/:clientId')
.get(userControllers.trackRecordsgetCounts)
router.route('/marriagesTrackRecordsPost/:clientId')
.post(multipartMiddleware,userControllers.trackRecordsPost);

// *******************************************************
// ****************** only updates details ****************
router.route('/marriagesupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/marriagesupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/marriagesupdatesgetpostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/marriagesupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/marriagesupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/marriagesupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/marriagesupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/marriagesupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/marriageseuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/marriagesCeuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

// *************************************** AddsOne ************************************* 
router.route('/marriagesAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/marriagesAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/marriagesAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/marriagesAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/marriagesAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/marriagesAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/marriagesAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/marriagesAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/marriagesAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/marriagesAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/marriagesAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/marriagesAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/marriagesAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/marriagesAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/marriagesAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/marriagesAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/marriagesAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/marriagesAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/marriagesAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/marriagesAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/marriagesAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/marriagesAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/marriagesAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/marriagesAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/marriagesAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/marriagesAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/marriagesAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
