const mongoose=require('mongoose');

const SchemaCUpdatesCommentsReply= new mongoose.Schema({
 
    descriptions:String,
   
    marriagesClientsForUpdatesCommentsReply:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'marriagesUpdatesComments'
}
    
},
{timestamps:true});
const   updatesCommentsReply=mongoose.model('marriagesUpdatesCommentsReply',SchemaCUpdatesCommentsReply);

module.exports=updatesCommentsReply;

