const mongoose=require('mongoose');

const SchemaCUpdatesComments= new mongoose.Schema({
 
    descriptions:String,
   
    marriagesClientsForUpdatesComments:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'marriagesUpdates'
}
    
},
{timestamps:true});
const   updatesComments=mongoose.model('marriagesUpdatesComments',SchemaCUpdatesComments);

module.exports=updatesComments;

