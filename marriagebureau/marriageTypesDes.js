const mongoose=require('mongoose');

const SchemaCasteTypeDescriptions= new mongoose.Schema({
    casteType:{type:String},
    MainCaste:{type:String},
    subCaste:{type:String},
    subCasteTypes:{type:String},
    relatedTocaste:{type:String},
     descriptions:{type:String},

 
    marriagesClientsForCasteTypeDescriptions:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'marriagesMainCaste'
}
    
},
{timestamps:true});
const   CasteTypeDescriptions=mongoose.model('marriagesCasteTypeDescriptions',SchemaCasteTypeDescriptions);

module.exports=CasteTypeDescriptions;

