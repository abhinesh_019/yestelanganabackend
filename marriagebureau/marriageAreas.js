const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    marriagesLocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'marriagesLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('marriagesAreas',areaSchema);
module.exports=areas;