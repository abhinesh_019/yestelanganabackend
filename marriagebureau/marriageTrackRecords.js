const mongoose=require('mongoose');

const SchemaCasteTypeDescriptions= new mongoose.Schema({
    groomeName:{type:String},
    brideName:{type:String},
    place:{type:String},
    images:{type:String},
    casteName:{type:String},
    descriptions:{type:String},

 
    marriagesClientsForTrackRecords:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'marriagesMainCaste'
}
    
},
{timestamps:true});
const   CasteTypeDescriptions=mongoose.model('marriagesTrackRecords',SchemaCasteTypeDescriptions);

module.exports=CasteTypeDescriptions;

