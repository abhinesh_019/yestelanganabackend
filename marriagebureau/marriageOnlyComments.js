const mongoose=require('mongoose');

const SchemaCommenlsOnly= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,
 
    marriagesClientsForCommenlsOnly:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'marriagesClients'
}
    
},
{timestamps:true});
const   commenlsOnly=mongoose.model('marriagesOnlyComments',SchemaCommenlsOnly);

module.exports=commenlsOnly;

