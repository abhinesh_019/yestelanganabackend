const express= require('express');
var router = express.Router();
const userControllers = require('../musics/musicsControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

 


//  ************ Locations ***********

router.route('/musicLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/musicAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/musicClientsget/:AreaId')
.get(userControllers.index)
router.route('/musicClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/musicClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/musicClientsAllCount') 
.get(userControllers.totalclients)

router.route('/musicClientsCountAreas')  
.get(userControllers.tuserscountAreas)


router.route('/musicClientsCountDist')  
.get(userControllers.tuserscountDist)

router.route('/musicClientsGetind/:clientsId')
.get(userControllers.indexs)
router.route('/musicClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/musicfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/musicfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/musicfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************
 

// *******************************************************
// ******************* Clients Services ******************
router.route('/musicServicesGet/:ServicesId')
.get(userControllers.servicesget)
router.route('/musicServicesGetCounts/:ServicesId')
.get(userControllers.servicesgetCounts)
router.route('/musicServicesPost/:ServicesId')
.post(userControllers.servicesPost);

// *******************************************************
// ******************* Clients servicesTwo ******************
router.route('/musicServicesTwoGet/:clientId')
.get(userControllers.servicesTwoGet)
 
router.route('/musicServicesTwoPost/:clientId')
.post(multipartMiddleware,userControllers.servicesTwoPost);

// *******************************************************
// ****************** only updates details ****************
router.route('/musicupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/musicupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/musicupdatespostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/musicupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/musicupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/musicupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/musicupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/musicupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/musiceuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/musicCeuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts);

// ******************* Clients Fecilities ******************
router.route('/musicfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/musicfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/musicfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************
// *******************  catageries******************
router.route('/musicCatageriesGets/:clientId')
.get(userControllers.musicCatageriesget)
router.route('/musicCatageriesGetCount/:clientId')
.get(userControllers.musicCatageriesgetCounts)
router.route('/musicCatageriesPosts/:clientId')
.post(multipartMiddleware,userControllers.musicCatageriespost);

// *******************************************************

// *************************************** AddsOne ************************************* 
router.route('/musicAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/musicAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/musicAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/musicAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/musicAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/musicAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/musicAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/musicAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/musicAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/musicAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/musicAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/musicAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/musicAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/musicAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/musicAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/musicAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/musicAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/musicAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/musicAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/musicAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/musicAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/musicAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/musicAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/musicAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/musicAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/musicAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/musicAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
