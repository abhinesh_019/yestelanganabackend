const mongoose=require('mongoose');

const SchemaAreas= new mongoose.Schema({

    name:{type:String},
     descriptions:{type:String},
    images:{type:String},
 
    ClientsForFecilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'musicClients'
}
    
},
{timestamps:true});
const Area=mongoose.model('musicsFecilities',SchemaAreas);

module.exports=Area;

