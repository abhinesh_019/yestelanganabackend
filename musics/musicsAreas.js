const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'musicsLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('musicAreas',areaSchema);
module.exports=areas;