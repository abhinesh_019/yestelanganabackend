const express= require('express');
var router = express.Router();
const userControllers = require('../classifieds/classifiedsControllers');
// const multer = require('multer');
// var nodemailer=require('../helpers/nodemailer');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ***********************
router.route('/clasdifiedsGeneralsLocations')
.get(userControllers.clasdifiedsGeneralsLocationsget)
.post(multipartMiddleware,userControllers.clasdifiedsGeneralsLocationsPosts);

router.route('/classifidsGeneralsLanding/:classifiedId')
.get(userControllers.classifidsGeneralsLandingget)
router.route('/classifidsGeneralsLandingp/:classifiedId')
 .post(userControllers.classifidsGeneralsLandingpost)


 router.route('/classifideGeneralsg/:classifiedId')
.get(userControllers.Generals)
router.route('/classifideGeneralsgc/:classifiedId')
.get(userControllers.Generalsc)
router.route('/classifideGeneralsp/:classifiedId')
.post(multipartMiddleware,userControllers.Generalsp)



router.route('/classifidesDate')
.get(userControllers.classifidesDateget)
router.route('/classifidesDatep')
 .post(userControllers.classifidesDatepost);

 router.route('/classifidesLands')
.get(userControllers.classifidesLandseget)
router.route('/classifideslandsp')
 .post(userControllers.classifidesLandspost)

 router.route('/classifidesStatus/:classifiedId')
 .get(userControllers.classifidesStatuseget)
 router.route('/classifidesStatusc')
 .get(userControllers.classifidesStatusegetc)
 router.route('/classifidesStatusp/:classifiedId')
  .post(userControllers.classifidesStatuspost)
 // *********** clients ***************
router.route('/classifideSearchJobsg/:classifiedId')
.get(userControllers.index)
 
router.route('/classifideSearchJobsgc/:classifiedId')
.get(userControllers.indexCounts)
 
router.route('/classifideSearchJobsp/:classifiedId')
.post(multipartMiddleware,userControllers.searrchJobs)

 module.exports = router;