const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'outdoorgamesLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('outdoorgamesArea',areaSchema);
module.exports=areas;