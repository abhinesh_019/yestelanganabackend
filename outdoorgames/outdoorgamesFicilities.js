const mongoose=require('mongoose');

const SchemaAreas= new mongoose.Schema({

    fecilitiesName:String,
    fecilitiesImg:String,
    fecilitiesDescriptions:String,
 
    ClientsForFecilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'outdoorgamesClient'
}
    
},
{timestamps:true});
const Area=mongoose.model('outdoorgamesFecilities',SchemaAreas);

module.exports=Area;

