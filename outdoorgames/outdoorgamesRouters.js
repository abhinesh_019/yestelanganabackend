const express= require('express');
var router = express.Router();
const userControllers = require('../outdoorgames/outdoorgamesControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ Locations ***********

router.route('/outdoorgamesLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/outdoorareas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/outdoorgamesClientsget/:AreaId')
.get(userControllers.index)

router.route('/outdoorgamesClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/outdoorgamesClientsAllCount') 
.get(userControllers.totalclients)


router.route('/outdoorgamesClientsCountAreas/:AreaId')  
.get(userControllers.tuserscountAreas)


router.route('/outdoorgamesClientsCountDist')  
.get(userControllers.tuserscountDist)


router.route('/outdoorgamesClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/outdoorgamesClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/outdoorgamesClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/outdoorgamesfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/outdoorgamesfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/outdoorgamesfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************


// ******************* Clients service Type ******************
router.route('/outdoorgamesServiceGet/:clientId')
.get(userControllers.serviceget)
 
router.route('/outdoorgamesServiceGetc/:clientId')
.get(userControllers.servicegetc)

router.route('/outdoorgamesServiceCatPost/:clientId')
.post(multipartMiddleware,userControllers.servicespostType);

// *******************************************************
// ******************* Clients Services ******************
router.route('/outdoorgamesServicesGet/:clientId')
.get(userControllers.servicesget)
router.route('/outdoorgamesServicesGetCounts/:clientId')
.get(userControllers.servicesgetCounts)
router.route('/outdoorgamesServicesPost/:clientId')
.post(multipartMiddleware,userControllers.servicesPost);

// *******************************************************
// ****************** only updates details ****************
router.route('/outdoorgamesupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/outdoorgamesupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/outdoorgamesupdatespostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/outdoorgamesupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/outdoorgamesupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/outdoorgamesupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/outdoorgamesupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/outdoorgamesupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/outdoorgamesUsersComments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/outdoorGamesUsersCommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

 
// *************************************** AddsOne ************************************* 
router.route('/outdoorGamesAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/outdoorGamesAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/outdoorGamesAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/outdoorGamesAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/outdoorGamesAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/outdoorGamesAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/outdoorGamesAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/outdoorGamesAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/outdoorGamesAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/outdoorGamesAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/outdoorGamesAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/outdoorGamesAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/outdoorGamesAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/outdoorGamesAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/outdoorGamesAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/outdoorGamesAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/outdoorGamesAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/outdoorGamesAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/outdoorGamesAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/outdoorGamesAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/outdoorGamesAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/outdoorGamesAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/outdoorGamesAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/outdoorGamesAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/outdoorGamesAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/outdoorGamesAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/outdoorGamesAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
