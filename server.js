var express =require('express');
var logger = require('morgan');
var app=express();
 var schools= require('./educations/schools/schoolsRoutes')
 var babycare=require('./babycares/babycareRoutes')
 var furnitures= require('./furnitures/furnituresRoutes')
 var tutions= require('./tutions/tutionsRoter')
 var parlour= require('./beautyParlour/beautyParlourRouter')
 var functionhalls= require('./functionhalls/functionsHallRouters')
 var gym= require('./Gym/gymRouters')
 var plants= require('./plants/plantsRouter')
 var animals= require('./animals/animalsRouter')
 var travels= require('./travels/travelsRouters')
 var dance= require('./danceInstutite/danceRouter')
 var musics= require('./musics/musicsRouters')
 var hotels= require('./hotels/hotelsRouters')
 var marriages= require('./marriagebureau/marriageRouters')
 var restarants= require('./restaurants/restaurantsRouters')
 var realEstate= require('./realestate/realestateRouters')
 var houseRentals= require('./houseRentals/houseRentalsRouters')
 var swimming= require('./swimming/swimmingRouter')
 var fights= require('./fights/fightsRouters')
 var indoor= require('./indoorgames/indoorgamesRouters')
 var outdooor= require('./outdoorgames/outdoorgamesRouters')
 var driving= require('./drivingSchools/drivingRouters')
 var fireWorks= require('./fireWorks/fireWorksRouters')
 var classifieds= require('./classifieds/classifiedsRouter')
 var packersnadmovers= require('./packersMovers/packersMoversRouters')
 var professionalInst= require('./professionalInst/professionalInstRoutes')
 var computers= require('./computerServiceCenter/computerServiceCenterRouter')
 var mobile= require('./mobileSalesServices/mobileSalesServicesRouter')
var user= require('./users/routes')
 var hostels=require('./boysHostels/boysHostelsRoutes')
 var feedback=require('./feedbacks/feedbackRouter')
  var mongoose=require('mongoose')
const bodyParser=require('body-parser')
 
mongoose.Promise=global.Promise;
// mongoose.connect('mongodb://localhost:27017/HyderabadHostels');
// 
mongoose.connect('mongodb://localhost:27017/HyderabadHostels',{user:"Abhinesh.236019",pass: "Abhinesh.236019"},function(err){console.log(err,"mongosdb test")});

  // db.createUser(  
    //     {
    //         user: "Abhinesh.236019",
    //         pwd: "Abhinesh.236019",
    //         roles: [ { role: "dbOwner", db: "HyderabadHostels" } ]
    //     })
//middle ware
app.use(logger('dev'));
app.use(bodyParser.json());
 
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', "*");
  
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'x-access-token,authorization,Content-Type,Access-Control-Request-Headers,enctype');
  
  // Set to true if you need the website to include cookies in requests
  res.setHeader('Access-Control-Allow-Credentials', true);
  
  if (req.method === 'OPTIONS') {
  res.status(200);
  res.end();
  }
  else {
  // Pass to next layer of middleware
  next();
  }
  });
  

//routes

 app.use('/telangana',hostels); 
 app.use('/telangana',user);
 app.use('/telangana',schools);
 app.use('/telangana',babycare);
 app.use('/telangana',furnitures);
 app.use('/telangana',tutions);
 app.use('/telangana',parlour);
 app.use('/telangana',gym);
 app.use('/telangana',travels);
 app.use('/telangana',plants);
 app.use('/telangana',animals);
 app.use('/telangana',functionhalls);
 app.use('/telangana',dance); 
 app.use('/telangana',musics); 
 app.use('/telangana',restarants); 
 app.use('/telangana',hotels);
 app.use('/telangana',marriages); 
 app.use('/telangana',realEstate); 
 app.use('/telangana',houseRentals);
 app.use('/telangana',swimming); 
 app.use('/telangana',fights); 
 app.use('/telangana',indoor); 
 app.use('/telangana',outdooor); 
 app.use('/telangana',driving); 
 app.use('/telangana',fireWorks); 
 app.use('/telangana',packersnadmovers); 
 app.use('/telangana',professionalInst); 
 app.use('/telangana',computers); 
 app.use('/telangana',mobile); 
 app.use('/telangana',feedback); 
app.use('/telangana',classifieds);


 const port=app.get('port') ||3000;
app.listen(port,()=>console.log(`server was running at port:${port}`));
module.exports=app

// aws note :

// Public IPv4 address: 52.66.33.87
// Instance ID : i-08d93faeecdca56e0
// IPv4 Public IP : 52.66.33.87

// eipalloc-07f03c0113132b4eb

// 686851781516







