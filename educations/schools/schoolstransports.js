const mongoose=require('mongoose');
const busSchema= new mongoose.Schema({
    
    name:String,
    from:String,
    fromtimings:String,
    to:String,
    totimings:String,
    contactNo:String,
    drivername:String,
    stopplace:String,
    educations:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'schools'
    }
});
const comm=mongoose.model('bustravells',busSchema);
module.exports=comm;