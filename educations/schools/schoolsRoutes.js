const express= require('express');
var router = express.Router();
const userControllers = require('../schools/schoolsControllers');
 
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

router.route('/schools')
.get(userControllers.index)
.post(multipartMiddleware,userControllers.newUser);


router.route('/schools/school')
.get(userControllers.schools)

// router.route('/boysHostel/:boysHostelId')
// .get(userControllers.getUser)
// .put(userControllers.replaceUser)
// .delete(userControllers.deleteUsers)

// router.route('/:boysHostelId/comments')
// .get(userControllers.getIDcomments)
// .post(userControllers.newIDcomments)

module.exports = router;