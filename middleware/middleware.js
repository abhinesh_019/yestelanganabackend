let jwt = require('jsonwebtoken');
let utils=require('./../helpers/utils')

let reqiuresBody = (req,res,next)=>{
    if(req.body && _.isObject(req.body) && !_.isEmpty(req.body)){
        next();
    }else{
        res.status(400).json({status:false,message:"Invalid or empty data",data:null});
    }
}

// after success login generate jwt token
let generateJWTForUser = (userData) => {
    return jwt.sign({ _id: userData._id}, config.jwt.secret, config.jwt.options);
}

// verify token valid or not
let isUserLogin = (req,res,next)=>{

    let token = req.headers['x-access-token'] || req.cookies['token'];
    
    
    if(token){
        let decodedData;
        try{
            decodedData =  utils.decodeJWTForUser(token);
        }catch (err){
            console.log("error",err)
            res.status(401).json({status:false,message:"Invalid token",data:null});
            return;
        }
        req.jwt = decodedData;
        

        next();
//         User.findOneByValues({_id:decodedData._id,role:decodedData.role})
//             .then(function (user) {
// req.jwt=decodedData;
// req.jwt.email=user.email;
// req.jwt.name=user.name;
// req.jwt.role=user.role;
// req.jwt.isEmailVerified=user.isEmailVerified;
               
//             }).catch(function (err) {
//                 console.log(err);
//             res.status(401).json({status:false,message:"User not found",data:null});
//         }); 
    }else{
        res.status(401).json({status:false,message:"User token not found",data:null});
    }
}



module.exports={
    generateJWTForUser,
    isUserLogin,
    reqiuresBody
};