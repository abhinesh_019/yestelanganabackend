const mongoose=require('mongoose');

const SchemaCUpdatesComments= new mongoose.Schema({
 
    descriptions:String,
   
    realestateClientsForUpdatesComments:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateUpdates'
}
    
},
{timestamps:true});
const   updatesComments=mongoose.model('realestateUpdatesComments',SchemaCUpdatesComments);

module.exports=updatesComments;

