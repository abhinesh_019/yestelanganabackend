const mongoose=require('mongoose');

const SchemaTypeDescriptions= new mongoose.Schema({
      area:{type:String},
      realestatType:{type:String},
      realestatTypeBusinessLogic:{type:String},
      realestatTypeBusinessLogicRoom:{type:String},
     propertyArea:{type:String}, //in square yards done
    propertyName:{type:String}, // done
    shortDesc:{type:String}, // done
    longDesc:{type:String}, // done
    furnituresDetails:{type:String}, // done
    price:{type:String}, // done
    locations:{type:String}, // done
    landmark:{type:String}, // done
    lat:{type:String}, // done
    long:{type:String}, // done
    status:{type:String}, // done
    floor:{type:String}, // done
    verifications:{type:String}, // done
    optionalDescriptions:{type:String}, // done
mainImg:{type:String},
    
    



    realestateClientsForTypeDescriptionsLogic:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestatebusinesrooms'
}
    
},
{timestamps:true});
const TypeDescriptions=mongoose.model('realestateTypeDescriptionsMain',SchemaTypeDescriptions);

module.exports=TypeDescriptions;

