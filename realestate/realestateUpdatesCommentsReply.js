const mongoose=require('mongoose');

const SchemaCUpdatesCommentsReply= new mongoose.Schema({
 
    descriptions:String,
   
    realestateClientsForUpdatesCommentsReply:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateUpdatesComments'
}
    
},
{timestamps:true});
const   updatesCommentsReply=mongoose.model('realestateUpdatesCommentsReply',SchemaCUpdatesCommentsReply);

module.exports=updatesCommentsReply;

