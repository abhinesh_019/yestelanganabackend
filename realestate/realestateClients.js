const mongoose=require('mongoose');
const Schema= new mongoose.Schema(
    {       
        shopname:{type:String},
            name:{type:String},
            keyword:{type:String},
            aboutUseDes1:{type:String},
            aboutUseDes2:{type:String},
            ourTeam:{type:String},
            teamMembersNames:{type:String},
            teamDescriptions:{type:String},
             day1:{type:String},
             day2:{type:String},
             day3:{type:String},
             day4:{type:String},
             day5:{type:String},
             day6:{type:String},
             day7:{type:String},
             allTimes:{type:String},
             houseNo:{type:String},
             mainArea:{type:String},
             subArea:{type:String},
             landmark:{type:String},
             city:{type:String},
             distict:{type:String},
             state:{type:String},
             pincode:{type:String},
             officeNo:{type:String},
             mobileNo:{type:String},
             whatsupno:{type:String},
             emailId:{type:String},
             images1:{type:String},
             images2:{type:String},
             images3:{type:String},
            
            realestateAreaForClients:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'realestateAreas'
     }

},

{timestamps:true}

);

const Clients=mongoose.model('realestateClients',Schema);
module.exports=Clients;