var locations = require('../realestate/realestateLocations')
var areas = require('../realestate/realestateArea')
var clientsareas = require('../realestate/realestateClientsArea')
var clients = require('../realestate/realestateClients')
 var clientsUpdates = require('../realestate/realestateUpdates')
var clientsUpdatesComments = require('../realestate/realestateUpdatesComments')
var clientsUpdatesCommentsReply = require('../realestate/realestateUpdatesCommentsReply')
var clientsOnlyComments = require('../realestate/realestateOnlyComments')
 var typeOfRealEstate = require('../realestate/realestateTypes')
var typeOfRealEstat = require('../realestate/realestateTypesBusinessTypes')
var typeOfRealRoom = require('../realestate/realestatebusinessRooms')
var typeOfRealLogicAll = require('../realestate/realestateTypesDesLogicAll')
var outerView = require('../realestate/realestateOuterView')
var overVIew = require('../realestate/realestatePropertyOverView')
var animities = require('../realestate/realestateamenities')
var loc = require('../realestate/realestateLocationsAdv')
var innerViews = require('../realestate/realestateInner')
const fileService = require('../realestate/realestateServer');

var babycaresAddsOne=require('../realestate/fightsAddsOnea');
var babycaresAddsTwo=require('../realestate/fightsAddsTwoa');
var babycaresAddsThree=require('../realestate/fightsAddsThreea');
var babycaresAddsFour=require('../realestate/fightsAddsFoura');
 
var babycaresAddsOneLoc=require('../realestate/fightsAddsOnel');
var babycaresAddsTwoLoc=require('../realestate/fightsAddsTwol');
var babycaresAddsThreeLoc=require('../realestate/fightsAddsThreel');
var babycaresAddsFourLoc=require('../realestate/fightsAddsFourl');
var babycaresAddsFiveLoc=require('../realestate/fightsAddsFivel');
var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 
var async = require('async');
  
module.exports = {

 
// ********************* Locations *********************

locationspost: async(req,res,next)=>{
    const locs=  req.body;
    await locations.create(locs, function (err, data) {
        if (err) {
            console.log('Error in Saving user: ' + err);
        } else {

            res.status(201).json({ status: true, message: "user added sucessfully"});
        }
    });
 },



locationsget: async(req,res,next)=>{
    const users = await locations.find({}).sort({"locations":1});
       res.status(200).json(users);
 },
// ******************************************

// *********************** Areas *******************

Areasposts : async(req,res,next)=>{
    const { LocationsId } = req.params;
    req.body.realestateLocationForAreas = LocationsId
     const userscomm = new areas(req.body);
    await userscomm.save();
        res.status(201).json(userscomm);
},
 
Areasget:async (req, res, next) => {
     const { LocationsId } = req.params;
     const usersposts = await areas.find({realestateLocationForAreas:LocationsId}).sort({"area":-1})
    res.status(200).json(usersposts);
 },

// ******************************************
// ************************************************ Clients Updates ***************************************
 
newUser: (req, res, next) => {
    const { AreaId } = req.params;
    req.body.realestateAreaForClients = AreaId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null,data.Location);
            })
        },
         
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clients.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                }
            });
        }
 });
},



index:async (req, res, next) => {

    const { AreaId } = req.params;
    let search=req.query.search;

    const usersposts = await clients.find({realestateAreaForClients:AreaId,$or:[
        {subArea:new RegExp(search, "gi")},
        {pincode:new RegExp(search, "gi")}, 
        {shopName:new RegExp(search, "gi")},
        {keyword:new RegExp(search, "gi")},
        {landmark:new RegExp(search, "gi")},
    ]
}).sort({"subArea":-1})
    res.status(200).json(usersposts);
 },

usersgetcoutarea: async (req, res, next) => {
    const { AreaId } = req.params;

    const users = await clients.aggregate([{$match:{realestateAreaForClients:ObjectId(AreaId)}},

        {"$group" : {_id:{subArea:"$subArea"}, count:{$sum:1}}},{$sort:{"subArea":1}}
     ])
    
    res.status(200).json(users);
 },
 
tuserscount:async (req, res, next) => {
     const { AreaId } = req.params;
     const usersposts = await clients.find({realestateAreaForClients:AreaId}).count()
    res.status(200).json(usersposts);
 },

 totalclientsLocations: async (req, res, next) => {
 
    const users = await clients.aggregate([{"$group" : {_id:{distict:"$distict"}, count:{$sum:1}}},{$sort:{"distict":1}}
     ])
    
    res.status(200).json(users);
 },
 
totalclients:async (req, res, next) => {
  const usersposts = await clients.find({}).count()
    res.status(200).json(usersposts);
  
},
 
indexs:async (req, res, next) => {

    const { clientsId } = req.params;
    const usersposts = await clients.findById(clientsId) 
    res.status(200).json(usersposts);
  
},
// ****************  Clients create area   *******************
clientsAreapost : async(req,res,next)=>{
    const { clientId } = req.params;
    req.body.realestateClientsForAreasSeperate = clientId
     const userscomm = new clientsareas(req.body);
    await userscomm.save();
        res.status(201).json(userscomm);
},
clientsAreaget:async (req, res, next) => {
    const { clientId } = req.params;
    const usersposts = await clientsareas.find({realestateClientsForAreasSeperate:clientId}).sort({"clientArea":-1})
   res.status(200).json(usersposts);
},
  
    
// ***********************************************************************

// ****************  Clients real estate type   *******************


typespost: (req, res, next) => {
    const { areaClientId } = req.params;
    req.body.realestateClientsAreaForTypes = areaClientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images, function (err, data) {
                allData.images = data.Location;
                callback(null, data.Location);
            })
        },
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            typeOfRealEstate.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
typesgets:async (req, res, next) => {
    const { areaClientId } = req.params;
    const usersposts = await typeOfRealEstate.find({realestateClientsAreaForTypes:areaClientId}).sort({"realestateType":1})
   res.status(200).json(usersposts);
},
  
typesgetCounts:async (req, res, next) => {
    const { areaClientId } = req.params;
    const usersposts = await typeOfRealEstate.find({realestateClientsAreaForTypes:areaClientId}).count()
   res.status(200).json(usersposts);
},
// ***********************************************************************


// ****************  Clients real estate business type   *******************


typesBusinespost: (req, res, next) => {
    const { typesClientId } = req.params;
    req.body.realestateTypesForBusinesTypes = typesClientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images, function (err, data) {
                allData.images = data.Location;
                callback(null, data.Location);
            })
        },
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            typeOfRealEstat.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
typesBusinesgets:async (req, res, next) => {
    const { typesClientId } = req.params;
    const usersposts = await typeOfRealEstat.find({realestateTypesForBusinesTypes:typesClientId}).sort({"Businesype":-1})
   res.status(200).json(usersposts);
},
  
typesBusinesgetCounts:async (req, res, next) => {
    const { typesClientId } = req.params;
    const usersposts = await typeOfRealEstat.find({realestateTypesForBusinesTypes:typesClientId}).count()
   res.status(200).json(usersposts);
},
// ***********************************************************************
// ******************* Clients real estate Busines main details Types  ******************
 
typesBusinesMainpost:async (req, res, next) => {
 
    const { typesClientId } = req.params;
    req.body.realestateClientsForTypeDescriptionsLogic = typesClientId
    let allData = req.body

    async.parallel([
 
         
        function (callback) {
            console.log(req.files.mainImg,"req.files.viedoes 302");
            if(req.files.mainImg){
                fileService.uploadImage(req.files.mainImg, function (err, data) {
                    allData.mainImg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          }, 

 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            typeOfRealLogicAll.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }

        console.log(allData, "obj,result-----------------------------------");

    });
},

 
typesBusinesMaingets:async (req, res, next) => {
    const { typesClientId } = req.params;
    let search=req.query.search;
    const usersposts = await typeOfRealLogicAll.find({realestateClientsForTypeDescriptionsLogic:typesClientId,$or:[
        {propertyName:new RegExp(search, "gi")},
        {propertyArea:new RegExp(search, "gi")}, 
        {furnituresDetails:new RegExp(search, "gi")},
        {price:new RegExp(search, "gi")},
        {locations:new RegExp(search, "gi")},
        {landmark:new RegExp(search, "gi")},
        {floor:new RegExp(search, "gi")},
        {status:new RegExp(search, "gi")},
        {verifications:new RegExp(search, "gi")}
    ]
    
    }).sort({"createdAt":1})
   res.status(200).json(usersposts);
},
typesBusinesMaingetsInd:async (req, res, next) => {
    const { mainId } = req.params;
     const usersposts = await typeOfRealLogicAll.findById(mainId)
   res.status(200).json(usersposts);
},
typesBusinesMaingetArea:async (req, res, next) => {
    const { typesClientId } = req.params;
    const usersposts = await typeOfRealLogicAll.aggregate([{$match:{realestateClientsForTypeDescriptionsLogic:ObjectId(typesClientId)}},

        {"$group" : {_id:{area:"$area"}, count:{$sum:1}}},{$sort:{"area":1}}
     ])
   res.status(200).json(usersposts);
},

typesBusinesMaingetsREType:async (req, res, next) => {
    const { typesClientId } = req.params;
    const usersposts = await typeOfRealLogicAll.aggregate([{$match:{realestateClientsForTypeDescriptionsLogic:ObjectId(typesClientId)}},

        {"$group" : {_id:{realestatType:"$realestatType"}, count:{$sum:1}}},{$sort:{"realestatType":1}}
     ])
   res.status(200).json(usersposts);
},
 
typesBusinesMaingetLogic:async (req, res, next) => {
    const { typesClientId } = req.params;
    const usersposts = await typeOfRealLogicAll.aggregate([{$match:{realestateClientsForTypeDescriptionsLogic:ObjectId(typesClientId)}},

        {"$group" : {_id:{realestatTypeBusinessLogic:"$realestatTypeBusinessLogic"}, count:{$sum:1}}},{$sort:{"realestatTypeBusinessLogic":1}}
     ])
   res.status(200).json(usersposts);
},
typesBusinesMaingetLogicRoom:async (req, res, next) => {
    const { typesClientId } = req.params;
    const usersposts = await typeOfRealLogicAll.aggregate([{$match:{realestateClientsForTypeDescriptionsLogic:ObjectId(typesClientId)}},

        {"$group" : {_id:{realestatTypeBusinessLogicRoom:"$realestatTypeBusinessLogicRoom"}, count:{$sum:1}}},{$sort:{"realestatTypeBusinessLogicRoom":1}}
     ])
   res.status(200).json(usersposts);
},
 
// ***********************************************************************
// ******************* Clients real estate Busines main details outerView ******************
 
 

typesOuterviewpost:async (req, res, next) => {
 
    const { typesClientId } = req.params;
    req.body.realestateMainForOuterView = typesClientId
    let allData = req.body

    async.parallel([
    function (callback) {
            console.log(req.files.outerFsideimg,"req.files.images 273");
            if(req.files.outerFsideimg){
                fileService.uploadImage(req.files.outerFsideimg, function (err, data) {
                    allData.outerFsideimg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.outerBsideimg,"req.files.viedoes 302");
            if(req.files.outerBsideimg){
                fileService.uploadImage(req.files.outerBsideimg, function (err, data) {
                    allData.outerBsideimg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.outerRsideimg,"req.files.images 273");
            if(req.files.outerRsideimg){
                fileService.uploadImage(req.files.outerRsideimg, function (err, data) {
                    allData.outerRsideimg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.outerLsideimg,"req.files.viedoes 302");
            if(req.files.outerLsideimg){
                fileService.uploadImage(req.files.outerLsideimg, function (err, data) {
                    allData.outerLsideimg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

 

    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            outerView.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
   });
},

 
typesOuterviewgets:async (req, res, next) => {
    const { typesClientId } = req.params;
   
    const usersposts = await outerView.find({realestateMainForOuterView:typesClientId}).sort({"createdAt":1})
   res.status(200).json(usersposts);
},
typesOuterviewgetsind:async (req, res, next) => {
    const { typesClientId } = req.params;
   
    const usersposts = await outerView.findById(typesClientId) 
   res.status(200).json(usersposts);
},
 
// ***********************************************************************

// ******************* Clients real estate Busines main details innerViews ******************
  
typesInnerViewspost:async (req, res, next) => {
 
    const { typesClientId } = req.params;
    req.body.realestateMainForInnerView = typesClientId
    let allData = req.body

    async.parallel([
        function (callback) {
            console.log(req.files.innerNimg1,"req.files.viedoes 302");
            if(req.files.innerNimg1){
                fileService.uploadImage(req.files.innerNimg1, function (err, data) {
                    allData.innerNimg1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg2,"req.files.viedoes 302");
            if(req.files.innerNimg2){
                fileService.uploadImage(req.files.innerNimg2, function (err, data) {
                    allData.innerNimg2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.innerNimg3,"req.files.viedoes 302");
            if(req.files.innerNimg3){
                fileService.uploadImage(req.files.innerNimg3, function (err, data) {
                    allData.innerNimg3 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg4,"req.files.viedoes 302");
            if(req.files.innerNimg4){
                fileService.uploadImage(req.files.innerNimg4, function (err, data) {
                    allData.innerNimg4 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg5,"req.files.viedoes 302");
            if(req.files.innerNimg5){
                fileService.uploadImage(req.files.innerNimg5, function (err, data) {
                    allData.innerNimg5 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg6,"req.files.viedoes 302");
            if(req.files.innerNimg6){
                fileService.uploadImage(req.files.innerNimg6, function (err, data) {
                    allData.innerNimg6 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.innerNimg7,"req.files.viedoes 302");
            if(req.files.innerNimg7){
                fileService.uploadImage(req.files.innerNimg7, function (err, data) {
                    allData.innerNimg7 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg8,"req.files.viedoes 302");
            if(req.files.innerNimg8){
                fileService.uploadImage(req.files.innerNimg8, function (err, data) {
                    allData.innerNimg8 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.innerNimg9,"req.files.viedoes 302");
            if(req.files.innerNimg9){
                fileService.uploadImage(req.files.innerNimg9, function (err, data) {
                    allData.innerNimg9 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg10,"req.files.viedoes 302");
            if(req.files.innerNimg10){
                fileService.uploadImage(req.files.innerNimg10, function (err, data) {
                    allData.innerNimg10 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.innerNimg11,"req.files.viedoes 302");
            if(req.files.innerNimg11){
                fileService.uploadImage(req.files.innerNimg11, function (err, data) {
                    allData.innerNimg11 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg12,"req.files.viedoes 302");
            if(req.files.innerNimg12){
                fileService.uploadImage(req.files.innerNimg12, function (err, data) {
                    allData.innerNimg12 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.innerNimg13,"req.files.viedoes 302");
            if(req.files.innerNimg13){
                fileService.uploadImage(req.files.innerNimg13, function (err, data) {
                    allData.innerNimg13 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg14,"req.files.viedoes 302");
            if(req.files.innerNimg14){
                fileService.uploadImage(req.files.innerNimg14, function (err, data) {
                    allData.innerNimg14 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.innerNimg15,"req.files.viedoes 302");
            if(req.files.innerNimg15){
                fileService.uploadImage(req.files.innerNimg15, function (err, data) {
                    allData.innerNimg15 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg16,"req.files.viedoes 302");
            if(req.files.innerNimg16){
                fileService.uploadImage(req.files.innerNimg16, function (err, data) {
                    allData.innerNimg16 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.innerNimg17,"req.files.viedoes 302");
            if(req.files.innerNimg17){
                fileService.uploadImage(req.files.innerNimg17, function (err, data) {
                    allData.innerNimg17 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg18,"req.files.viedoes 302");
            if(req.files.innerNimg18){
                fileService.uploadImage(req.files.innerNimg18, function (err, data) {
                    allData.innerNimg18 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.innerNimg19,"req.files.viedoes 302");
            if(req.files.innerNimg19){
                fileService.uploadImage(req.files.innerNimg19, function (err, data) {
                    allData.innerNimg19 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.innerNimg20,"req.files.viedoes 302");
            if(req.files.innerNimg20){
                fileService.uploadImage(req.files.innerNimg20, function (err, data) {
                    allData.innerNimg20 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
        function (callback) {
            console.log(req.files.outerLsideimg,"req.files.viedoes 302");
            if(req.files.outerLsideimg){
                fileService.uploadImage(req.files.outerLsideimg, function (err, data) {
                    allData.outerLsideimg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

 

    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            innerViews.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
   });
},

 
typesInnerViewsgets:async (req, res, next) => {
    const { typesClientId } = req.params;
   
    const usersposts = await innerViews.find({realestateMainForInnerView:typesClientId}).sort({"createdAt":1})
   res.status(200).json(usersposts);
},
  
typesInnerViewsCounts:async (req, res, next) => {
    const { typesClientId } = req.params;
     
    const usersposts = await innerViews.find({realestateMainForInnerView:typesClientId}).count()
   res.status(200).json(usersposts);
},
// ***********************************************************************
// ******************* Clients real estate Busines main details over view ******************
 
typesOverviewspost : async(req,res,next)=>{
    const { typesClientId } = req.params;
    req.body.realestateMainForOverView = typesClientId
     const userscomm = new overVIew(req.body);
    await userscomm.save();
        res.status(201).json(userscomm);
},
 
typesOverviewsgets:async (req, res, next) => {
    const { typesClientId } = req.params;
   
    const usersposts = await overVIew.find({realestateMainForOverView:typesClientId}).sort({"createdAt":1})
   res.status(200).json(usersposts);
},
  
typesOverviewsCounts:async (req, res, next) => {
    const { typesClientId } = req.params;
     
    const usersposts = await overVIew.find({realestateMainForOverView:typesClientId}).count()
   res.status(200).json(usersposts);
},
// ***********************************************************************

// ******************* Clients real estate Busines locations advantahges ******************
  
typeslocationAdvantagespost:async (req, res, next) => {
 
    const { typesClientId } = req.params;
    req.body.realestateMainForLocationAdvantages = typesClientId
    let allData = req.body

    async.parallel([
        
// LOCATIONS ADVANTAGES
function (callback) {
    console.log(req.files.lAdvantagesImg1,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg1){
        fileService.uploadImage(req.files.lAdvantagesImg1, function (err, data) {
            allData.lAdvantagesImg1 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg2,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg2){
        fileService.uploadImage(req.files.lAdvantagesImg2, function (err, data) {
            allData.lAdvantagesImg2 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg3,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg3){
        fileService.uploadImage(req.files.lAdvantagesImg3, function (err, data) {
            allData.lAdvantagesImg3 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg4,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg4){
        fileService.uploadImage(req.files.lAdvantagesImg4, function (err, data) {
            allData.lAdvantagesImg4 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg5,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg5){
        fileService.uploadImage(req.files.lAdvantagesImg5, function (err, data) {
            allData.lAdvantagesImg5 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg6,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg6){
        fileService.uploadImage(req.files.lAdvantagesImg6, function (err, data) {
            allData.lAdvantagesImg6 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg7,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg7){
        fileService.uploadImage(req.files.lAdvantagesImg7, function (err, data) {
            allData.lAdvantagesImg7 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg8,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg8){
        fileService.uploadImage(req.files.lAdvantagesImg8, function (err, data) {
            allData.lAdvantagesImg8 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg9,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg9){
        fileService.uploadImage(req.files.lAdvantagesImg9, function (err, data) {
            allData.lAdvantagesImg1 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg10,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg10){
        fileService.uploadImage(req.files.lAdvantagesImg10, function (err, data) {
            allData.lAdvantagesImg10 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },

  function (callback) {
    console.log(req.files.lAdvantagesImg11,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg11){
        fileService.uploadImage(req.files.lAdvantagesImg11, function (err, data) {
            allData.lAdvantagesImg11 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg12,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg12){
        fileService.uploadImage(req.files.lAdvantagesImg12, function (err, data) {
            allData.lAdvantagesImg12 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg13,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg13){
        fileService.uploadImage(req.files.lAdvantagesImg13, function (err, data) {
            allData.lAdvantagesImg13 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg14,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg14){
        fileService.uploadImage(req.files.lAdvantagesImg14, function (err, data) {
            allData.lAdvantagesImg14 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg15,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg15){
        fileService.uploadImage(req.files.lAdvantagesImg15, function (err, data) {
            allData.lAdvantagesImg15 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg16,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg16){
        fileService.uploadImage(req.files.lAdvantagesImg16, function (err, data) {
            allData.lAdvantagesImg16 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg17,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg17){
        fileService.uploadImage(req.files.lAdvantagesImg17, function (err, data) {
            allData.lAdvantagesImg17 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg18,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg18){
        fileService.uploadImage(req.files.lAdvantagesImg18, function (err, data) {
            allData.lAdvantagesImg18 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg19,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg19){
        fileService.uploadImage(req.files.lAdvantagesImg19, function (err, data) {
            allData.lAdvantagesImg19 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },
  function (callback) {
    console.log(req.files.lAdvantagesImg20,"req.files.viedoes 302");
    if(req.files.lAdvantagesImg20){
        fileService.uploadImage(req.files.lAdvantagesImg20, function (err, data) {
            allData.lAdvantagesImg20 = data.Location;
            callback(null, data.Location);
        })
      }
    else{
        callback(null, null);   
    }
  },

 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            loc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
   });
},

 
typeslocationAdvantagesgets:async (req, res, next) => {
    const { typesClientId } = req.params;
   
    const usersposts = await loc.find({realestateMainForLocationAdvantages:typesClientId}).sort({"createdAt":1})
   res.status(200).json(usersposts);
},
  
typeslocationAdvantagesCounts:async (req, res, next) => {
    const { typesClientId } = req.params;
     
    const usersposts = await loc.find({realestateMainForLocationAdvantages:typesClientId}).count()
   res.status(200).json(usersposts);
},
// ***********************************************************************


// ****************  Clients real estate business room   *******************



typesBusinesRoomspost : async(req,res,next)=>{
    const { typesClientId } = req.params;
    req.body.realestateTypesForBusinesRooms = typesClientId
     const userscomm = new typeOfRealRoom(req.body);
    await userscomm.save();
        res.status(201).json(userscomm);
},
 
typesBusinesRoomsgets:async (req, res, next) => {
     const { typesClientId } = req.params;
     const usersposts = await typeOfRealRoom.find({realestateTypesForBusinesRooms:typesClientId}).sort({"businesTypeRooms":1})
    res.status(200).json(usersposts);
 },

 typesBusinesgetRoomsCounts:async (req, res, next) => {
    const { typesClientId } = req.params;
    const usersposts = await typeOfRealRoom.find({realestateTypesForBusinesRooms:typesClientId}).count()
   res.status(200).json(usersposts);
},
 
// ***********************************************************************

// ******************* Clients real estate Busines main details Types  aminities******************
 
typesAmenitiespost:async (req, res, next) => {
 
    const { typesClientId } = req.params;
    req.body.realestateMainForAminities = typesClientId
    let allData = req.body

    async.parallel([
 
 
          function (callback) {
            console.log(req.files.amenitiesImg1,"req.files.viedoes 302");
            if(req.files.amenitiesImg1){
                fileService.uploadImage(req.files.amenitiesImg1, function (err, data) {
                    allData.amenitiesImg1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg2,"req.files.viedoes 302");
            if(req.files.amenitiesImg2){
                fileService.uploadImage(req.files.amenitiesImg2, function (err, data) {
                    allData.amenitiesImg2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg3,"req.files.viedoes 302");
            if(req.files.amenitiesImg3){
                fileService.uploadImage(req.files.amenitiesImg3, function (err, data) {
                    allData.amenitiesImg3 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg4,"req.files.viedoes 302");
            if(req.files.amenitiesImg4){
                fileService.uploadImage(req.files.amenitiesImg4, function (err, data) {
                    allData.amenitiesImg4 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg5,"req.files.viedoes 302");
            if(req.files.amenitiesImg5){
                fileService.uploadImage(req.files.amenitiesImg5, function (err, data) {
                    allData.amenitiesImg5 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg6,"req.files.viedoes 302");
            if(req.files.amenitiesImg6){
                fileService.uploadImage(req.files.amenitiesImg6, function (err, data) {
                    allData.amenitiesImg6 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg7,"req.files.viedoes 302");
            if(req.files.amenitiesImg7){
                fileService.uploadImage(req.files.amenitiesImg7, function (err, data) {
                    allData.amenitiesImg7 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg8,"req.files.viedoes 302");
            if(req.files.amenitiesImg8){
                fileService.uploadImage(req.files.amenitiesImg8, function (err, data) {
                    allData.amenitiesImg8 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg9,"req.files.viedoes 302");
            if(req.files.amenitiesImg9){
                fileService.uploadImage(req.files.amenitiesImg9, function (err, data) {
                    allData.amenitiesImg9 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg10,"req.files.viedoes 302");
            if(req.files.amenitiesImg10){
                fileService.uploadImage(req.files.amenitiesImg10, function (err, data) {
                    allData.amenitiesImg10 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg11,"req.files.viedoes 302");
            if(req.files.amenitiesImg11){
                fileService.uploadImage(req.files.amenitiesImg11, function (err, data) {
                    allData.amenitiesImg11 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg12,"req.files.viedoes 302");
            if(req.files.amenitiesImg12){
                fileService.uploadImage(req.files.amenitiesImg12, function (err, data) {
                    allData.amenitiesImg12 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg13,"req.files.viedoes 302");
            if(req.files.amenitiesImg13){
                fileService.uploadImage(req.files.amenitiesImg13, function (err, data) {
                    allData.amenitiesImg13 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg14,"req.files.viedoes 302");
            if(req.files.amenitiesImg14){
                fileService.uploadImage(req.files.amenitiesImg14, function (err, data) {
                    allData.amenitiesImg14 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg15,"req.files.viedoes 302");
            if(req.files.amenitiesImg15){
                fileService.uploadImage(req.files.amenitiesImg15, function (err, data) {
                    allData.amenitiesImg15 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg16,"req.files.viedoes 302");
            if(req.files.amenitiesImg16){
                fileService.uploadImage(req.files.amenitiesImg16, function (err, data) {
                    allData.amenitiesImg16 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg17,"req.files.viedoes 302");
            if(req.files.amenitiesImg17){
                fileService.uploadImage(req.files.amenitiesImg17, function (err, data) {
                    allData.amenitiesImg17 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg18,"req.files.viedoes 302");
            if(req.files.amenitiesImg18){
                fileService.uploadImage(req.files.amenitiesImg18, function (err, data) {
                    allData.amenitiesImg18 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg19,"req.files.viedoes 302");
            if(req.files.amenitiesImg19){
                fileService.uploadImage(req.files.amenitiesImg19, function (err, data) {
                    allData.amenitiesImg19 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.amenitiesImg20,"req.files.viedoes 302");
            if(req.files.amenitiesImg20){
                fileService.uploadImage(req.files.amenitiesImg20, function (err, data) {
                    allData.amenitiesImg20 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },


    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            animities.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }

        console.log(allData, "obj,result-----------------------------------");

    });
},

 
typesAmenitiesgets:async (req, res, next) => {
    const { typesClientId } = req.params;
    let search=req.query.search;
    const usersposts = await animities.find({realestateMainForAminities:typesClientId }).sort({"createdAt":1})
   res.status(200).json(usersposts);
},
  
typesAmenitiesCounts:async (req, res, next) => {
    const { typesClientId } = req.params;
     
    const usersposts = await animities.find({realestateMainForAminities:typesClientId}).count()
   res.status(200).json(usersposts);
},
// ***********************************************************************

 
 


// ****************users updates*************

UsersUpdates: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.realestateClientsForUpdates = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.viedoes,"req.files.viedoes 302");
            if(req.files.viedoes){
                fileService.uploadImage(req.files.viedoes, function (err, data) {
                    allData.viedoes = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clientsUpdates.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

UsersGet:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({realestateClientsForUpdates:ClientsId}).sort({"createdAt:":1})

        res.status(200).json(users);
       console.log("users****", users);
},

UsersGetCounts:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({realestateClientsForUpdates:ClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
//admin users updates comments
updatesCommentsposts:async (req, res, next) => {

const { updatesId } = req.params;
  req.body.realestateClientsForUpdatesComments = updatesId 
const userscomm = new clientsUpdatesComments(req.body);

await userscomm.save();
console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
 res.status(201).json(userscomm);
},

updatesCommentsGet:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({realestateClientsForUpdatesComments:updatesId}).sort({"createdAt:":-1})
    res.status(200).json(users); 
},
updatesCommentsGetcounts:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({realestateClientsForUpdatesComments:updatesId}).count()
    res.status(200).json(users);
},

updatesCommentsReplyposts:async (req, res, next) => {
const { updatesCommId } = req.params;
  req.body.realestateClientsForUpdatesCommentsReply = updatesCommId 
const userscomm = new clientsUpdatesCommentsReply(req.body);
await userscomm.save();
 res.status(201).json(userscomm);
},

updatesCommentsReplyGet:async (req, res, next) => {

const { updatesCommId } = req.params;

    const users = await clientsUpdatesCommentsReply.find({realestateClientsForUpdatesCommentsReply:updatesCommId})

    res.status(200).json(users);
   console.log("users**** 399", users);
},

// ****************only comments *******************
usersCommentsposts : async (req,res) => {
const { ClientsId } = req.params;
req.body.realestateClientsForCommenlsOnly = ClientsId
const generalcomm = new clientsOnlyComments(req.body);
await generalcomm.save();
res.status(201).json(generalcomm);
},
usersCommentsget:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await clientsOnlyComments.find({realestateClientsForCommenlsOnly:ClientsId}).sort({"createdAt:":-1})
res.status(200).json(users);   
},
usersCommentsgetCounts:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await clientsOnlyComments.find({realestateClientsForCommenlsOnly:ClientsId}).count()
    res.status(200).json(users);   
    },


    // *************************************AddsOne*****************
 

    babycareAddsOneP: async (req, res, next) => {
 
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsOnea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImg, function (err, data) {
                    allData.addsOneImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOne.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOne.find({babycareAreaForAddsOnea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOne.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsTwo*****************
     
     
    babycareAddsTwoP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsTwoa = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImg, function (err, data) {
                    allData.addsTwoImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwo.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
    babycareAddsTwoGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwo.find({babycareAreaForAddsTwoa:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwo.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsThree*****************
    
    
    babycareAddsThreeP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsThreea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImg, function (err, data) {
                    allData.addsThreeImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThree.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThree.find({babycareAreaForAddsThreea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThree.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsFour*****************
    
     
     
    babycareAddsFourP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsFoura = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImg, function (err, data) {
                    allData.addsFourImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFour.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFour.find({babycareAreaForAddsFoura:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFour.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
     
    // *************************************AddsOneLoc*****************
    
    
    babycareAddsOneLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocOne = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImgLoc, function (err, data) {
                    allData.addsOneImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOneLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOneLoc.find({babycaresAreaForAddsLocOne:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOneLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsTwoLoc*****************
    
    
    babycareAddsTwoLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocTwo = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImgLoc, function (err, data) {
                    allData.addsTwoImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwoLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsTwoLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwoLoc.find({babycaresAreaForAddsLocTwo:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwoLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsThreeLoc*****************
    
    
    babycareAddsThreeLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocThree = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImgLoc, function (err, data) {
                    allData.addsThreeImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThreeLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThreeLoc.find({babycaresAreaForAddsLocThree:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThreeLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFourLoc*****************
    
    
    babycareAddsFourLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFour = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImgLoc, function (err, data) {
                    allData.addsFourImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFourLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFourLoc.find({babycaresAreaForAddsLocFour:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFourLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFiveLoc*****************
    
    
    babycareAddsFiveLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFive = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFiveImgLoc, function (err, data) {
                    allData.addsFiveImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFiveLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFiveLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFiveLoc.find({babycaresAreaForAddsLocFive:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFiveDeleteLoc: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFiveLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    }, 
    }
    