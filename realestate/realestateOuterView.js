const mongoose=require('mongoose');

const SchemaTypeDescriptions= new mongoose.Schema({
      
  
    outerFsideNme:{type:String},
    outerFsideDes:{type:String},
    outerFsideimg:{type:String},  // done

    outerBsideNme:{type:String},
    outerBsideDes:{type:String},
    outerBsideimg:{type:String}, // done

    outerLsideNme:{type:String},
    outerLsideDes:{type:String},
    outerLsideimg:{type:String}, // done

    outerRsideNme:{type:String},
    outerRsideDes:{type:String},
    outerRsideimg:{type:String}, // done
    outerOptionalDescriptions:{type:String},
 


    realestateMainForOuterView:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateTypeDescriptionsMain'
}
    
},
{timestamps:true});
const TypeDescriptions=mongoose.model('realestateOuterView',SchemaTypeDescriptions);

module.exports=TypeDescriptions;

