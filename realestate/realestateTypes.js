const mongoose=require('mongoose');

const SchemaType= new mongoose.Schema({

    realestateType:{type:String},
    images:{type:String},
 
    realestateClientsAreaForTypes:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateAreasForClients'
}
    
},
{timestamps:true});
const   TypesSche=mongoose.model('realestateTypes',SchemaType);

module.exports=TypesSche;

