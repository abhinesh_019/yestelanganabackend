const mongoose=require('mongoose');

const SchemaTypeDescriptions= new mongoose.Schema({
      
  
    amenitiesNames1:{type:String},
    amenitiesNames2:{type:String},
    amenitiesNames3:{type:String},
    amenitiesNames4:{type:String},
    amenitiesNames5:{type:String},
    amenitiesNames6:{type:String},
    amenitiesNames7:{type:String},
    amenitiesNames8:{type:String},
    amenitiesNames9:{type:String},
    amenitiesNames10:{type:String},
    amenitiesNames11:{type:String},
    amenitiesNames12:{type:String},
    amenitiesNames13:{type:String},
    amenitiesNames14:{type:String},
    amenitiesNames15:{type:String},
    amenitiesNames16:{type:String},
    amenitiesNames17:{type:String},
    amenitiesNames18:{type:String},
    amenitiesNames19:{type:String},
    amenitiesNames20:{type:String},


    amenitiesImg1:{type:String},
    amenitiesImg2:{type:String},
    amenitiesImg3:{type:String},
    amenitiesImg4:{type:String},
    amenitiesImg5:{type:String},
    amenitiesImg6:{type:String},
    amenitiesImg7:{type:String},
    amenitiesImg8:{type:String},
    amenitiesImg9:{type:String},
    amenitiesImg10:{type:String},
    amenitiesImg11:{type:String},
    amenitiesImg12:{type:String},
    amenitiesImg13:{type:String},
    amenitiesImg14:{type:String},
    amenitiesImg15:{type:String},
    amenitiesImg16:{type:String},
    amenitiesImg17:{type:String},
    amenitiesImg18:{type:String},
    amenitiesImg19:{type:String},
    amenitiesImg20:{type:String},




    realestateMainForAminities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateTypeDescriptionsMain'
}
    
},
{timestamps:true});
const TypeDescriptions=mongoose.model('realestateAminities',SchemaTypeDescriptions);

module.exports=TypeDescriptions;

