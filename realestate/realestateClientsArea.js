const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    clientArea:String,

    realestateClientsForAreasSeperate:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateClients'
}
   
},
{timestamps:true});
const areas=mongoose.model('realestateAreasForClients',areaSchema);
module.exports=areas;