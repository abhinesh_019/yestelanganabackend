const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    businesTypeRooms:String,

    realestateTypesForBusinesRooms:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateBusinesTypes'
}
   
},
{timestamps:true});
const areas=mongoose.model('realestatebusinesrooms',areaSchema);
module.exports=areas;