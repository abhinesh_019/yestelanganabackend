const express= require('express');
var router = express.Router();
const userControllers = require('../realestate/realestateControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

 


//  ************ Locations ***********

router.route('/realestateLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/realestateAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/realestateClientsget/:AreaId')
.get(userControllers.index)

router.route('/realestateClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/realestateClientsAllCount') 
.get(userControllers.totalclients)

router.route('/realestateClientsAllCountLocations') 
.get(userControllers.totalclientsLocations)

router.route('/realestateClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/realestateClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/realestateClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************
// ******************* Clients create area ****************** done
router.route('/realestateAreaGet/:clientId')
.get(userControllers.clientsAreaget)
 
router.route('/realestateclientsAreaPost/:clientId')
.post(userControllers.clientsAreapost);

// *******************************************************
// ******************* Clients real estate type  ****************** done
router.route('/realestatefecitiesGets/:areaClientId')
.get(userControllers.typesgets)
router.route('/realestatefecitiesGetCounts/:areaClientId')
.get(userControllers.typesgetCounts)
router.route('/realestateTypesPost/:areaClientId')
.post(multipartMiddleware,userControllers.typespost);

// *******************************************************
// ******************* Clients real estate Busines type  ****************** done
router.route('/realestateBusinesGets/:typesClientId')
.get(userControllers.typesBusinesgets)
router.route('/realestateBusinesGetCounts/:typesClientId')
.get(userControllers.typesBusinesgetCounts)
router.route('/realestateTypesBusinesPost/:typesClientId')
.post(multipartMiddleware,userControllers.typesBusinespost);

// *******************************************************

// ******************* Clients real estate Busines Logic Types  ******************
router.route('/realestateBusinesRoomsGets/:typesClientId')
.get(userControllers.typesBusinesRoomsgets)
router.route('/realestateBusinesRoomsGetCounts/:typesClientId')
.get(userControllers.typesBusinesgetRoomsCounts)
router.route('/realestateTypesBusinesRoomsPost/:typesClientId')
.post(multipartMiddleware,userControllers.typesBusinesRoomspost);

// *******************************************************


// ******************* Clients real estate Busines main details Types  ******************
router.route('/realestateBusinesMainGets/:typesClientId')
.get(userControllers.typesBusinesMaingets)

router.route('/realestateBusinesMainGetsInd/:mainId')
.get(userControllers.typesBusinesMaingetsInd)

router.route('/realestateBusinesMainArea/:typesClientId')
.get(userControllers.typesBusinesMaingetArea)
 


router.route('/realestateBusinesMainGetsREType/:typesClientId')
.get(userControllers.typesBusinesMaingetsREType)
 
router.route('/realestateBusinesMainGetLogic/:typesClientId')
.get(userControllers.typesBusinesMaingetLogic)
 
router.route('/realestateBusinesMainGetLogicRoom/:typesClientId')
.get(userControllers.typesBusinesMaingetLogicRoom)
 

router.route('/realestateTypesBusinesMainPost/:typesClientId')
.post(multipartMiddleware,userControllers.typesBusinesMainpost);

// *******************************************************
// ******************* Clients real estate Busines main details Outerview  ******************
router.route('/realestateOuterviewGets/:typesClientId')
.get(userControllers.typesOuterviewgets)

router.route('/realestateOuterviewGetsind/:typesClientId')
.get(userControllers.typesOuterviewgetsind)
 
router.route('/realestateOuterviewPost/:typesClientId')
.post(multipartMiddleware,userControllers.typesOuterviewpost);

// *******************************************************
// ******************* Clients real estate Busines main details InnerViews  ******************
router.route('/realestateInnerViewsGets/:typesClientId')
.get(userControllers.typesInnerViewsgets)
 
router.route('/realestateInnerViewsost/:typesClientId')
.post(multipartMiddleware,userControllers.typesInnerViewspost);

// *******************************************************


// ******************* Clients real estate Overviews  ******************
router.route('/realestateOverviewsGets/:typesClientId')
.get(userControllers.typesOverviewsgets)
router.route('/realestateOverviewsCounts/:typesClientId')
.get(userControllers.typesOverviewsCounts)
router.route('/realestateOverviewspost/:typesClientId')
.post(userControllers.typesOverviewspost);

// *******************************************************
// ******************* Clients real estate Busines main details locationAdvantages  ******************
router.route('/realestatelocationAdvantagesGets/:typesClientId')
.get(userControllers.typeslocationAdvantagesgets)
router.route('/realestatelocationAdvantagesCounts/:typesClientId')
.get(userControllers.typeslocationAdvantagesCounts)
router.route('/realestatelocationAdvantagesPost/:typesClientId')
.post(multipartMiddleware,userControllers.typeslocationAdvantagespost);

// *******************************************************
// ******************* Clients real estate Busines main details Amenities  ******************
router.route('/realestateAmenitiesGets/:typesClientId')
.get(userControllers.typesAmenitiesgets)
router.route('/realestateAmenitiesCounts/:typesClientId')
.get(userControllers.typesAmenitiesCounts)
router.route('/realestateAmenitiesPost/:typesClientId')
.post(multipartMiddleware,userControllers.typesAmenitiespost);

// *******************************************************

 // ****************** only updates details ****************
router.route('/realestateupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/realestateupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/realestateupdatesgetpostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/realestateupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/realestateupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/realestateupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/realestateupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/realestateupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/realestateeuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/realestateCeuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

// *************************************** AddsOne ************************************* 
router.route('/realestateAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/realestateAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/realestateAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/realestateAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/realestateAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/realestateAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/realestateAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/realestateAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/realestateAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/realestateAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/realestateAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/realestateAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/realestateAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/realestateAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/realestateAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/realestateAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/realestateAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/realestateAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/realestateAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/realestateAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/realestateAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/realestateAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/realestateAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/realestateAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/realestateAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/realestateAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/realestateAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
