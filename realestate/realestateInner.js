const mongoose=require('mongoose');

const SchemaTypeDescriptions= new mongoose.Schema({
      
  
  innerNameTitle:{type:String},

    innerName1:{type:String},
    innerName2:{type:String},
    innerName3:{type:String},
    innerName4:{type:String},
    innerName5:{type:String},
    innerName6:{type:String},
    innerName7:{type:String},
    innerName8:{type:String},
    innerName9:{type:String},
    innerName10:{type:String},
    innerName11:{type:String},
    innerName12:{type:String},
    innerName13:{type:String},
    innerName14:{type:String},
    innerName15:{type:String},
    innerName16:{type:String},
    innerName17:{type:String},
    innerName18:{type:String},
    innerName19:{type:String},
    innerName20:{type:String},

    innerNdes1:{type:String},
    innerNdes2:{type:String},
    innerNdes3:{type:String},
    innerNdes4:{type:String},
    innerNdes5:{type:String},
    innerNdes6:{type:String},
    innerNdes7:{type:String},
    innerNdes8:{type:String},
    innerNdes9:{type:String},
    innerNdes10:{type:String},
    innerNdes11:{type:String},
    innerNdes12:{type:String},
    innerNdes13:{type:String},
    innerNdes14:{type:String},
    innerNdes15:{type:String},
    innerNdes16:{type:String},
    innerNdes17:{type:String},
    innerNdes18:{type:String},
    innerNdes19:{type:String},
    innerNdes20:{type:String},

    innerNimg1:{type:String}, // done
    innerNimg2:{type:String}, // done
    innerNimg3:{type:String}, // done
    innerNimg4:{type:String}, // done
    innerNimg5:{type:String},  // done
    innerNimg6:{type:String}, // done
    innerNimg7:{type:String}, // done
    innerNimg8:{type:String}, // done
    innerNimg9:{type:String}, // done
    innerNimg10:{type:String}, // done
    innerNimg11:{type:String}, // done
    innerNimg12:{type:String}, // done
    innerNimg13:{type:String}, // done
    innerNimg14:{type:String}, // done
    innerNimg15:{type:String}, // done
    innerNimg16:{type:String},  // done
    innerNimg17:{type:String}, // done
    innerNimg18:{type:String}, // done
    innerNimg19:{type:String}, // done
    innerNimg20:{type:String}, // done



    realestateMainForInnerView:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateTypeDescriptionsMain'
}
    
},
{timestamps:true});
const TypeDescriptions=mongoose.model('realestateInnerView',SchemaTypeDescriptions);

module.exports=TypeDescriptions;

