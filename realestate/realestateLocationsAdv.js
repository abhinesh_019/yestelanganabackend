const mongoose=require('mongoose');

const SchemaTypeDescriptions= new mongoose.Schema({
      
  
  
    lAdvantagesName1:{type:String},
    lAdvantagesName2:{type:String},
    lAdvantagesName3:{type:String},
    lAdvantagesName4:{type:String},
    lAdvantagesName5:{type:String},
    lAdvantagesName6:{type:String},
    lAdvantagesName7:{type:String},
    lAdvantagesName8:{type:String},
    lAdvantagesName9:{type:String},
    lAdvantagesName10:{type:String},
    lAdvantagesName11:{type:String},
    lAdvantagesName12:{type:String},
    lAdvantagesName13:{type:String},
    lAdvantagesName14:{type:String},
    lAdvantagesName15:{type:String},
    lAdvantagesName16:{type:String},
    lAdvantagesName17:{type:String},
    lAdvantagesName18:{type:String},
    lAdvantagesName19:{type:String},
    lAdvantagesName20:{type:String},

    lAdvantagesImg1:{type:String},
    lAdvantagesImg2:{type:String},
    lAdvantagesImg3:{type:String},
    lAdvantagesImg4:{type:String},
    lAdvantagesImg5:{type:String},
    lAdvantagesImg6:{type:String},
    lAdvantagesImg7:{type:String},
    lAdvantagesImg8:{type:String},
    lAdvantagesImg9:{type:String},

    lAdvantagesImg10:{type:String},
    lAdvantagesImg11:{type:String},
    lAdvantagesImg12:{type:String},
    lAdvantagesImg13:{type:String},
    lAdvantagesImg14:{type:String},
    lAdvantagesImg15:{type:String},
    lAdvantagesImg16:{type:String},
    lAdvantagesImg17:{type:String},
    lAdvantagesImg18:{type:String},
    lAdvantagesImg19:{type:String},
    lAdvantagesImg20:{type:String},



    realestateMainForLocationAdvantages:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateTypeDescriptionsMain'
}
    
},
{timestamps:true});
const TypeDescriptions=mongoose.model('realestateLocationAdvantages',SchemaTypeDescriptions);

module.exports=TypeDescriptions;

