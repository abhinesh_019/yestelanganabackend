const mongoose=require('mongoose');

const SchemaCommenlsOnly= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,
  
    realestateClientsForCommenlsOnly:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateClients'
}
    
},
{timestamps:true});
const   commenlsOnly=mongoose.model('realestateOnlyComments',SchemaCommenlsOnly);

module.exports=commenlsOnly;

