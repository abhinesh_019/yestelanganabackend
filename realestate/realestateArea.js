const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    realestateLocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('realestateAreas',areaSchema);
module.exports=areas;