const mongoose=require('mongoose');

const SchemaTypeDescriptions= new mongoose.Schema({
      
  
    overview1:{type:String},
    overview2:{type:String},
    overview3:{type:String},
    overview4:{type:String},
    overview5:{type:String},
    overview6:{type:String},
    overview7:{type:String},
    overview8:{type:String},
    overview9:{type:String},
    overview10:{type:String},
    overview11:{type:String},
    overview12:{type:String},
    overview13:{type:String},
    overview14:{type:String},
    overview15:{type:String},
    overview16:{type:String},
    overview17:{type:String},
    overview18:{type:String},
    overview19:{type:String},
    overview20:{type:String},
 


    realestateMainForOverView:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'realestateTypeDescriptionsMain'
}
    
},
{timestamps:true});
const TypeDescriptions=mongoose.model('realestateOverView',SchemaTypeDescriptions);

module.exports=TypeDescriptions;

