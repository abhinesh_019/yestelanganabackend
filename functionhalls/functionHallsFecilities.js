const mongoose=require('mongoose');

const functionsHallsSchemaAreas= new mongoose.Schema({

    name:{type:String},
    importantsKey:{type:String},
    descriptions:{type:String},
    maintenance:{type:String},
    images1:{type:String},
 
    functionsHallsClientsForFecilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'functionsHallClient'
}
    
},
{timestamps:true});
const functionsHallsArea=mongoose.model('functionsHallsFecilities',functionsHallsSchemaAreas);

module.exports=functionsHallsArea;

