const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    brideName:String,
    groomeName:String,
    Place:String,
    images1:String,
    functionHallsClientsForCustomers:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'functionsHallClient'
    }
    
},
{timestamps:true});

const comm=mongoose.model('functionHallsCustomers',commentsSchema);
module.exports=comm;
