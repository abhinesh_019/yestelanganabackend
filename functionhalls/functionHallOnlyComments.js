const mongoose=require('mongoose');
const hallSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,

    hallClientsForGeneralComments:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'functionsHallClient'
    }
},
{timestamps:true});
const comm=mongoose.model('functionHallGeneralComments',hallSchema);
module.exports=comm;