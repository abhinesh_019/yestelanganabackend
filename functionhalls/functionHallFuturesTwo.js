const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    subTitle:String,
    value:String,
    images:String,
    descriptions:String,

     functionHallsClientsForFuturesTwo:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'functionHallsFutures'
    }
 
},
{timestamps:true});

const comm=mongoose.model('functionHallsFuturesTwo',commentsSchema);
module.exports=comm;


