var hallsC = require('../functionhalls/functionshallscatageries')
var hallsL = require('../functionhalls/functionshallslocations')
var hallsA = require('../functionhalls/functionshallsAreas')
var hallsClients = require('../functionhalls/functionHallsClients')
var hallsClientsUpdates = require('../functionhalls/functionHallsUpdates')
var hallsClientsUpdatesComments = require('../functionhalls/functionHallUpdatesComments')
var hallsCustomers = require('../functionhalls/functionshallsCustomers')
var hallsClientsUpdatesCommentsReply = require('../functionhalls/functionHallCommentsReply')
var hallsClientsOnlyComments = require('../functionhalls/functionHallOnlyComments')
var hallsFecilities = require('../functionhalls/functionHallsFecilities')
var hallFutures = require('../functionhalls/functionHallFutures')
var hallFuturesTwo = require('../functionhalls/functionHallFuturesTwo')
const fileService = require('../functionhalls/functionsHallsServer');
  

var babycaresAddsOne=require('../functionhalls/fightsAddsOnea');
var babycaresAddsTwo=require('../functionhalls/fightsAddsTwoa');
var babycaresAddsThree=require('../functionhalls/fightsAddsThreea');
var babycaresAddsFour=require('../functionhalls/fightsAddsFoura');
 
var babycaresAddsOneLoc=require('../functionhalls/fightsAddsOnel');
var babycaresAddsTwoLoc=require('../functionhalls/fightsAddsTwol');
var babycaresAddsThreeLoc=require('../functionhalls/fightsAddsThreel');
var babycaresAddsFourLoc=require('../functionhalls/fightsAddsFourl');
var babycaresAddsFiveLoc=require('../functionhalls/fightsAddsFivel');

var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 
var async = require('async');
  
module.exports = {
// ********************categories*****************
Categoriesposts:(req, res, next) => {

    let allData = req.body;
 
    async.parallel([

        function (callback) {

            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
          
            hallsC.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                    res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
                   });
        }
    });
},

 

Categoriesget: async(req,res,next)=>{
    const users = await hallsC.find({}).sort({"locations":1});
       res.status(200).json(users);

   console.log(users,"users");
},

CategoriesidgetId:async (req, res, next) => {
       const { CategoriesId } = req.params;
       const usersposts = await hallsC.findById(CategoriesId)
       res.status(200).json(usersposts);
        },
 // ******************************************

// ********************* Locations *********************

locationspost: async(req,res,next)=>{
    const { catagereiesId } = req.params;
    req.body.functionhallsCatageriousForLocations = catagereiesId
     const userscomm = new hallsL(req.body);
    await userscomm.save();
    res.status(201).json(userscomm);
},


locationsget: async(req,res,next)=>{
    
    const { catagereiesId } = req.params;
    const users = await hallsL.find({functionhallsCatageriousForLocations:catagereiesId}).sort({"locations":1});
       res.status(200).json(users);
 },
// ******************************************

// *********************** Areas *******************

Areasposts : async(req,res,next)=>{
    const { LocationsId } = req.params;
    req.body.functionsHallsLocationsForAreas = LocationsId
     const userscomm = new hallsA(req.body);
    await userscomm.save();
        res.status(201).json(userscomm);
},
 
Areasget:async (req, res, next) => {
     const { LocationsId } = req.params;
     const usersposts = await hallsA.find({functionsHallsLocationsForAreas:LocationsId}).sort({"area":-1})
    res.status(200).json(usersposts);
 },

// ******************************************
// ************************************************ Clients Updates ***************************************
 
newUser: (req, res, next) => {

        
    const { AreaId } = req.params;
    req.body.functionHallsAreaForClients = AreaId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null,data.Location);
            })
        },
        function(callback) {
            fileService.uploadImage(req.files.images4, function (err, data) {
                allData.images4 = data.Location;
                callback(null, data.Location);
            })
        },
 
        function (callback) {
            fileService.uploadImage(req.files.images5, function (err, data) {
                allData.images5 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images6, function (err, data) {
                allData.images6 = data.Location;
                callback(null, data.Location);
            })
        },
         

    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            hallsClients.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},



index:async (req, res, next) => {

    const { AreaId } = req.params;
    let search=req.query.search;

    const usersposts = await hallsClients.find({functionHallsAreaForClients:AreaId,$or:[
        {subArea:new RegExp(search, "gi")},
        {pincode:new RegExp(search, "gi")}, 
        {shopName:new RegExp(search, "gi")},
        {place:new RegExp(search, "gi")},
    ]
}).sort({"subArea":-1})
    res.status(200).json(usersposts);
 },

usersgetcoutarea: async (req, res, next) => {
    const { AreaId } = req.params;

    const users = await hallsClients.aggregate([{$match:{functionHallsAreaForClients:ObjectId(AreaId)}},

        {"$group" : {_id:{subArea:"$subArea"}, count:{$sum:1}}},{$sort:{"subArea":1}}
     ])
    
    res.status(200).json(users);
 },
 
tuserscount:async (req, res, next) => {
     const { AreaId } = req.params;
     const usersposts = await hallsClients.find({functionHallsAreaForClients:AreaId}).count()
    res.status(200).json(usersposts);
 },


totalclients:async (req, res, next) => {
  const usersposts = await hallsClients.find({}).count()
    res.status(200).json(usersposts);
  
},
 
clientsGetinds:async (req, res, next) => {

    const { clientsId } = req.params;
    const usersposts = await hallsClients.findById(clientsId) 
    res.status(200).json(usersposts);
  
}, 
countLocationsFunctionsHalls: async (req, res, next) => {
    const users = await hallsClients.aggregate([ 
       { $match: { hallCatageries : "function hall"  } },
          {"$group" : {_id:{distict:"$distict"},count:{$sum:1}}}
    ]).sort({"distict":1})
     res.status(200).json(users);
},
countLocationsBanquietHalls: async (req, res, next) => {

   const users = await hallsClients.aggregate([ 
       { $match: { hallCatageries : "banquet hall"}},
          {"$group" : {_id:{distict:"$distict"},count:{$sum:1}}},{$sort:{"distict":1}}
    ]).sort({"distict":1})
     res.status(200).json(users);
},

indexs: async (req, res, next) => {

   const users = await hallsClients.aggregate(
       [         {"$group" : {_id:{hallCatageries:"$hallCatageries"},count:{$sum:1}}}

        ]
   );
   
   res.status(200).json(users);
  console.log("users****", users);

},
 
// ***********************************************************************


// ************************************************ Clients fecilities ***************************************
 
fecilitiespost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.functionsHallsClientsForFecilities = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            hallsFecilities.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},



fecilitiesget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await hallsFecilities.find({functionsHallsClientsForFecilities:clientId}).sort({"createdAt":-1})
    res.status(200).json(usersposts);
 },
 fecilitiesgetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await hallsFecilities.find({functionsHallsClientsForFecilities:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************


// ************************************************ Clients customers ***************************************
 
customerspost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.functionHallsClientsForCustomers = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            hallsCustomers.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},



customersget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await hallsCustomers.find({functionHallsClientsForCustomers:clientId}).sort({"createdAt":-1})
    res.status(200).json(usersposts);
 },
 customersgetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await hallsCustomers.find({functionHallsClientsForCustomers:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************




// ****************users updates*************

UsersUpdates: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.functionHallsClientsForUpdates = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.viedoes,"req.files.viedoes 302");
            if(req.files.viedoes){
                fileService.uploadImage(req.files.viedoes, function (err, data) {
                    allData.viedoes = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            hallsClientsUpdates.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

UsersGet:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await hallsClientsUpdates.find({functionHallsClientsForUpdates:ClientsId}).sort({"createdAt":-1})

        res.status(200).json(users);
       console.log("users****", users);
},

UsersGetCounts:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await hallsClientsUpdates.find({functionHallsClientsForUpdates:ClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
//admin users updates comments
updatesCommentsposts:async (req, res, next) => {

const { updatesId } = req.params;
  req.body.hallUpdatesForComments = updatesId 
const userscomm = new hallsClientsUpdatesComments(req.body);

await userscomm.save();
console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
 res.status(201).json(userscomm);
},

updatesCommentsGet:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await hallsClientsUpdatesComments.find({hallUpdatesForComments:updatesId})
    res.status(200).json(users); 
},
updatesCommentsGetcounts:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await hallsClientsUpdatesComments.find({hallUpdatesForComments:updatesId}).count()
    res.status(200).json(users);
},

updatesCommentsReplyposts:async (req, res, next) => {
const { updatesCommId } = req.params;
  req.body.hallUpdatesCommForReply = updatesCommId 
const userscomm = new hallsClientsUpdatesCommentsReply(req.body);
await userscomm.save();
console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
res.status(201).json(userscomm);
},

updatesCommentsReplyGet:async (req, res, next) => {

const { updatesCommId } = req.params;

    const users = await hallsClientsUpdatesCommentsReply.find({hallUpdatesCommForReply:updatesCommId})

    res.status(200).json(users);
   console.log("users**** 399", users);
},

// ****************only comments *******************
usersCommentsposts : async (req,res) => {
const { ClientsId } = req.params;
req.body.hallClientsForGeneralComments = ClientsId
const generalcomm = new hallsClientsOnlyComments(req.body);
await generalcomm.save();
res.status(201).json(generalcomm);
},
usersCommentsget:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await hallsClientsOnlyComments.find({hallClientsForGeneralComments:ClientsId})
res.status(200).json(users);   
},
usersCommentsgetCounts:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await hallsClientsOnlyComments.find({hallClientsForGeneralComments:ClientsId}).count()
res.status(200).json(users); 
},

// ****************** AdditionalFutures ****************

AdditionalFuturesposts : async (req,res) => {
    const { ClientsId } = req.params;
    req.body.functionHallsClientsForFutures = ClientsId
    const generalcomm = new hallFutures(req.body);
    await generalcomm.save();
    res.status(201).json(generalcomm);
    },
    AdditionalFuturesget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await hallFutures.find({functionHallsClientsForFutures:ClientsId})
    res.status(200).json(users);   
    },

    // ****************** AdditionalFuturesTwo ****************

   
    AdditionalFuturesTwoposts: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.functionHallsClientsForFuturesTwo = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
        },
        
       
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            hallFuturesTwo.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

    AdditionalFuturesTwoget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await hallFuturesTwo.find({functionHallsClientsForFuturesTwo:ClientsId})
    res.status(200).json(users);   
    },
    
    

    
    // *************************************AddsOne*****************
 

    babycareAddsOneP: async (req, res, next) => {
 
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsOnea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImg, function (err, data) {
                    allData.addsOneImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOne.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOne.find({babycareAreaForAddsOnea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOne.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsTwo*****************
     
     
    babycareAddsTwoP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsTwoa = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImg, function (err, data) {
                    allData.addsTwoImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwo.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
    babycareAddsTwoGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwo.find({babycareAreaForAddsTwoa:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwo.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsThree*****************
    
    
    babycareAddsThreeP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsThreea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImg, function (err, data) {
                    allData.addsThreeImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThree.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThree.find({babycareAreaForAddsThreea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThree.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsFour*****************
    
     
     
    babycareAddsFourP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsFoura = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImg, function (err, data) {
                    allData.addsFourImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFour.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFour.find({babycareAreaForAddsFoura:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFour.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
     
    // *************************************AddsOneLoc*****************
    
    
    babycareAddsOneLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocOne = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImgLoc, function (err, data) {
                    allData.addsOneImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOneLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOneLoc.find({babycaresAreaForAddsLocOne:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOneLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsTwoLoc*****************
    
    
    babycareAddsTwoLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocTwo = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImgLoc, function (err, data) {
                    allData.addsTwoImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwoLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsTwoLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwoLoc.find({babycaresAreaForAddsLocTwo:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwoLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsThreeLoc*****************
    
    
    babycareAddsThreeLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocThree = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImgLoc, function (err, data) {
                    allData.addsThreeImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThreeLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThreeLoc.find({babycaresAreaForAddsLocThree:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThreeLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFourLoc*****************
    
    
    babycareAddsFourLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFour = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImgLoc, function (err, data) {
                    allData.addsFourImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFourLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFourLoc.find({babycaresAreaForAddsLocFour:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFourLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFiveLoc*****************
    
    
    babycareAddsFiveLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFive = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFiveImgLoc, function (err, data) {
                    allData.addsFiveImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFiveLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFiveLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFiveLoc.find({babycaresAreaForAddsLocFive:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFiveDeleteLoc: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFiveLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    }, 
    }
    