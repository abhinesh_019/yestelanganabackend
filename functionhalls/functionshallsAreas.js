const mongoose=require('mongoose');

const functionsHallsSchemaAreas= new mongoose.Schema({

    area:String,
    
    functionsHallsLocationsForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'functionHallsLocations'
}
    
},
{timestamps:true});
const functionsHallsArea=mongoose.model('functionsHallsAreas',functionsHallsSchemaAreas);

module.exports=functionsHallsArea;

