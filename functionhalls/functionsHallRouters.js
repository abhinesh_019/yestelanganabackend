const express= require('express');
var router = express.Router();
const userControllers = require('../functionhalls/functionHallsControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ categories ***********
router.route('/Categories')
.get(userControllers. Categoriesget)
router.route('/CategoriesP')
 .post(multipartMiddleware,userControllers.Categoriesposts);

 router.route('/CategoriesIds/:CategoriesId')
 .get(userControllers.CategoriesidgetId)

//  ***********************


//  ************ Locations ***********

router.route('/locations/:catagereiesId')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/Areas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/hclientsget/:AreaId')
.get(userControllers.index)

router.route('/clientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/clientsAllCount') 
.get(userControllers.totalclients)

router.route('/clientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/clientsGetind/:clientsId')
.get(userControllers.clientsGetinds)


router.route('/clientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);



router.route('/UsersLocationsFunctionsHalls') 
.get(userControllers.countLocationsFunctionsHalls)

router.route('/UsersLocationsBanquietHalls') 
.get(userControllers.countLocationsBanquietHalls)


router.route('/ClientsgetLocationsCat')
.get(userControllers.indexs)
// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/fecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/fecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/fecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************

// ******************* Clients customers  ******************
router.route('/customersGet/:clientId')
.get(userControllers.customersget)
router.route('/customersGetCounts/:clientId')
.get(userControllers.customersgetCounts)
router.route('/fcustomersPost/:clientId')
.post(multipartMiddleware,userControllers.customerspost);

// *******************************************************


// ****************** only updates details ****************
router.route('/updatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/updatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/updatesgetpostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/updatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/updatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/updatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/updatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/updatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/userscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/userscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

// ****************** AdditionalFutures ****************
router.route('/functionalHallAdditionalFutures/:ClientsId')
.get(userControllers.AdditionalFuturesget)

router.route('/functionalHallAdditionalFuture/:ClientsId')
.post(userControllers.AdditionalFuturesposts);
 
// ****************** AdditionalFuturesTwo ****************
router.route('/functionalHallAdditionalFuturesTwo/:ClientsId')
.get(userControllers.AdditionalFuturesTwoget)
.post(multipartMiddleware,userControllers.AdditionalFuturesTwoposts);

// *************************************** AddsOne ************************************* 
router.route('/functionalHallAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/functionalHallAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/functionalHallAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/functionalHallAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/functionalHallAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/functionalHallAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/functionalHallAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/functionalHallAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/functionalHallAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/functionalHallAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/functionalHallAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/functionalHallAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/functionalHallAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/functionalHallAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/functionalHallAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/functionalHallAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/functionalHallAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/functionalHallAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/functionalHallAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/functionalHallAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/functionalHallAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/functionalHallAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/functionalHallAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/functionalHallAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/functionalHallAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/functionalHallAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/functionalHallAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
