const mongoose=require('mongoose');

const TutionSchemaDeg= new mongoose.Schema({
    BusinessDescription:String,
    ComputingDescription:String,
    EngineeringDescription:String,
    HumaniliesDescription:String,
    LanguageDescription:String,
    LawDescription:String,
    PsychologyDescription:String,
    ScienceDescription:String,
    BusinessTutors:String,
    ComputingTutors:String,
    EngineeringTutors:String,
    HumaniliesTutors:String,
    LanguageTutors:String,
    LawTutors:String,
    PsychologyTutors:String,
    ScienceTutors:String,
    BusinessYearExp:String,
    ComputingYearExp:String,
    EngineeringYearExp:String,
    HumaniliesYearExp:String,
    LanguageYearExp:String,
    LawYearExp:String,
    PsychologyYearExp:String,
    ScienceYearExp:String,

    tutionUserForDegree:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'tutionsUsers'
}
    
},
{timestamps:true});

const tutionDegs=mongoose.model('tutionDegree',TutionSchemaDeg);
module.exports=tutionDegs;

