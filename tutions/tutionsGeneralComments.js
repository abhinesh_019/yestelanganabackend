const mongoose=require('mongoose');

const tutionsCommentsSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,
   
    tutionUsers:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'tutionsUsers'
}
},
{timestamps:true});

const tutionsComments=mongoose.model('tutionsGeneralComments',tutionsCommentsSchema);
module.exports=tutionsComments;