const express= require('express');
var router = express.Router();
const userControllers = require('../tutions/tutionsControllers');
var middleware = require('../middleware/middleware')

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();


//  ************ categories ***********
router.route('/tutionsCategories')
.get(userControllers.tutionsCategoriesget)
router.route('/tutionsCategorie')
 .post(multipartMiddleware,userControllers.tutionsCategoriesposts);

 router.route('/tutionsCategories/:tutionsCategoriesId')
 .get(userControllers.tutionsCategoriesidget)


//  ***********************



router.route('/tutionslocations/:tutionsCatageroiusId')
.get(userControllers.tutionslocationsget)
 .post(userControllers.tutionslocationspost);

//  ************ Area ***********
 router.route('/tutionsAreas/:tutionsLocationsId')
 .get(userControllers.tutionsAreasget)
  .post(userControllers.tutionsAreasposts);

//  ***********************

// ******************* users details ******************
router.route('/tutionshUsers/:tutionsAreaId')
.get(userControllers.index)

router.route('/tutionsUsersAreas/:tutionsAreaId')
.get(userControllers.tutionsusersgetcoutarea)
 

router.route('/tutionshUsersAllCount')  //done (4)
.get(userControllers.totalUsers)

router.route('/tutionshUsersCount/:tutionsAreaId')  //done 
.get(userControllers.tuserscount)


router.route('/tutionshUsersin/:tutionsUsersId')
.get(userControllers.indexs)

router.route('/tutionsClientsAllCountLocations') 
.get(userControllers.totalclientsLocations)

router.route('/tutionhUser/:tutionsAreaId')
.post(multipartMiddleware,userControllers.newUser);

// ********************************* LKG TO SSC **********************

router.route('/tutionshUserSSC/:tutionsUsersId')
.get(userControllers.sscGet)


router.route('/tutionshUserSSCp/:tutionsUsersId')
.post(userControllers.sscPost);


// *************************************************************

// ********************************* Intermediate **********************

router.route('/tutionshUserInt/:tutionsUsersId')
.get(userControllers.intGet)


router.route('/tutionshUserIntp/:tutionsUsersId')
.post(userControllers.intPost);


// *************************************************************

// ********************************* degree **********************

router.route('/tutionshUserDegree/:tutionsUsersId')
.get(userControllers.degGet)


router.route('/tutionshUserDegreep/:tutionsUsersId')
.post(userControllers.degPost);


// *************************************************************
// ********************************* updates main **********************

router.route('/tutionsUserUpdates/:tutionsUsersId')
.get(userControllers.tutionsUsersGet)

router.route('/tutionsUserUpdatesCount/:tutionsUsersId')
.get(userControllers.tutionsUsersGetCounts)

router.route('/tutionsUserUpdatesp/:tutionsUsersId')
.post(multipartMiddleware,userControllers.postupdates);



// *************************************************************

// ********************************* tutions updates Comments **********************

router.route('/tutionsgCommentsUpdatesg/:tutionsUpdatesId')
.get(userControllers.tutionsUpdatesComments)

router.route('/tutionsgCommentUpdatesCountg/:tutionsUpdatesId')
.get(userControllers.tutionsUpdatesCommentsCounts)



router.route('/tutionspCommentsUpdatesp/:tutionsUpdatesId')
.post(userControllers.postUpdatesComments);


// *************************************************************

// ********************************* tutions Reply updates Comments **********************

router.route('/tutionsgCommentsReplyUpdatesg/:tutionsUpdatesCommId')
.get(userControllers.getUpdatesCommentsReply)
 

router.route('/tutionspCommentsReplyUpdatesp/:tutionsUpdatesCommId')
.post(userControllers.postUpdatesCommentsReply);


// *************************************************************

// ********************************* general comments **********************

router.route('/tutionsgComments/:tutionsUsersId')
.get(userControllers.tutionsComments)

router.route('/tutionsgComment/:tutionsUsersId')
.get(userControllers.tutionsCommentsCounts)



router.route('/tutionspComments/:tutionsUsersId')
.post(userControllers.postComments);


// *************************************************************

// *************************************** AddsOne ************************************* 
router.route('/tutionsAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/tutionsAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/tutionsAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/tutionsAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/tutionsAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/tutionsAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/tutionsAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/tutionsAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/tutionsAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/tutionsAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/tutionsAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/tutionsAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/tutionsAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/tutionsAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/tutionsAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/tutionsAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/tutionsAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/tutionsAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/tutionsAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/tutionsAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/tutionsAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/tutionsAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/tutionsAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/tutionsAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/tutionsAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/tutionsAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/tutionsAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;







