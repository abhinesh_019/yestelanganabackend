const mongoose=require('mongoose');
 
const babycareSchema= new mongoose.Schema(
    {
        addsFourLink:{type:String},
        addsFourImg:{type:String},   
           babycareAreaForAddsFoura:{
              type:mongoose.Schema.Types.ObjectId,
              ref:'tutionsAreas'
                     }
 },

{timestamps:true}

);

const babycare=mongoose.model('tutionsAddsFoura',babycareSchema);
module.exports=babycare;