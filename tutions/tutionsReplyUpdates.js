const mongoose=require('mongoose');

const TutionSchemaUpCommReply= new mongoose.Schema({

    descriptions:String,
    
    tutionUpdatedsComm:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'tutionsUpdatesComments'
}
    
},
{timestamps:true});

const tutionCommentsReply=mongoose.model('tutionsUpdatesCommentsReply',TutionSchemaUpCommReply);
module.exports=tutionCommentsReply;

