const mongoose=require('mongoose');
 
const  tutionsSchema= new mongoose.Schema(
    {
             name:{type:String},
             tutionsName:{type:String},
             myProfileDescriptionOne:{type:String},
             myProfileDescriptionTwo:{type:String},
             ourAimDescriptionOne:{type:String},
             ourAimDescriptionTwo:{type:String},
             parentsInformation:{type:String},
             day1:{type:String},
             day2:{type:String},
             day3:{type:String},
             day4:{type:String},
             day5:{type:String},
             day6:{type:String},
             day7:{type:String},
             allTimes:{type:String},
             houseNo:{type:String},
             mainArea:{type:String},
             subArea:{type:String},
             landmark:{type:String},
             city:{type:String},
             distict:{type:String},
             state:{type:String},
             pincode:{type:String},
             officeNo:{type:String},
             mobileNo:{type:String},
             whatsupno:{type:String},
             latitude:{type:String},
             longitude:{type:String},
             whatsupno:{type:String},
             emailId:{type:String},
             images1:{type:String},
             images2:{type:String},
             images3:{type:String},
             images4:{type:String},
             images5:{type:String},

             tutionArea:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'tutionsAreas'
     }

},

{timestamps:true}

);

const tutions=mongoose.model('tutionsUsers',tutionsSchema);
module.exports=tutions;