const mongoose=require('mongoose');

const TutionSchemaAreas= new mongoose.Schema({

    area:String,
    
    tutionLocations:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'tutionsLocations'
}
    
},
{timestamps:true});

const tutionArea=mongoose.model('tutionsAreas',TutionSchemaAreas);
module.exports=tutionArea;

