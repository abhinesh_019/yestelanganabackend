const mongoose=require('mongoose');

const TravelsSchemaAreas= new mongoose.Schema({

    area:String,
    
    travelsLocations:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'travelsLocations'
}
    
},
{timestamps:true});

const travelsArea=mongoose.model('travelsAreas',TravelsSchemaAreas);
module.exports=travelsArea;

