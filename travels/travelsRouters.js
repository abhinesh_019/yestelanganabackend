const express= require('express');
var router = express.Router();
const userControllers = require('../travels/travelsControllers');
var middleware = require('../middleware/middleware')

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

router.route('/travelsLocations')
.get(userControllers.travelslocationsget)
 .post(userControllers.travelslocationspost);

 router.route('/travelsAreas/:travelsLocationsId')
 .get(userControllers.travelsAreasget)
  .post(userControllers.travelsAreasposts);
module.exports = router;