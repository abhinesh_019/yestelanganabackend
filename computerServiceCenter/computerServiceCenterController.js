var locations = require('../computerServiceCenter/computerServiceCenterLocation');
var area = require('../computerServiceCenter/computerServiceCenterArea');
var clients = require('../computerServiceCenter/computerServiceCenterClient');
var users = require('../computerServiceCenter/computerServiceCenterUsers');
var successRate = require('../computerServiceCenter/computerServiceCenterSuccessRate');
var unsuccessRate = require('../computerServiceCenter/computerServiceCenterUnsuccessrate');
var brand = require('../computerServiceCenter/computerServiceCenterBrand');
var fileService = require('../computerServiceCenter/computerServiceCenterFileServer');
var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 
var async = require('async');
  
module.exports = {

// ********************* Locations *********************

locationspost: async(req,res,next)=>{
    const locs=  req.body;
    await locations.create(locs, function (err, data) {
        if (err) {
            console.log('Error in Saving user: ' + err);
        } else {

            res.status(201).json({ status: true, message: "user added sucessfully"});
        }
    });
 },



locationsget: async(req,res,next)=>{
    const users = await locations.find({}).sort({"locations":1});
       res.status(200).json(users);
 },
// ******************************************

// *********************** Areas *******************

Areasposts : async(req,res,next)=>{
    const { LocationsId } = req.params;
    req.body.LocationForAreas = LocationsId
     const userscomm = new area(req.body);
    await userscomm.save();
        res.status(201).json(userscomm);
},
 
Areasget:async (req, res, next) => {
     const { LocationsId } = req.params;
     const usersposts = await area.find({LocationForAreas:LocationsId}).sort({"area":-1})
    res.status(200).json(usersposts);
 },

// ******************************************
// ************************************************ Clients Updates ***************************************
 
  
newUser: (req, res, next) => {
      const { AreaId } = req.params;
    req.body.areaForClients = AreaId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1,function (err,data) {
                allData.images1 = data.Location;
                callback(null,data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images2,function (err,data) {
                allData.images2 = data.Location;
                callback(null,data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null,data.Location);
            })
        },
        function(callback) {
            fileService.uploadImage(req.files.images4, function (err, data) {
                allData.images4 = data.Location;
                callback(null, data.Location);
            })
        },
 
        function (callback) {
            fileService.uploadImage(req.files.images5, function (err, data) {
                allData.images5 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images6, function (err, data) {
                allData.images6 = data.Location;
                callback(null, data.Location);
            })
        },
         

    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clients.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully",data: allData });
                }
            });
        }
 });
},



index:async (req, res, next) => {

    const { AreaId } = req.params;
    let search=req.query.search;

    const usersposts = await clients.find({areaForClients:AreaId,$or:[
        {subArea:new RegExp(search, "gi")},
        {pincode:new RegExp(search, "gi")}, 
        {instName:new RegExp(search, "gi")},
        {danceCat:new RegExp(search, "gi")},
        {place:new RegExp(search, "gi")},
    ]
}).sort({"subArea":-1})
    res.status(200).json(usersposts);
 },

usersgetcoutarea: async (req, res, next) => {
    const { AreaId } = req.params;

    const users = await clients.aggregate([{$match:{areaForClients:ObjectId(AreaId)}},

        {"$group" : {_id:{subArea:"$subArea"}, count:{$sum:1}}},{$sort:{"subArea":1}}
     ])
    
    res.status(200).json(users);
 },
 
tuserscount:async (req, res, next) => {
     const { AreaId } = req.params;
     const usersposts = await clients.find({areaForClients:AreaId}).count()
    res.status(200).json(usersposts);
 },


totalclients:async (req, res, next) => {
  const usersposts = await clients.find({}).count()
    res.status(200).json(usersposts);
  
},
 
indexs:async (req, res, next) => {

    const { clientsId } = req.params;
    const usersposts = await clients.findById(clientsId) 
    res.status(200).json(usersposts);
  
},
tuserscountAreas: async (req, res, next) => {
      const users = await clients.aggregate([

        {"$group" : {_id:{mainArea:"$mainArea"}, count:{$sum:1}}},{$sort:{"mainArea":1}}
     ])
    
    res.status(200).json(users);
 },

 tuserscountDist: async (req, res, next) => {
     const users = await clients.aggregate([

        {"$group" : {_id:{distict:"$distict"}, count:{$sum:1}}},{$sort:{"distict":1}}
     ])
    
    res.status(200).json(users);
 },
// ***********************************************************************
// ******************* users ******************
 
computerservicecenterpost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.computerServicesUsers = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) {
            console.log(req.files.images1,"req.files.images 273");
            if(req.files.images1){
                fileService.uploadImage(req.files.images1, function (err, data) {
                    allData.images1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.images2,"req.files.images 273");
            if(req.files.images2){
                fileService.uploadImage(req.files.images2, function (err, data) {
                    allData.images2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.images3,"req.files.images 273");
            if(req.files.images3){
                fileService.uploadImage(req.files.images3, function (err, data) {
                    allData.images3 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.userImg,"req.files.images 273");
            if(req.files.userImg){
                fileService.uploadImage(req.files.userImg, function (err, data) {
                    allData.userImg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.video,"req.files.images 273");
            if(req.files.video){
                fileService.uploadImage(req.files.video, function (err, data) {
                    allData.video = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
        
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            users.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
computerservicecenterget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await users.find({computerServicesUsers:clientId}).sort({"userName":1})
    res.status(200).json(usersposts);
 },
 computerservicecentergetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await users.find({computerServicesUsers:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************
// ******************* successRate  ******************
 
computerServiceCentersuccessRatePost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.computerServicesSuccessRate = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) {
            console.log(req.files.images1,"req.files.images 273");
            if(req.files.images1){
                fileService.uploadImage(req.files.images1, function (err, data) {
                    allData.images1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.images2,"req.files.images 273");
            if(req.files.images2){
                fileService.uploadImage(req.files.images2, function (err, data) {
                    allData.images2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.images3,"req.files.images 273");
            if(req.files.images3){
                fileService.uploadImage(req.files.images3, function (err, data) {
                    allData.images3 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.userImg,"req.files.images 273");
            if(req.files.userImg){
                fileService.uploadImage(req.files.userImg, function (err, data) {
                    allData.userImg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.video,"req.files.images 273");
            if(req.files.video){
                fileService.uploadImage(req.files.video, function (err, data) {
                    allData.video = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
        
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            successRate.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
computerservicecentersuccessRateGet:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await successRate.find({computerServicesSuccessRate:clientId}).sort({"userName":1})
    res.status(200).json(usersposts);
 },
 computerservicecenterSuccessRateGetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await successRate.find({computerServicesSuccessRate:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************
// ******************* unSuccessRate  ******************
 
computerServiceCenterunSuccessRatePost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.computerServicesunSuccessRate = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) {
            console.log(req.files.images1,"req.files.images 273");
            if(req.files.images1){
                fileService.uploadImage(req.files.images1, function (err, data) {
                    allData.images1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.images2,"req.files.images 273");
            if(req.files.images2){
                fileService.uploadImage(req.files.images2, function (err, data) {
                    allData.images2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.images3,"req.files.images 273");
            if(req.files.images3){
                fileService.uploadImage(req.files.images3, function (err, data) {
                    allData.images3 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.userImg,"req.files.images 273");
            if(req.files.userImg){
                fileService.uploadImage(req.files.userImg, function (err, data) {
                    allData.userImg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },

          function (callback) {
            console.log(req.files.video,"req.files.images 273");
            if(req.files.video){
                fileService.uploadImage(req.files.video, function (err, data) {
                    allData.video = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
        
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            unsuccessRate.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
computerservicecenterunSuccessRateGet:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await unsuccessRate.find({computerServicesunSuccessRate:clientId}).sort({"userName":1})
    res.status(200).json(usersposts);
 },
 computerservicecenterunSuccessRateGetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await unsuccessRate.find({computerServicesunSuccessRate:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************
// ******************* unSuccessRate  ******************
 
computerServiceCenterunBrandPost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.computerServicesBrand = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) {
            console.log(req.files.brandImg,"req.files.images 273");
            if(req.files.brandImg){
                fileService.uploadImage(req.files.brandImg, function (err, data) {
                    allData.brandImg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
         
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            brand.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
computerservicecenterunBrandGet:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await brand.find({computerServicesBrand:clientId}).sort({"userName":1})
    res.status(200).json(usersposts);
 },
 computerservicecenterunBrandGetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await brand.find({computerServicesBrand:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************

}