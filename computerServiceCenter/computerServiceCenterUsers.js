const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    userName:{type:String},
    userPlace:{type:String},
    damageDescriptions:{type:String},
    images1:{type:String},
    images2:{type:String},
    images3:{type:String},
    userImg:{type:String},
    video:{type:String},

    computerServicesUsers:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'computerServicesBrand'
}
   
},
{timestamps:true});
const areas=mongoose.model('computerServicesUsers',areaSchema);
module.exports=areas;

