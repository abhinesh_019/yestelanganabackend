const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    userName:{type:String},
    userPlace:{type:String},
    atPresentDescriptions:{type:String},
    images1:{type:String},
    images2:{type:String},
    images3:{type:String},
    userImg:{type:String},
    video:{type:String},
    result:{type:String},
    computerServicesSuccessRate:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'computerServicesUsers'
}
   
},
{timestamps:true});
const areas=mongoose.model('computerServicesSuccessRate',areaSchema);
module.exports=areas;

