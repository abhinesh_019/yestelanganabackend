const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:{type:String},

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'computerServicesLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('computerServicesArea',areaSchema);
module.exports=areas;