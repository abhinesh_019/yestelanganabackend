const express= require('express');
var router = express.Router();
const userControllers = require('../computerServiceCenter/computerServiceCenterController');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();


//  ************ Locations ***********

router.route('/computerServieCenterLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/computerServieCenterAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/computerServieCenterClientsget/:AreaId')
.get(userControllers.index)

router.route('/computerServieCenterClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/computerServieCenterClientsAllCount') 
.get(userControllers.totalclients)


router.route('/computerServieCenterClientsCountAreas')  
.get(userControllers.tuserscountAreas)


router.route('/computerServieCenterClientsCountDist')  
.get(userControllers.tuserscountDist)


router.route('/computerServieCenterClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/computerServieCenterClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/computerServieCenterClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* users ******************
router.route('/computerServieCenterusersGet/:clientId')
.get(userControllers.computerservicecenterget)
router.route('/computerServieCenterusersGetCounts/:clientId')
.get(userControllers.computerservicecentergetCounts)
router.route('/computerServieCenterusersPost/:clientId')
.post(multipartMiddleware,userControllers.computerservicecenterpost);

// ******************* successRate  ******************
router.route('/computerServieCentersuccessRateGet/:clientId')
.get(userControllers.computerservicecentersuccessRateGet)
router.route('/computerServieCentersuccessRateGetCounts/:clientId')
.get(userControllers.computerservicecenterSuccessRateGetCounts)
router.route('/computerServieCenterSuccessRatePost/:clientId')
.post(multipartMiddleware,userControllers.computerServiceCentersuccessRatePost);

// ******************* unSuccessRate  ******************
router.route('/computerServieCenterunSuccessRateGet/:clientId')
.get(userControllers.computerservicecenterunSuccessRateGet)
router.route('/computerServieCenterunSuccessRateGetCounts/:clientId')
.get(userControllers.computerservicecenterunSuccessRateGetCounts)
router.route('/computerServieCenterunSuccessRatePost/:clientId')
.post(multipartMiddleware,userControllers.computerServiceCenterunSuccessRatePost);

// ******************* Brand  ******************
router.route('/computerServieCenterunBrandGet/:clientId')
.get(userControllers.computerservicecenterunBrandGet)
router.route('/computerServieCenterunBrandGetCounts/:clientId')
.get(userControllers.computerservicecenterunBrandGetCounts)
router.route('/computerServieCenterBrandPost/:clientId')
.post(multipartMiddleware,userControllers.computerServiceCenterunBrandPost);

module.exports = router;