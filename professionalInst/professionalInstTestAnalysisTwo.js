const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    name:{type:String},
    place:{type:String},
    hallTicketNo:{type:String},
    markSecured:{type:String},
    TotalMarks:{type:String},
    studentImg:{type:String},
    studentQualification:{type:String},
    description:{type:String},
    profestionalInstForTestAnaysisTwo:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBatches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstTestAnalysisTwo',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

