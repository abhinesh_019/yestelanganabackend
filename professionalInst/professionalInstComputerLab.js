const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    computerLabOne:String,
    computerLabImg:String,
    descriptions:String,

    profestionalInstForComputerLab:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBatches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstComputerLab',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

