const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    mainTitle:{type:String},
     descriptions:{type:String},
     descriptions1:{type:String},
     descriptions2:{type:String},
     descriptions3:{type:String},
     descriptions4:{type:String},
     descriptions5:{type:String},

    profestionalForSubjectSpecilizationPrority:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstTrackRecordsYear'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalSubjectSpecilizationPrority',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

