const mongoose=require('mongoose');

const profestionalInstSchemaAreas= new mongoose.Schema({

    mainTtitle:{type:String},
    titleDescriptions:{type:String},
    subTtitle:{type:String},
    subTtitleDescriptions:{type:String},
    subTtitleDescriptionsOne:{type:String},
    subTtitleDescriptionsTwo:{type:String},
    subTtitleDescriptionsThree:{type:String},
    subTtitleDescriptionsFour:{type:String},
    images:{type:String},



    profestionalInstLocationsForWhyInstTwo:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstClient'
}
    
},
{timestamps:true});
const profestionalInstArea=mongoose.model('profestionalInstWhyInstTwo',profestionalInstSchemaAreas);

module.exports=profestionalInstArea;

