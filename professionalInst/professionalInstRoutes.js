const express= require('express');
var router = express.Router();
const userControllers = require('../professionalInst/professionalInstControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ categories ***********
router.route('/professionalInstCategories')
.get(userControllers.Categoriesget)
router.route('/professionalInstCategoriesP')
 .post(multipartMiddleware,userControllers.Categoriesposts);

 router.route('/professionalInstCategoriesIds/:CategoriesId')
 .get(userControllers.CategoriesidgetId)

//  ***********************


//  ************ Locations ***********

router.route('/professionalInstlocations/:catagereiesId')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/professionalInstAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/professionalInstlientsget/:AreaId')
.get(userControllers.index)

router.route('/professionalInstclientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/professionalInstclientsAllCount') 
.get(userControllers.totalclients)

router.route('/professionalInstclientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/professionalInstclientsGetind/:clientsId')
.get(userControllers.clientsGetinds)

 
router.route('/professionalInstclientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);
 
router.route('/professionalInstUsersLocationsGroups') 
.get(userControllers.countLocationsProfessionalInstGroups)
 
router.route('/professionalInstUsersAllCounts') 
.get(userControllers.professionalInstUsersAllCounts)

router.route('/professionalInstUsersLocationsSoftware') 
.get(userControllers.countLocationsProfessionalInstIes)
 

router.route('/professionalInstClientsgetLocationsCat')
.get(userControllers.indexs)
// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/professionalInstfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/professionalInstfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/professionalInstfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************
 
// ****************** only updates details ****************
router.route('/professionalInstupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/professionalInstupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/professionalInstupdatesgetpostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/professionalInstupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/professionalInstupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/professionalInstupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/professionalInstupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/professionalInstupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/professionalInstuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/professionalInstuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

// ****************** about us ****************
router.route('/professionalInstAboutUsget/:ClientsId')
.get(userControllers.UsersAboutUsGet)

router.route('/professionalInstupdatesAboutUsgetCounts/:ClientsId')
.get(userControllers.UsersAboutUsGetCounts)

router.route('/professionalInstupdatesAboutUspostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersAboutUsUpdates);

// ****************** about us Our Philosophy ****************
router.route('/professionalInstPhilosophy/:ClientsId')
.get(userControllers.usersPhilosophyget)
.post(userControllers.usersPhilosophyposts);

 // ****************** about us Our journey ****************
router.route('/professionalInstJourney/:ClientsId')
.get(userControllers.usersJourneyget)
.post(userControllers.usersJourneyposts);
 
// ****************** about us Our Why this Inst Two ****************
router.route('/professionalInstJourneyWhyTwo/:ClientsId')
.get(userControllers.userswhyInstTwoget)
.post(multipartMiddleware,userControllers.userswhyInstTwoposts);

// ****************** Testimonials ****************
router.route('/professionalTestimonials/:ClientsId')
.get(userControllers.userswhyInstTestimonialsget)
.post(multipartMiddleware,userControllers.userswhyInstTestimonialsposts);
// ****************** InfraStructure ****************
router.route('/professionalInfraStructure/:ClientsId')
.get(userControllers.userswhyInstInfraStructureget)
.post(multipartMiddleware,userControllers.userswhyInstInfraStructureposts);

// ****************** BoardDirectors ****************
router.route('/professionalBoardDirectors/:ClientsId')
.get(userControllers.userBoardDirectorsget)
.post(multipartMiddleware,userControllers.usersBoardDirectorsposts);

// ****************** AwardsRewards ****************
router.route('/professionalAwardsRewards/:ClientsId')
.get(userControllers.usersAwardsRewardsget)
.post(multipartMiddleware,userControllers.usersAwardsRewardsposts);

// ****************** services one ****************
router.route('/professionalSerivesOne/:ClientsId')
.get(userControllers.usersSerivesOneget)
router.route('/professionalSerivesOnep/:ClientsId')
.post(multipartMiddleware,userControllers.usersSerivesOneposts);

// ****************** services Two ****************
router.route('/professionalSerivesTwo/:ClientsId')
.get(userControllers.usersSerivesTwoget)
router.route('/professionalSerivesTwop/:ClientsId')
.post(multipartMiddleware,userControllers.usersSerivesTwoposts);

// ****************** track recods year ****************
router.route('/professionalInstyear/:ClientsId')
.get(userControllers.userswhyInstyearget)
.post(userControllers.userswhyInstyearposts);

// ****************** Branches ****************
router.route('/professionalBranches/:ClientsId')
.get(userControllers.usersBranchesget)
router.route('/professionalBranchesp/:ClientsId')
.post(multipartMiddleware,userControllers.usersBranchesPosts);

// ****************** Batches ****************
router.route('/professionalBatches/:ClientsId')
.get(userControllers.usersBatchesget)
router.route('/professionalBatchesP/:ClientsId')
.post(userControllers.usersBatchesPosts);

// ****************** faculty Details  ****************
router.route('/professionalfacultyDetails/:ClientsId')
.get(userControllers.usersfacultyDetailsget)
router.route('/professionalfacultyDetailsp/:ClientsId')
.post(multipartMiddleware,userControllers.usersfacultyDetailsPosts);

// ****************** Eligiblities ****************
router.route('/professionalEligiblities/:ClientsId')
.get(userControllers.usersEligiblitiesget)
router.route('/professionalEligiblitiesP/:ClientsId')
.post(userControllers.usersEligiblitiesPosts);


// ****************** syllabusOne ****************
router.route('/professionalsyllabusOne/:ClientsId')
.get(userControllers.userssyllabusOneGet)
router.route('/professionalSyllabusOneP/:ClientsId')
.post(userControllers.usersSyllabusOnePosts);


// ****************** syllabusTwo ****************
router.route('/professionalsyllabusTwo/:ClientsId')
.get(userControllers.userssyllabusTwoGet)
router.route('/professionalsyllabusTwoP/:ClientsId')
.post(userControllers.userssyllabusTwoPosts);


// ****************** impOrganizationsOne ****************
router.route('/professionalimpOrganizationsOne/:ClientsId')
.get(userControllers.usersimpOrganizationsOneGet)
router.route('/professionalimpOrganizationsOneP/:ClientsId')
.post(userControllers.usersimpOrganizationsOnePosts);


// ****************** impOrganizationsTwo ****************
router.route('/professionalimpOrganizationsTwo/:ClientsId')
.get(userControllers.usersimpOrganizationsTwoGet)
router.route('/professionalimpOrganizationsTwoP/:ClientsId')
.post(userControllers.userimpOrganizationsTwoPosts);

// ****************** preparationPatterns ****************
router.route('/professionalpreparationPatternsTwo/:ClientsId')
.get(userControllers.userspreparationPatternsGet)
router.route('/professionalpreparationPatternsP/:ClientsId')
.post(userControllers.userPreparationPatternsPosts);


// ****************** ExamPattern  ****************
router.route('/professionalExamPattern/:ClientsId')
.get(userControllers.usersExamPatternGet)
router.route('/professionalExamPatternP/:ClientsId')
.post(multipartMiddleware,userControllers.usersExamPatternPosts);

// ****************** ImportantNotifications ****************
router.route('/professionalImportantNotifications/:ClientsId')
.get(userControllers.userImportantNotificationssGet)
router.route('/professionalImportantNotificationsCount/:ClientsId')
.get(userControllers.userImportantNotificationssGetCount)
router.route('/professionalImportantNotificationsP/:ClientsId')
.post(userControllers.userImportantNotificationsPosts);
 

// ****************** Materials ****************
router.route('/professionalMaterials/:ClientsId')
.get(userControllers.userMaterialsGet)
router.route('/professionalMaterialsP/:ClientsId')
.post(multipartMiddleware,userControllers.userMaterialsPosts);

 
 
// ****************** NewBatchOne ****************
router.route('/professionalNewBatchOne/:ClientsId')
.get(userControllers.userNewBatchOneGet)
router.route('/professionalNewBatchOneP/:ClientsId')
.post(multipartMiddleware,userControllers.userNewBatchOnePosts);

 

// ****************** AdminProcess ****************
router.route('/professionalAdminProcess/:ClientsId')
.get(userControllers.userAdminProcessGet)
router.route('/professionalAdminProcessP/:ClientsId')
.post(userControllers.userAdminProcessPosts);

// ****************** SubjectPrority ****************
router.route('/professionalSubjectPrority/:ClientsId')
.get(userControllers.userSubjectProrityGet)
router.route('/professionalSubjectProrityP/:ClientsId')
.post(userControllers.userSubjectProrityPosts);

 

// ****************** FeeStructureTwo ****************
router.route('/professionalFeeStructureTwo/:ClientsId')
.get(userControllers.userFeeStructureTwoGet)
router.route('/professionalFeeStructureTwoP/:ClientsId')
.post(userControllers.userFeeStructureTwoPosts);

// ****************** AgeAttempts ****************
router.route('/professionalAgeAttempts/:ClientsId')
.get(userControllers.userAgeAttemptsGet)
router.route('/professionalAgeAttemptsP/:ClientsId')
.post(userControllers.userAgeAttemptsPosts);

// ********************************************************* batches ******************************************************

router.route('/professionalBatchGet/:ClientsId')
.get(userControllers.professionalBatchGet)
router.route('/professionalBatchP/:ClientsId')
.post(userControllers.professionalBatchPosts);


// ****************** testAnalysis ****************
router.route('/professionaltestAnalysis/:ClientsId')
.get(userControllers.usertestAnalysisGet)
router.route('/professionaltestAnalysisP/:ClientsId')
.post(multipartMiddleware,userControllers.usertestAnalysisPosts);

// ****************** trackRecord ****************
router.route('/professionaltrackRecord/:ClientsId')
.get(userControllers.professionaltrackRecordGet)
router.route('/professionaltrackRecordP/:ClientsId')
.post(multipartMiddleware,userControllers.professionaltrackRecordPost);

// ****************** computerLab ****************
router.route('/professionalComputerLab/:ClientsId')
.get(userControllers.userComputerLabGet)
router.route('/professionalComputerLabP/:ClientsId')
.post(multipartMiddleware,userControllers.userComputerLabPosts);

// ****************** liraryOne ****************
router.route('/professionalliraryOneGet/:ClientsId')
.get(userControllers.professionalliraryOneBatchGet)
router.route('/professionalliraryOneP/:ClientsId')
.post(userControllers.professionalliraryOnePosts);

// ****************** liraryTwo ****************
router.route('/professionalliraryTwoGet/:ClientsId')
.get(userControllers.professionalliraryTwoGet)
router.route('/professionalliraryTwoPost/:ClientsId')
.post(userControllers.professionalliraryTwoPost);

// ****************** liraryThree ****************
router.route('/professionalliraryThree/:ClientsId')
.get(userControllers.userliraryThreeGet)
router.route('/professionalliraryThreeP/:ClientsId')
.post(multipartMiddleware,userControllers.userliraryThreePosts);


// *************************************** AddsOne ************************************* 
router.route('/professionalAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/professionalAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/professionalAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/professionalAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/professionalAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/professionalAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/professionalAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/professionalAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/professionalAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/professionalAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/professionalAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/professionalAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/professionalAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/professionalAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/professionalAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/professionalAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/professionalAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/professionalAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/professionalAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/professionalAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/professionalAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/professionalAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/professionalAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/professionalAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/professionalAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/professionalAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/professionalAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
