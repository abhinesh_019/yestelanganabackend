const mongoose=require('mongoose');

const profestionalInstSchemaAreas= new mongoose.Schema({

    mainTitle:{type:String},
    Year:{type:String},
     descriptionsOne:{type:String},
     descriptionsTwo:{type:String},
     descriptionsThree:{type:String},
     descriptionsFour:{type:String},
     descriptionsFive:{type:String},
     descriptionsSix:{type:String},
 
     profestionalInstClientsForJourneyMilestone:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstClient'
}
    
},
{timestamps:true});
const profestionalInstArea=mongoose.model('profestionalInstJourneyMilestone',profestionalInstSchemaAreas);

module.exports=profestionalInstArea;

