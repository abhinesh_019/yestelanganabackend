const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({
     branchName:String,
     branchImage:String,
     houseNo:String,
     streetNo:String,
     area:String,
     city:String,
     state:String,
     picode:String,
     contactNo:String,
     profestionalInstCatTwoForCreatingBranches:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstTrackRecordsYear'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstBranches',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

 