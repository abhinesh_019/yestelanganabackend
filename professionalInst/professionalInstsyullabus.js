const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    mainTitle:{type:String},
     simpleText:{type:String},
    subTitle:{type:String},
    
 
    profestionalInstForsubjectSyllabusOne:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBranches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstSubJectSyllabus',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

