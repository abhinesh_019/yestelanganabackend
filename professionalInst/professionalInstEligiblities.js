const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

     mainTitle:{type:String},
     subTitle:{type:String},
     mainTitleDescriptionsOne:{type:String},
     mainTitleDescriptionsTwo:{type:String},
     descriptionsTitle1:{type:String},
     descriptionsTitle2:{type:String},
     descriptionsTitle3:{type:String},
     descriptionsImportantPoints1:{type:String},
     descriptionsImportantPoints2:{type:String},
     descriptionsImportantPoints3:{type:String},
    descriptions1:{type:String},
    descriptions2:{type:String},
    descriptions3:{type:String},
    note1:{type:String},
    note2:{type:String},
    profestionalInstCatTwoForEligibilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBranches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstEligibilities',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

