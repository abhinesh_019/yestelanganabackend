const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({
    mainTitle:{type:String},
    batchName:{type:String},
    subtitle1:{type:String},
    subtitle2:{type:String},
    subtitle3:{type:String},
    subtitle4:{type:String},
    subtitle5:{type:String},
    subtitle6:{type:String},
    subtitle7:{type:String},
    subtitle8:{type:String},
    subtitleText1:{type:String},
    subtitleText2:{type:String},
    subtitleText3:{type:String},
    subtitleText4:{type:String},
    subtitleText5:{type:String},
    subtitleText6:{type:String},
    subtitleText7:{type:String},
    subtitleText8:{type:String},
    note:{type:String},

    profestionalForFreeStructureTwo:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBranches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalFreeStructureTwo',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

