const mongoose=require('mongoose');

const profestionalInstSchemaTestimonials= new mongoose.Schema({
    catageries:{type:String},
    name:{type:String},
    place:{type:String},
    images:{type:String},
    year:{type:String},
    rank:{type:String},
    descriptions:{type:String},
qualification:{type:String},
selectedAs:{type:String},
    profestionalInstAboutUsForTestimonials:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstClient'
}
    
},
{timestamps:true});
const profestionalInstTestimonials=mongoose.model('profestionalInstTestimonials',profestionalInstSchemaTestimonials);

module.exports=profestionalInstTestimonials;

