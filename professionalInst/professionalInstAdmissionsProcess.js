const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({
    mainTitle:{type:String},
     admissionProcessType:{type:String},
     necessaryRequireTitle:{type:String},
    necessaryRequire1:{type:String},
    necessaryRequire2:{type:String},
    necessaryRequireDescription1:{type:String},
    necessaryRequireDescription2:{type:String},
    mainDescriptions:{type:String},
impNote1:{type:String},
impNote2:{type:String},
    profestionalForAdmissionProcess:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBranches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstAdmissionProcess',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

