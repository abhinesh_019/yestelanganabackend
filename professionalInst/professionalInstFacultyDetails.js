const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    facultyName:{type:String},
    facultyQualifications:{type:String},
    facultyDescriptions:{type:String},
    facultyImg:{type:String},
     
    profestionalInstForFacultyDetails:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBatches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstFaculty',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

