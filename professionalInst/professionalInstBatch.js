const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({
     batchName:String,
     branchShift:String,
     branchTimmingsStart:String,
     branchTimmingsEnding:String,
     batchDurations:String,
     totalBatchDurations:String,
     batchDescription1:String,
     batchDescription2:String,
     batchDescription3:String,
     batchDescription4:String,
     
    profestionalInstForCreatingBatches:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBranches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstBatches',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

 