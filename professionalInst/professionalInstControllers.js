var hallsC = require('../professionalInst/professionalInstCatageries')
var hallsL = require('../professionalInst/professionalInstLocations')
var hallsA = require('../professionalInst/professionalInstAreas')
var hallsClients = require('../professionalInst/professionalInstClients')
var hallsClientsUpdates = require('../professionalInst/professionalInstUpdates')
var hallsClientsUpdatesComments = require('../professionalInst/professionalInstUpdatesComments')
 var hallsClientsUpdatesCommentsReply = require('../professionalInst/professionalInstUpdatesCommentsReply')
var hallsClientsOnlyComments = require('../professionalInst/professionalInstOnlyComments')
var hallsFecilities = require('../professionalInst/professionalInstFecilities')
var philosophy = require('../professionalInst/professionalInstOurPhilosophy')
var journey = require('../professionalInst/professionalInstJourneyMilestone')
 var whyInstTwo = require('../professionalInst/professionalInstWhyInstTwo')
var testimonials= require('../professionalInst/professionalInstTestimonials')
var directors= require('../professionalInst/professionalInstBoardOfDirectors')
var rewards= require('../professionalInst/professionalInstAwardsAndRewards')
var servicesOne= require('../professionalInst/professionalInstCatageriesOne')
var servicesTwo= require('../professionalInst/professionalInstCatageriesTwo')
var trackYear= require('../professionalInst/professionalInstTrackRecordYear')
var branches= require('../professionalInst/professionalInstBranches')
var infra= require('../professionalInst/professionalInstInfraStructure')
var batches= require('../professionalInst/professionalInstBatch')
var faculty= require('../professionalInst/professionalInstFacultyDetails')

var babycaresAddsOne=require('../professionalInst/fightsAddsOnea');
var babycaresAddsTwo=require('../professionalInst/fightsAddsTwoa');
var babycaresAddsThree=require('../professionalInst/fightsAddsThreea');
var babycaresAddsFour=require('../professionalInst/fightsAddsFoura');
 
var babycaresAddsOneLoc=require('../professionalInst/fightsAddsOnel');
var babycaresAddsTwoLoc=require('../professionalInst/fightsAddsTwol');
var babycaresAddsThreeLoc=require('../professionalInst/fightsAddsThreel');
var babycaresAddsFourLoc=require('../professionalInst/fightsAddsFourl');
var babycaresAddsFiveLoc=require('../professionalInst/fightsAddsFivel');
var eligible=require('../professionalInst/professionalInstEligiblities')
var syllabusOne=require('../professionalInst/professionalInstsyullabus')
var syllabusTwo=require('../professionalInst/professionalInstSyllabusTwo')
var impOrgOne=require('../professionalInst/professionalInstImptOrganizations')
var impOrgTwo=require('../professionalInst/professionalInstImptOrganizationsTwo')
var ppatren=require('../professionalInst/professionalInstPreparationPatternOne')
var examP=require('../professionalInst/professionalInstexmOrIntPattren')
var impNoti=require('../professionalInst/professionalInstImptNotifications')
var matOne=require('../professionalInst/professionalInstMaterials')
 var newBatchOne=require('../professionalInst/professionalInstNewBatchesOne')
 var admiss=require('../professionalInst/professionalInstAdmissionsProcess')
 var subjectPrority=require('../professionalInst/professionalInstSubjectSpecilizationPrority')
 var freeTwo=require('../professionalInst/professionalInstFreeStructureTwo')
var ages=require('../professionalInst/professionalInstAgeAndAttempts')
var batch=require('../professionalInst/professionalInstBatch')
var testAn=require('../professionalInst/professionalInstTestAnalysisTwo')
var faculty=require('../professionalInst/professionalInstFacultyDetails')
var trackRecord=require('../professionalInst/professionalInsTrackRecords')
var computerLab=require('../professionalInst/professionalInstComputerLab')
var librararyOne=require('../professionalInst/professionalInstLibraryTypeOne')
var librarary=require('../professionalInst/professionalInstLibrary')
var libThree=require('../professionalInst/professionalInstLibraryThree')
const fileService = require('../professionalInst/professionalInstFileServer');
  var about=require('../professionalInst/professionalInstAboutUs');
var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 
var async = require('async');
  
module.exports = {
// ********************categories*****************
Categoriesposts:(req, res, next) => {

    let allData = req.body;
 
    async.parallel([

        function (callback) {

            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
          
            hallsC.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                    res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
                   });
        }
    });
},

 

Categoriesget: async(req,res,next)=>{
    const users = await hallsC.find({}).sort({"locations":1});
       res.status(200).json(users);

   console.log(users,"users");
},

CategoriesidgetId:async (req, res, next) => {
       const { CategoriesId } = req.params;
       const usersposts = await hallsC.findById(CategoriesId)
       res.status(200).json(usersposts);
        },
 // ******************************************

// ********************* Locations *********************

locationspost: async(req,res,next)=>{
    const { catagereiesId } = req.params;
    req.body.profestionalInstCatageriousForLocations = catagereiesId
     const userscomm = new hallsL(req.body);
    await userscomm.save();
    res.status(201).json(userscomm);
},


locationsget: async(req,res,next)=>{
    
    const { catagereiesId } = req.params;
    const users = await hallsL.find({profestionalInstCatageriousForLocations:catagereiesId}).sort({"locations":1});
       res.status(200).json(users);
 },
// ******************************************

// *********************** Areas *******************

Areasposts : async(req,res,next)=>{
    const { LocationsId } = req.params;
    req.body.profestionalInstLocationsForAreas = LocationsId
     const userscomm = new hallsA(req.body);
    await userscomm.save();
        res.status(201).json(userscomm);
},
 
Areasget:async (req, res, next) => {
     const { LocationsId } = req.params;
     const usersposts = await hallsA.find({profestionalInstLocationsForAreas:LocationsId}).sort({"area":-1})
    res.status(200).json(usersposts);
 },

// ******************************************
// ************************************************ Clients Updates ***************************************
 
newUser: (req, res, next) => {
    const { AreaId } = req.params;
    req.body.areaForClients = AreaId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },

        function(callback) {
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null,data.Location);
            })
        },
        function(callback) {
            fileService.uploadImage(req.files.images4, function (err, data) {
                allData.images4 = data.Location;
                callback(null, data.Location);
            })
        },
 
        function (callback) {
            fileService.uploadImage(req.files.images5, function (err, data) {
                allData.images5 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images6, function (err, data) {
                allData.images6 = data.Location;
                callback(null, data.Location);
            })
        },
         

    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            hallsClients.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},



index:async (req, res, next) => {

    const { AreaId } = req.params;
    let search=req.query.search;

    const usersposts = await hallsClients.find({areaForClients:AreaId,$or:[
        {mainArea:new RegExp(search, "gi")},
        {subArea:new RegExp(search, "gi")},
        {pincode:new RegExp(search, "gi")}, 
        {instName:new RegExp(search, "gi")},
        {place:new RegExp(search, "gi")},
    ]
}).sort({"subArea":-1})
    res.status(200).json(usersposts);
 },

usersgetcoutarea: async (req, res, next) => {
    const { AreaId } = req.params;

    const users = await hallsClients.aggregate([{$match:{areaForClients:ObjectId(AreaId)}},

        {"$group" : {_id:{subArea:"$subArea"}, count:{$sum:1}}},{$sort:{"subArea":1}}
     ])
    
    res.status(200).json(users);
 },
  
 
tuserscount:async (req, res, next) => {
     const { AreaId } = req.params;
     const usersposts = await hallsClients.find({areaForClients:AreaId}).count()
    res.status(200).json(usersposts);
 },


totalclients:async (req, res, next) => {
  const usersposts = await hallsClients.find({}).count()
    res.status(200).json(usersposts);
  
},
 
clientsGetinds:async (req, res, next) => {

    const { clientsId } = req.params;
 
    const usersposts = await hallsClients.findById(clientsId)
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},
countLocationsProfessionalInstGroups: async (req, res, next) => {
    const users = await hallsClients.aggregate([ 
        
          {"$group" : {_id:{distict:"$distict",instCat:"$instCat"},count:{$sum:1}}}
    ]).sort({"instCat":1})
     res.status(200).json(users);
},
professionalInstUsersAllCounts: async (req, res, next) => {
    const users = await hallsClients.aggregate([ 
        
          {"$group" : {_id:{instCat:"$instCat"},count:{$sum:1}}}
    ]).sort({"instCat":1})
     res.status(200).json(users);
},
countLocationsProfessionalInstIes: async (req, res, next) => {
    const users = await hallsClients.aggregate([ 
        { $match : { instCat : "software institues" } },
          {"$group" : {_id:{distict:"$distict"},count:{$sum:1}}}
    ]).sort({"distict":1})
     res.status(200).json(users);
},
 

indexs: async (req, res, next) => {

   const users = await hallsClients.aggregate(
       [ {"$group" : {_id:{catageries:"$instCat"},count:{$sum:1}}}

        ]
   );
   
   res.status(200).json(users);
  console.log("users****", users);

},
 
// ***********************************************************************


// ************************************************ Clients fecilities ***************************************
 
fecilitiespost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.functionsHallsClientsForFecilities = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            hallsFecilities.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},



fecilitiesget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await hallsFecilities.find({functionsHallsClientsForFecilities:clientId}).sort({"createdAt":-1})
    res.status(200).json(usersposts);
 },
 fecilitiesgetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await hallsFecilities.find({functionsHallsClientsForFecilities:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************


 

// ****************users updates*************

UsersUpdates: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.UpdatedDetails = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.viedoes,"req.files.viedoes 302");
            if(req.files.viedoes){
                fileService.uploadImage(req.files.viedoes, function (err, data) {
                    allData.viedoes = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            hallsClientsUpdates.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

UsersGet:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await hallsClientsUpdates.find({UpdatedDetails:ClientsId}).sort({"createdAt":-1})

        res.status(200).json(users);
       console.log("users****", users);
},

UsersGetCounts:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await hallsClientsUpdates.find({UpdatedDetails:ClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
//admin users updates comments
updatesCommentsposts:async (req, res, next) => {

const { updatesId } = req.params;
  req.body.updatesForComments = updatesId 
const userscomm = new hallsClientsUpdatesComments(req.body);

await userscomm.save();
console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
 res.status(201).json(userscomm);
},

updatesCommentsGet:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await hallsClientsUpdatesComments.find({updatesForComments:updatesId})
    res.status(200).json(users); 
},
updatesCommentsGetcounts:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await hallsClientsUpdatesComments.find({updatesForComments:updatesId}).count()
    res.status(200).json(users);
},

updatesCommentsReplyposts:async (req, res, next) => {
const { updatesCommId } = req.params;
  req.body.updatesCommentsForReply = updatesCommId 
const userscomm = new hallsClientsUpdatesCommentsReply(req.body);
await userscomm.save();
console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
res.status(201).json(userscomm);
},

updatesCommentsReplyGet:async (req, res, next) => {

const { updatesCommId } = req.params;

    const users = await hallsClientsUpdatesCommentsReply.find({updatesCommentsForReply:updatesCommId})

    res.status(200).json(users);
   console.log("users**** 399", users);
},

// ****************only comments *******************
usersCommentsposts : async (req,res) => {
const { ClientsId } = req.params;
req.body.clientsForOnlyComments = ClientsId
const generalcomm = new hallsClientsOnlyComments(req.body);
await generalcomm.save();
res.status(201).json(generalcomm);
},
usersCommentsget:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await hallsClientsOnlyComments.find({clientsForOnlyComments:ClientsId})
res.status(200).json(users);   
},
usersCommentsgetCounts:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await hallsClientsOnlyComments.find({clientsForOnlyComments:ClientsId}).count()
res.status(200).json(users); 
},


// ****************** about us ****************

UsersAboutUsUpdates: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.profestionalInstLocationsForAboutUs = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.managmentTeamImg1,"req.files.images 273");
            if(req.files.managmentTeamImg1){
                fileService.uploadImage(req.files.managmentTeamImg1, function (err, data) {
                    allData.managmentTeamImg1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.managmentTeamImg2,"req.files.viedoes 302");
            if(req.files.managmentTeamImg2){
                fileService.uploadImage(req.files.managmentTeamImg2, function (err, data) {
                    allData.managmentTeamImg2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.managmentTeamImg3,"req.files.viedoes 302");
            if(req.files.managmentTeamImg3){
                fileService.uploadImage(req.files.managmentTeamImg3, function (err, data) {
                    allData.managmentTeamImg3 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.managmentTeamImg4,"req.files.viedoes 302");
            if(req.files.managmentTeamImg4){
                fileService.uploadImage(req.files.managmentTeamImg4, function (err, data) {
                    allData.managmentTeamImg4 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
          function (callback) {
            console.log(req.files.visonMissionImg,"req.files.viedoes 302");
            if(req.files.visonMissionImg){
                fileService.uploadImage(req.files.visonMissionImg, function (err, data) {
                    allData.visonMissionImg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            about.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

UsersAboutUsGet:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await about.find({profestionalInstLocationsForAboutUs:ClientsId}).sort({"createdAt":-1})

        res.status(200).json(users);
       console.log("users****", users);
},

UsersAboutUsGetCounts:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await about.find({profestionalInstLocationsForAboutUs:ClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
// ****************** about us Our Philosophy ****************
    usersPhilosophyposts : async (req,res) => {
    const { ClientsId } = req.params;
    req.body.profestionalInsClientsForOurPhilosophy = ClientsId
    const generalcomm = new philosophy(req.body);
    await generalcomm.save();
    res.status(201).json(generalcomm);
    },
    usersPhilosophyget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await philosophy.find({profestionalInsClientsForOurPhilosophy:ClientsId})
    res.status(200).json(users);   
    },
 // ****************** about us Our journey ****************
 usersJourneyposts : async (req,res) => {
    const { ClientsId } = req.params;
    req.body.profestionalInstClientsForJourneyMilestone = ClientsId
    const generalcomm = new journey(req.body);
    await generalcomm.save();
    res.status(201).json(generalcomm);
    },
    usersJourneyget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await journey.find({profestionalInstClientsForJourneyMilestone:ClientsId})
    res.status(200).json(users);   
    },
 
// ****************** about us Our Why this Inst Two ****************
userswhyInstTwoposts : async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.profestionalInstLocationsForWhyInstTwo = ClientsId
    let allData = req.body 

    async.parallel([
       function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
         },
       ], function (err, result) {
             if (err) {
            consoler.err(err);
            return;
        } else {
            whyInstTwo.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");
                  if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

    userswhyInstTwoget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await whyInstTwo.find({profestionalInstLocationsForWhyInstTwo:ClientsId})
    res.status(200).json(users);   
    },
    // ****************** Testimonials ****************
    
    userswhyInstTestimonialsposts: async (req, res, next) => {
 
        const { ClientsId } = req.params;
        req.body.profestionalInstAboutUsForTestimonials = ClientsId
        let allData = req.body 
    
        async.parallel([
    
    
            function (callback) {
                console.log(req.files.images,"req.files.images 273");
                if(req.files.images){
                    fileService.uploadImage(req.files.images, function (err, data) {
                        allData.images = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
             },
           ], function (err, result) {
                 if (err) {
                consoler.err(err);
                return;
            } else {
                testimonials.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
                      if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }  });  }  });
    },
    
    userswhyInstTestimonialsget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await testimonials.find({profestionalInstAboutUsForTestimonials:ClientsId})
    res.status(200).json(users);   
    },

     // ****************** InfraStructure ****************
    
     userswhyInstInfraStructureposts: async (req, res, next) => {
 
        const { ClientsId } = req.params;
        req.body.profestionalInstAboutUsForInfraStructure = ClientsId
        let allData = req.body 
    
        async.parallel([
    
    
            function (callback) {
                console.log(req.files.images,"req.files.images 273");
                if(req.files.images){
                    fileService.uploadImage(req.files.images, function (err, data) {
                        allData.images = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
             },
           ], function (err, result) {
                 if (err) {
                consoler.err(err);
                return;
            } else {
                infra.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
                      if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }  });  }  });
    },
    
    userswhyInstInfraStructureget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await infra.find({profestionalInstAboutUsForInfraStructure:ClientsId})
    res.status(200).json(users);   
    },

     // ****************** BoardDirectors ****************
    
     usersBoardDirectorsposts: async (req, res, next) => {
 
        const { ClientsId } = req.params;
        req.body.profestionalInstLocationsForBoardOfDirectors = ClientsId
        let allData = req.body 
    
        async.parallel([
    
    
            function (callback) {
                console.log(req.files.directorImg,"req.files.images 273");
                if(req.files.directorImg){
                    fileService.uploadImage(req.files.directorImg, function (err, data) {
                        allData.directorImg = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
             },
           ], function (err, result) {
                 if (err) {
                consoler.err(err);
                return;
            } else {
                directors.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
                      if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }  });  }  });
    },
    
    userBoardDirectorsget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await directors.find({profestionalInstLocationsForBoardOfDirectors:ClientsId})
    res.status(200).json(users);   
    },

     // ****************** InfraStructure ****************
    
     usersAwardsRewardsposts: async (req, res, next) => {
 
        const { ClientsId } = req.params;
        req.body.profestionalInstLocationsForAwardsnRewards = ClientsId
        let allData = req.body 
    
        async.parallel([
    
    
            function (callback) {
                console.log(req.files.images,"req.files.images 273");
                if(req.files.images){
                    fileService.uploadImage(req.files.images, function (err, data) {
                        allData.images = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
             },
           ], function (err, result) {
                 if (err) {
                consoler.err(err);
                return;
            } else {
                rewards.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
                      if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }  });  }  });
    },
    
    usersAwardsRewardsget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await rewards.find({profestionalInstLocationsForAwardsnRewards:ClientsId})
    res.status(200).json(users);   
    },

      // ****************** services one  ****************
    
      usersSerivesOneposts: async (req, res, next) => {
 
        const { ClientsId } = req.params;
        req.body.profestionalInstClientsForCatageriesOne = ClientsId
        let allData = req.body 
    
        async.parallel([
             function (callback) {
                console.log(req.files.images,"req.files.images 273");
                if(req.files.images){
                    fileService.uploadImage(req.files.images, function (err, data) {
                        allData.images = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
             },
           ], function (err, result) {
                 if (err) {
                consoler.err(err);
                return;
            } else {
                servicesOne.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
                      if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }  });  }  });
    },
    
    usersSerivesOneget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await servicesOne.find({profestionalInstClientsForCatageriesOne:ClientsId})
    res.status(200).json(users);   
    },

     // ****************** services Two  ****************
    
     usersSerivesTwoposts: async (req, res, next) => {
 
        const { ClientsId } = req.params;
        req.body.profestionalInstClientsForCatageriesTwo = ClientsId
        let allData = req.body 
    
        async.parallel([
             function (callback) {
                console.log(req.files.images,"req.files.images 273");
                if(req.files.images){
                    fileService.uploadImage(req.files.images, function (err, data) {
                        allData.images = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
             },
           ], function (err, result) {
                 if (err) {
                consoler.err(err);
                return;
            } else {
                servicesTwo.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
                      if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }  });  }  });
    },
    
    usersSerivesTwoget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await servicesTwo.find({profestionalInstClientsForCatageriesTwo:ClientsId}).sort({"subCourseName":-1})
    res.status(200).json(users);   
    },
// ****************** track recods year ****************
userswhyInstyearposts : async (req,res) => {
    const { ClientsId } = req.params;
    req.body.profestionalInstCatTwoForTrackRecordYears = ClientsId
    const generalcomm = new trackYear(req.body);
    await generalcomm.save();
    res.status(201).json(generalcomm);
    },
    userswhyInstyearget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await trackYear.find({profestionalInstCatTwoForTrackRecordYears:ClientsId})
    res.status(200).json(users);   
    },

// ****************** Branches ****************
    
usersBranchesPosts: async (req, res, next) => {
 
        const { ClientsId } = req.params;
        req.body.profestionalInstCatTwoForCreatingBranches = ClientsId
        let allData = req.body 
    
        async.parallel([
             function (callback) {
                console.log(req.files.branchImage,"req.files.images 273");
                if(req.files.branchImage){
                    fileService.uploadImage(req.files.branchImage, function (err, data) {
                        allData.branchImage = data.Location;
                        callback(null, data.Location);
                    })
                  }
                else{
                    callback(null, null);   
                }
             },
           ], function (err, result) {
                 if (err) {
                consoler.err(err);
                return;
            } else {
                branches.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
                      if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }  });  }  });
    },
    
    usersBranchesget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await branches.find({profestionalInstCatTwoForCreatingBranches:ClientsId}).sort({"branchName":-1})
    res.status(200).json(users);   
    },

// ****************** Batches ****************
usersBatchesPosts : async (req,res) => {
    const { ClientsId } = req.params;
    req.body.profestionalInstCatTwoForCreatingBatches = ClientsId
    const generalcomm = new batches(req.body);
    await generalcomm.save();
    res.status(201).json(generalcomm);
    },
    usersBatchesget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await batches.find({profestionalInstCatTwoForCreatingBatches:ClientsId})
    res.status(200).json(users);   
    },
// ****************** faculty Details  ****************
    
usersfacultyDetailsPosts: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.profestionalInstForFacultyDetails = ClientsId
    let allData = req.body 

    async.parallel([
         function (callback) {
            console.log(req.files.facultyImg,"req.files.images 273");
            if(req.files.facultyImg){
                fileService.uploadImage(req.files.facultyImg, function (err, data) {
                    allData.facultyImg = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
         },
       ], function (err, result) {
             if (err) {
            consoler.err(err);
            return;
        } else {
            faculty.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");
                  if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

usersfacultyDetailsget:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await faculty.find({profestionalInstForFacultyDetails:ClientsId})
res.status(200).json(users);   
},
// ****************** Eligiblities ****************
usersEligiblitiesPosts : async (req,res) => {
    const { ClientsId } = req.params;
    req.body.profestionalInstCatTwoForEligibilities = ClientsId
    const generalcomm = new eligible(req.body);
    await generalcomm.save();
    res.status(201).json(generalcomm);
    },
    usersEligiblitiesget:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await eligible.find({profestionalInstCatTwoForEligibilities:ClientsId})
    res.status(200).json(users);   
    },

    // ****************** syllabusOne ****************
    usersSyllabusOnePosts : async (req,res) => {
    const { ClientsId } = req.params;
    req.body.profestionalInstForsubjectSyllabusOne = ClientsId
    const generalcomm = new syllabusOne(req.body);
    await generalcomm.save();
    res.status(201).json(generalcomm);
    },
    userssyllabusOneGet:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await syllabusOne.find({profestionalInstForsubjectSyllabusOne:ClientsId})
    res.status(200).json(users);   
    },

    // ****************** syllabusTwo ****************
    userssyllabusTwoPosts : async (req,res) => {
    const { ClientsId } = req.params;
    req.body.profestionalInsForsubjectSyllabusTwo = ClientsId
    const generalcomm = new syllabusTwo(req.body);
    await generalcomm.save();
    res.status(201).json(generalcomm);
    },
    userssyllabusTwoGet:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await syllabusTwo.find({profestionalInsForsubjectSyllabusTwo:ClientsId})
    res.status(200).json(users);   
    },
    // ****************** impOrganizationsOne ****************
    usersimpOrganizationsOnePosts : async (req,res) => {
        const { ClientsId } = req.params;
        req.body.profestionalInstForImportantOrganizations = ClientsId
        const generalcomm = new impOrgOne(req.body);
        await generalcomm.save();
        res.status(201).json(generalcomm);
        },
        usersimpOrganizationsOneGet:async (req, res, next) => {
        const { ClientsId } = req.params;
        const users = await impOrgOne.find({profestionalInstForImportantOrganizations:ClientsId})
        res.status(200).json(users);   
        },
        // ****************** impOrganizationsTwo ****************
        userimpOrganizationsTwoPosts : async (req,res) => {
        const { ClientsId } = req.params;
        req.body.profestionalInstForImportantOrganizationsTwo = ClientsId
        const generalcomm = new impOrgTwo(req.body);
        await generalcomm.save();
        res.status(201).json(generalcomm);
        },
        usersimpOrganizationsTwoGet:async (req, res, next) => {
        const { ClientsId } = req.params;
        const users = await impOrgTwo.find({profestionalInstForImportantOrganizationsTwo:ClientsId})
        res.status(200).json(users);   
        },

         // ****************** preparationPatterns ****************
         userPreparationPatternsPosts : async (req,res) => {
            const { ClientsId } = req.params;
            req.body.profestionalInstForPreparationPattern = ClientsId
            const generalcomm = new ppatren(req.body);
            await generalcomm.save();
            res.status(201).json(generalcomm);
            },
            userspreparationPatternsGet:async (req, res, next) => {
            const { ClientsId } = req.params;
            const users = await ppatren.find({profestionalInstForPreparationPattern:ClientsId})
            res.status(200).json(users);   
            },
            // ****************** ExamPattern  ****************
    
            usersExamPatternPosts: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.profestionalInstForExamPattrens = ClientsId
    let allData = req.body 

    async.parallel([
         function (callback) {
            console.log(req.files.images1,"req.files.images 273");
            if(req.files.images1){
                fileService.uploadImage(req.files.images1, function (err, data) {
                    allData.images1 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
         },

         function (callback) {
            console.log(req.files.images2,"req.files.images 273");
            if(req.files.images2){
                fileService.uploadImage(req.files.images2, function (err, data) {
                    allData.images2 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
         },

         function (callback) {
            console.log(req.files.images3,"req.files.images 273");
            if(req.files.images3){
                fileService.uploadImage(req.files.images3, function (err, data) {
                    allData.images3 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
         },

         function (callback) {
            console.log(req.files.images4,"req.files.images 273");
            if(req.files.images4){
                fileService.uploadImage(req.files.images4, function (err, data) {
                    allData.images4 = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
         },

          
       ], function (err, result) {
             if (err) {
            consoler.err(err);
            return;
        } else {
            examP.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");
                  if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

usersExamPatternGet:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await examP.find({profestionalInstForExamPattrens:ClientsId})
res.status(200).json(users);   
},

 // ****************** ImportantNotifications ****************
 userImportantNotificationsPosts : async (req,res) => {
    const { ClientsId } = req.params;
    req.body.profestionalForImportantNotifications = ClientsId
    const generalcomm = new impNoti(req.body);
    await generalcomm.save();
    res.status(201).json(generalcomm);
    },
    userImportantNotificationssGet:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await impNoti.find({profestionalForImportantNotifications:ClientsId})
    res.status(200).json(users);   
    },
    userImportantNotificationssGetCount:async (req, res, next) => {
        const { ClientsId } = req.params;
        const users = await impNoti.find({profestionalForImportantNotifications:ClientsId}).count()
        res.status(200).json(users);   
        },

        
            // ****************** Materials ****************
             

            userMaterialsPosts: async (req, res, next) => {
 
                const { ClientsId } = req.params;
                req.body.profestionalForMaterials = ClientsId
                let allData = req.body 
            
                async.parallel([
                     function (callback) {
                        console.log(req.files.images,"req.files.images 273");
                        if(req.files.images){
                            fileService.uploadImage(req.files.images, function (err, data) {
                                allData.images = data.Location;
                                callback(null, data.Location);
                            })
                          }
                        else{
                            callback(null, null);   
                        }
                     },
            
                    
                   ], function (err, result) {
                         if (err) {
                        consoler.err(err);
                        return;
                    } else {
                        matOne.create(allData, function (err, data) {
                         console.log(this.allData, "this.alldata");
                              if (err) {
                                console.log('Error in Saving user: ' + err);
                            } else {
                                console.log('User send succesful', allData);
                              res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                            }  });  }  });
            },
            userMaterialsGet:async (req, res, next) => {
            const { ClientsId } = req.params;
            const users = await matOne.find({profestionalForMaterials:ClientsId})
            res.status(200).json(users);   
            },
              
      
        // ****************** NewBatchOne ****************
      

        userNewBatchOnePosts: async (req, res, next) => {
 
            const { ClientsId } = req.params;
            req.body.profestionalForNewBatchesOne = ClientsId
            let allData = req.body 
        
            async.parallel([
                 function (callback) {
                    console.log(req.files.demoLectureImg,"req.files.images 273");
                    if(req.files.demoLectureImg){
                        fileService.uploadImage(req.files.demoLectureImg, function (err, data) {
                            allData.demoLectureImg = data.Location;
                            callback(null, data.Location);
                        })
                      }
                    else{
                        callback(null, null);   
                    }
                 },
        
                
               ], function (err, result) {
                     if (err) {
                    consoler.err(err);
                    return;
                } else {
                    newBatchOne.create(allData, function (err, data) {
                     console.log(this.allData, "this.alldata");
                          if (err) {
                            console.log('Error in Saving user: ' + err);
                        } else {
                            console.log('User send succesful', allData);
                          res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                        }  });  }  });
        },


        userNewBatchOneGet:async (req, res, next) => {
        const { ClientsId } = req.params;
        const users = await newBatchOne.find({profestionalForNewBatchesOne:ClientsId})
        res.status(200).json(users);   
        },    
        
        // ****************** AdminProcess ****************
        
            userAdminProcessPosts : async (req,res) => {
                const { ClientsId } = req.params;
                req.body.profestionalForAdmissionProcess = ClientsId
                const generalcomm = new admiss(req.body);
                await generalcomm.save();
                res.status(201).json(generalcomm);
                },
            userAdminProcessGet:async (req, res, next) => {
            const { ClientsId } = req.params;
            const users = await admiss.find({profestionalForAdmissionProcess:ClientsId})
            res.status(200).json(users);   
            },
            // ****************** SubjectPrority ****************
            userSubjectProrityPosts : async (req,res) => {
            const { ClientsId } = req.params;
            req.body.profestionalForSubjectSpecilizationPrority = ClientsId
            const generalcomm = new subjectPrority(req.body);
            await generalcomm.save();
            res.status(201).json(generalcomm);
            },
            userSubjectProrityGet:async (req, res, next) => {
            const { ClientsId } = req.params;
            const users = await subjectPrority.find({profestionalForSubjectSpecilizationPrority:ClientsId})
            res.status(200).json(users);   
            }, 

            

                 // ****************** FeeStructure Two****************
                 userFeeStructureTwoPosts : async (req,res) => {
                const { ClientsId } = req.params;
                req.body.profestionalForFreeStructureTwo = ClientsId
                const generalcomm = new freeTwo(req.body);
                await generalcomm.save();
                res.status(201).json(generalcomm);
                },
                userFeeStructureTwoGet:async (req, res, next) => {
                const { ClientsId } = req.params;
                const users = await freeTwo.find({profestionalForFreeStructureTwo:ClientsId})
                res.status(200).json(users);   
                },
                
                 // ******************AgeAttempts****************
                 userAgeAttemptsPosts : async (req,res) => {
                    const { ClientsId } = req.params;
                    req.body.profestionalForAgeAttempts = ClientsId
                    const generalcomm = new ages(req.body);
                    await generalcomm.save();
                    res.status(201).json(generalcomm);
                    },
                    userAgeAttemptsGet:async (req, res, next) => {
                    const { ClientsId } = req.params;
                    const users = await ages.find({profestionalForAgeAttempts:ClientsId})
                    res.status(200).json(users);   
                    },
                      // ******************batches****************
                      professionalBatchPosts : async (req,res) => {
                    const { ClientsId } = req.params;
                    req.body.profestionalInstForCreatingBatches = ClientsId
                    const generalcomm = new batch(req.body);
                    await generalcomm.save();
                    res.status(201).json(generalcomm);
                    },
                    professionalBatchGet:async (req, res, next) => {
                    const { ClientsId } = req.params;
                    const users = await batch.find({profestionalInstForCreatingBatches:ClientsId})
                    res.status(200).json(users);   
                    },
                    // ******************testAnalysis****************
                    


                        usertestAnalysisPosts: async (req, res, next) => {
 
            const { ClientsId } = req.params;
            req.body.profestionalInstForTestAnaysisTwo = ClientsId
            let allData = req.body 
        
            async.parallel([
                 function (callback) {
                    console.log(req.files.studentImg,"req.files.images 273");
                    if(req.files.studentImg){
                        fileService.uploadImage(req.files.studentImg, function (err, data) {
                            allData.studentImg = data.Location;
                            callback(null, data.Location);
                        })
                      }
                    else{
                        callback(null, null);   
                    }
                 },
               ], function (err, result) {
                     if (err) {
                    consoler.err(err);
                    return;
                } else {
                    testAn.create(allData, function (err, data) {
                     console.log(this.allData, "this.alldata");
                          if (err) {
                            console.log('Error in Saving user: ' + err);
                        } else {
                            console.log('User send succesful', allData);
                          res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                        }  });  }  });
        },

                        usertestAnalysisGet:async (req, res, next) => {
                        const { ClientsId } = req.params;
                        const users = await testAn.find({profestionalInstForTestAnaysisTwo:ClientsId})
                        res.status(200).json(users);   
                        },
                        // ******************trackRecord****************
                     
                        professionaltrackRecordPost: async (req, res, next) => {
 
                            const { ClientsId } = req.params;
                            req.body.profestionalInstForTrackRecords = ClientsId
                            let allData = req.body 
                        
                            async.parallel([
                                 function (callback) {
                                    console.log(req.files.studentImg,"req.files.images 273");
                                    if(req.files.studentImg){
                                        fileService.uploadImage(req.files.studentImg, function (err, data) {
                                            allData.studentImg = data.Location;
                                            callback(null, data.Location);
                                        })
                                      }
                                    else{
                                        callback(null, null);   
                                    }
                                 },
                               ], function (err, result) {
                                     if (err) {
                                    consoler.err(err);
                                    return;
                                } else {
                                    trackRecord.create(allData, function (err, data) {
                                     console.log(this.allData, "this.alldata");
                                          if (err) {
                                            console.log('Error in Saving user: ' + err);
                                        } else {
                                            console.log('User send succesful', allData);
                                          res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                                        }  });  }  });
                        },
                
                        professionaltrackRecordGet:async (req, res, next) => {
                        const { ClientsId } = req.params;
                        const users = await trackRecord.find({profestionalInstForTrackRecords:ClientsId})
                        res.status(200).json(users);   
                        },


                        // ****************** facultyDetails ****************
      

                        professionalfacultyDetailsPost: async (req, res, next) => {
 
            const { ClientsId } = req.params;
            req.body.profestionalInstForFacultyDetails = ClientsId
            let allData = req.body 
        
            async.parallel([
                 function (callback) {
                    console.log(req.files.facultyImg,"req.files.images 273");
                    if(req.files.facultyImg){
                        fileService.uploadImage(req.files.facultyImg, function (err, data) {
                            allData.facultyImg = data.Location;
                            callback(null, data.Location);
                        })
                      }
                    else{
                        callback(null, null);   
                    }
                 },
              ], function (err, result) {
                     if (err) {
                    consoler.err(err);
                    return;
                } else {
                    newBatchOne.create(allData, function (err, data) {
                     console.log(this.allData, "this.alldata");
                          if (err) {
                            console.log('Error in Saving user: ' + err);
                        } else {
                            console.log('User send succesful', allData);
                          res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                        }  });  }  });
        },


        professionalfacultyDetailsGet:async (req, res, next) => {
        const { ClientsId } = req.params;
        const users = await newBatchOne.find({profestionalInstForFacultyDetails:ClientsId})
        res.status(200).json(users);   
        },    
          // ****************** computerLab ****************
      

          userComputerLabPosts: async (req, res, next) => {
 
            const { ClientsId } = req.params;
            req.body.profestionalInstForComputerLab = ClientsId
            let allData = req.body 
        
            async.parallel([
                 function (callback) {
                    console.log(req.files.computerLabImg,"req.files.images 273");
                    if(req.files.computerLabImg){
                        fileService.uploadImage(req.files.computerLabImg, function (err, data) {
                            allData.computerLabImg = data.Location;
                            callback(null, data.Location);
                        })
                      }
                    else{
                        callback(null, null);   
                    }
                 },
               ], function (err, result) {
                     if (err) {
                    consoler.err(err);
                    return;
                } else {
                    computerLab.create(allData, function (err, data) {
                     console.log(this.allData, "this.alldata");
                          if (err) {
                            console.log('Error in Saving user: ' + err);
                        } else {
                            console.log('User send succesful', allData);
                          res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                        }  });  }  });
        },


        userComputerLabGet:async (req, res, next) => {
        const { ClientsId } = req.params;
        const users = await computerLab.find({profestionalInstForComputerLab:ClientsId})
        res.status(200).json(users);   
        },    

            // ****************** liraryOne ****************
            professionalliraryOnePosts : async (req,res) => {
            const { ClientsId } = req.params;
            req.body.profestionalInstForLibraryTypeOne = ClientsId
            const generalcomm = new librararyOne(req.body);
            await generalcomm.save();
            res.status(201).json(generalcomm);
            },
            professionalliraryOneBatchGet:async (req, res, next) => {
            const { ClientsId } = req.params;
            const users = await librararyOne.find({profestionalInstForLibraryTypeOne:ClientsId})
            res.status(200).json(users);   
            }, 

                // ****************** Lirary Two ****************
                professionalliraryTwoPost : async (req,res) => {
                const { ClientsId } = req.params;
                req.body.profestionalInstForLibrary = ClientsId
                const generalcomm = new librarary(req.body);
                await generalcomm.save();
                res.status(201).json(generalcomm);
                },
                
                professionalliraryTwoGet:async (req, res, next) => {
                const { ClientsId } = req.params;
                const users = await librarary.find({profestionalInstForLibrary:ClientsId})
                res.status(200).json(users);   
                }, 
           
// ****************** liraryThree ****************
      

userliraryThreePosts: async (req, res, next) => {
 
            const { ClientsId } = req.params;
            req.body.profestionalInstForLibraryThree = ClientsId
            let allData = req.body 
        
            async.parallel([
                 function (callback) {
                    console.log(req.files.booksImg,"req.files.images 273");
                    if(req.files.booksImg){
                        fileService.uploadImage(req.files.booksImg, function (err, data) {
                            allData.booksImg = data.Location;
                            callback(null, data.Location);
                        })
                      }
                    else{
                        callback(null, null);   
                    }
                 },
               ], function (err, result) {
                     if (err) {
                    consoler.err(err);
                    return;
                } else {
                    libThree.create(allData, function (err, data) {
                     console.log(this.allData, "this.alldata");
                          if (err) {
                            console.log('Error in Saving user: ' + err);
                        } else {
                            console.log('User send succesful', allData);
                          res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                        }  });  }  });
        },


        userliraryThreeGet:async (req, res, next) => {
        const { ClientsId } = req.params;
        const users = await libThree.find({profestionalInstForLibraryThree:ClientsId})
        res.status(200).json(users);   
        },  
        
        

    // *************************************AddsOne*****************
 

    babycareAddsOneP: async (req, res, next) => {
 
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsOnea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImg, function (err, data) {
                    allData.addsOneImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOne.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOne.find({babycareAreaForAddsOnea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOne.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsTwo*****************
     
     
    babycareAddsTwoP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsTwoa = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImg, function (err, data) {
                    allData.addsTwoImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwo.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
    babycareAddsTwoGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwo.find({babycareAreaForAddsTwoa:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwo.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsThree*****************
    
    
    babycareAddsThreeP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsThreea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImg, function (err, data) {
                    allData.addsThreeImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThree.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThree.find({babycareAreaForAddsThreea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThree.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsFour*****************
    
     
     
    babycareAddsFourP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsFoura = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImg, function (err, data) {
                    allData.addsFourImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFour.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFour.find({babycareAreaForAddsFoura:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFour.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
     
    // *************************************AddsOneLoc*****************
    
    
    babycareAddsOneLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocOne = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImgLoc, function (err, data) {
                    allData.addsOneImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOneLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOneLoc.find({babycaresAreaForAddsLocOne:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOneLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsTwoLoc*****************
    
    
    babycareAddsTwoLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocTwo = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImgLoc, function (err, data) {
                    allData.addsTwoImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwoLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsTwoLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwoLoc.find({babycaresAreaForAddsLocTwo:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwoLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsThreeLoc*****************
    
    
    babycareAddsThreeLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocThree = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImgLoc, function (err, data) {
                    allData.addsThreeImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThreeLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThreeLoc.find({babycaresAreaForAddsLocThree:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThreeLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFourLoc*****************
    
    
    babycareAddsFourLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFour = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImgLoc, function (err, data) {
                    allData.addsFourImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFourLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFourLoc.find({babycaresAreaForAddsLocFour:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFourLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFiveLoc*****************
    
    
    babycareAddsFiveLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFive = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFiveImgLoc, function (err, data) {
                    allData.addsFiveImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFiveLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFiveLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFiveLoc.find({babycaresAreaForAddsLocFive:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFiveDeleteLoc: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFiveLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    }, 
    }
    