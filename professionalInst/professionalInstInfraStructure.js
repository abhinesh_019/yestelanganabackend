const mongoose=require('mongoose');

const profestionalInstSchemaInfraStructure= new mongoose.Schema({

    descriptionsOne:{type:String},
    descriptionsTwo:{type:String},
    images:{type:String},
    title:{type:String},

    profestionalInstAboutUsForInfraStructure:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstClient'
}
    
},
{timestamps:true});
const profestionalInstInfraStructure=mongoose.model('profestionalInstInfraStructure',profestionalInstSchemaInfraStructure);

module.exports=profestionalInstInfraStructure;

