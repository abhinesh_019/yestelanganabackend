const mongoose=require('mongoose');

const profestionalInstSchemaAreas= new mongoose.Schema({

    mainTitle:{type:String},
     descriptionsOne:{type:String},
     descriptionsTwo:{type:String},
    importantNote:{type:String},
  

    profestionalInsClientsForOurPhilosophy:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstClient'
}
    
},
{timestamps:true});
const profestionalInstArea=mongoose.model('profestionalInstOurPhilosophy',profestionalInstSchemaAreas);

module.exports=profestionalInstArea;

