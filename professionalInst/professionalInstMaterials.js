const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    mainTitle:{type:String},
     descriptions:{type:String},
     subtitle:{type:String}, 
     images:{type:String},
      
    profestionalForMaterials:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBranches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstMaterials',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

