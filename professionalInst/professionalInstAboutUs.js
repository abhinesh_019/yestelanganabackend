const mongoose=require('mongoose');

const profestionalInstSchemaAreas= new mongoose.Schema({

    aboutUsDescriptionsOne:{type:String},
    aboutUsDescriptionsTwo:{type:String},
    managmentTeams:{type:String},
    managmentTeamsDecriptionsOne:{type:String},
    managmentTeamsDecriptionsTwo:{type:String},
    visonMissionTitle:{type:String},
    visonMissionDescriptionsOne:{type:String},
    visonMissionDescriptionsTwo:{type:String},
    managmentTeams1:{type:String},
    managmentTeams2:{type:String},
    managmentTeams3:{type:String},
    managmentTeams4:{type:String},
    managmentTeamImg1:{type:String},
    managmentTeamImg2:{type:String},
    managmentTeamImg3:{type:String},
    managmentTeamImg4:{type:String},
    visonMissionImg:{type:String},
    awardsAndRewardsDescriptions:{type:String},
   


    profestionalInstLocationsForAboutUs:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstClient'
}
    
},
{timestamps:true});
const profestionalInstArea=mongoose.model('profestionalInstAboutUs',profestionalInstSchemaAreas);

module.exports=profestionalInstArea;

