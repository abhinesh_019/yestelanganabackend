const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    demoCode:{type:String},
    demoTiming:{type:String},
    demoName:{type:String},
    demoDate:{type:String},
    BatchDuration:{type:String},
    EndDate:{type:String},
     locations:{type:String},
     demoLectureName:{type:String},
     demoLectureIntro:{type:String},
     demoLectureImg:{type:String},

    profestionalForNewBatchesOne:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBranches'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstNewBatchesOne',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

