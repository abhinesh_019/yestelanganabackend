const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    organizationName:{type:String},
    noOfAttempts:{type:String},
    ageLtms:{type:String},
    basicQualificationalDetails:{type:String},
    descriptions1:{type:String},
    descriptions2:{type:String},
    descriptions3:{type:String},
    descriptions4:{type:String},

    profestionalInstForImportantOrganizations:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstBranches'
        
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstImpOrganizations',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

