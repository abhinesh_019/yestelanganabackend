const mongoose=require('mongoose');

const profestionalInstSchemaAreas= new mongoose.Schema({

    mainTitle:{type:String},
    name:{type:String},
    aboutDirector:{type:String},
    importantNote:{type:String},
    directorImg:{type:String},
 

    profestionalInstLocationsForBoardOfDirectors:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstClient'
}
    
},
{timestamps:true});
const profestionalInstArea=mongoose.model('profestionalInstBoardDirectors',profestionalInstSchemaAreas);

module.exports=profestionalInstArea;

