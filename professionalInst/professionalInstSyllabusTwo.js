const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

      syllabusDescriptions1:{type:String},
     syllabusDescriptions2:{type:String},
     syllabusDescriptions3:{type:String},
     
    profestionalInsForsubjectSyllabusTwo:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstSubJectSyllabus'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstSubJectSyllabusTwo',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

