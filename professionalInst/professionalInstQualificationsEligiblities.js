const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    year:{type:String},
    OrganizingInstitute:{type:String},
    basicQualifications:{type:String},
    descriptions:{type:String},

    profestionalInstCatTwoForQualificationalEligiblities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstTrackRecordsYear'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstOrgorQualificationsDetails',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

