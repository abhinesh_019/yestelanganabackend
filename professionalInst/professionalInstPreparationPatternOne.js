const mongoose=require('mongoose');

const profestionalInstSchemaEligibilities= new mongoose.Schema({

    mainTitle:{type:String},
    subTitle1:{type:String},
    descriptions1:{type:String},
    descriptions2:{type:String},
    descriptions3:{type:String},

    profestionalInstForPreparationPattern:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstTrackRecordsYear'
}
    
},
{timestamps:true});
const profestionalInstEligibilities=mongoose.model('profestionalInstPreparationPattern',profestionalInstSchemaEligibilities);

module.exports=profestionalInstEligibilities;

