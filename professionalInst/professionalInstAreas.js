const mongoose=require('mongoose');

const profestionalInstSchemaAreas= new mongoose.Schema({

    subArea:String,
    
    profestionalInstLocationsForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'profestionalInstLocations'
}
    
},
{timestamps:true});
const profestionalInstArea=mongoose.model('profestionalInstAreas',profestionalInstSchemaAreas);

module.exports=profestionalInstArea;

