const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    tclassname:String,
    tclassDescription:String,
    trainnerName:String,
    yearsOfExp:String,
    Shifts:String,
    ShiftsTimmingsFrom:String,
    ShiftsTimmingsTo:String,
    images:String,

    bpUserDataForTraining:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'beautyParloursClient'
    }
},
{timestamps:true});
const comm=mongoose.model('beautyParlourTraining',commentsSchema);
module.exports=comm;