const mongoose=require('mongoose');
const  beautyParlourSchema= new mongoose.Schema(
    {       
            bpCatageries:{type:String},
            parlourName:{type:String},
            aboutUsDescription1:{type:String},
            aboutUsDescription2:{type:String},
             name:{type:String},
             day1:{type:String},
             day2:{type:String},
             day3:{type:String},
             day4:{type:String},
             day5:{type:String},
             day6:{type:String},
             day7:{type:String},
             allTimes:{type:String},
             houseNo:{type:String},
             mainArea:{type:String},
             subArea:{type:String},
             landmark:{type:String},
             city:{type:String},
             distict:{type:String},
             state:{type:String},
             pincode:{type:String},
             officeNo:{type:String},
             mobileNo:{type:String},
             whatsupno:{type:String},
             emailId:{type:String},
             latitude:{type:String},
             longitude:{type:String},
             images1:{type:String},
             images2:{type:String},
             images3:{type:String},
             images4:{type:String},
             images5:{type:String},

             bpArea:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'beautyparlourAreas'
     }

},

{timestamps:true}

);

const BeautyParlourClients=mongoose.model('beautyParloursClient',beautyParlourSchema);
module.exports=BeautyParlourClients;