const mongoose=require('mongoose');

const parlourSchemaAreas= new mongoose.Schema({

    area:String,
    
    beautyparlourLocation:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'beautyparlourLocations'
}
    
},
{timestamps:true});

const parlourArea=mongoose.model('beautyparlourAreas',parlourSchemaAreas);
module.exports=parlourArea;