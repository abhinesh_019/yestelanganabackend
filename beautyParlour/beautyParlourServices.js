const mongoose=require('mongoose');
 
const  beautyParlourSchema= new mongoose.Schema(
    {
        images:{type:String},
        servicesName:{type:String},
        descriptions:{type:String},

             bpClient:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'beautyParloursClient'
     }
},
{timestamps:true}
);
const BeautyParlourClients=mongoose.model('beautyParlourServices',beautyParlourSchema);
module.exports=BeautyParlourClients;