const mongoose=require('mongoose');
 
const  beautyParlourEquipmentsSchema= new mongoose.Schema(
    {
        equipnments:{type:String},
        usages:{type:String},
        description:{type:String},
        images:{type:String},

        
        bpClientForEquipments:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'beautyParloursClient'
     }
},
{timestamps:true}
);
const Equipments=mongoose.model('beautyParlourEquipments',beautyParlourEquipmentsSchema);
module.exports=Equipments;
