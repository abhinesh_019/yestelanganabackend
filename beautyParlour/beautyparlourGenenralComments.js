const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,

    bpUserDataForGeneralComments:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'beautyParloursClient'
    }
},
{timestamps:true});
const comm=mongoose.model('beautyParlourGeneralComments',commentsSchema);
module.exports=comm;