const express= require('express');
var router = express.Router();
const userControllers = require('../beautyParlour/beautyparlourController');
var middleware = require('../middleware/middleware')

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  ************ categories ***********
router.route('/parlourCategories')
.get(userControllers.parloursCategoriesget)
router.route('/parlourCategoriesp')
 .post(multipartMiddleware,userControllers.parlourCategoriesposts);

 router.route('/parlourCategorie/:parloursCategoriesId')
 .get(userControllers.parloueCategoriesidget)
//  ***********************

router.route('/parlourlocations/:parlourscatId')
.get(userControllers.parlourlocationsget)
router.route('/parlourlocationsp/:parlourscatId')

 .post(userControllers.parlourlocationspost);

 router.route('/beautyParlourAreas/:parlourlocationsId')
 .get(userControllers.parlourAreasget)
  .post(userControllers.parlourAreasposts);


// ******************* Clients details ******************
router.route('/bpClientsget/:bpAreaId')
.get(userControllers.index)

router.route('/bpClientsgetId/:bpClientsId')
.get(userControllers.clientsId)

router.route('/bpClientsgetCounts/:bpAreaId')
.get(userControllers.bpusersgetcoutarea)
 
router.route('/bpClientsgetAreaCounts/:bpAreaId') 
.get(userControllers.bpAreaCount)

router.route('/bpUsersLocationsMens') 
.get(userControllers.bpcountLocationsMens)

router.route('/bpUsersLocationsWomens') 
.get(userControllers.bpcountLocationsWomens)


router.route('/bpClientsgetLocationsCatmens')
.get(userControllers.indexs)
 


router.route('/bpClients/:bpAreaId')
.post(multipartMiddleware,userControllers.newUser);
// *******************************************************

// ******************* Clients services details ******************
router.route('/clientServices/:bpClientId')
.get(userControllers.servicesGet)

router.route('/clientServicesCounts/:bpClientId')
.get(userControllers.servicesGetCounts)
 
router.route('/bpClientsService/:bpClientsId')
.post(multipartMiddleware,userControllers.newClientService);
// *******************************************************


// ******************* Clients Equipments details ******************
router.route('/beaautyParloureEquipmentsGet/:clientId')
.get(userControllers.Equipmentsget)
router.route('/beaautyParlourEquipmentsGetCounts/:clientId')
.get(userControllers.EquipmentsgetCounts)
router.route('/beaautyParloureEquipmentsPost/:clientId')
.post(multipartMiddleware,userControllers.Equipmentspost);
// *******************************************************

// ******************* Clients Updates details ******************
router.route('/bpupdates/:bpClientsId')
.get(userControllers.updatesGet)

router.route('/updatesCount/:bpClientsId')
.get(userControllers.updatesGetCount)
 
router.route('/updatesPostes/:bpClientsId')
.post(multipartMiddleware,userControllers.updatesPost);
// *******************************************************

// ******************* Clients Updates Comments details ******************
router.route('/bpupdatesComments/:bpClientsUpdatesId')
.get(userControllers.updatesCommentsGet)

router.route('/updatesCommentsCount/:bpClientsUpdatesId')
.get(userControllers.updatesCommentsGetCount)
 
router.route('/updatesCommentsPostes/:bpClientsUpdatesId')
.post(userControllers.updatesCommentsPost);
// *******************************************************

// ******************* Clients Updates Comments Reply details ******************
router.route('/bpupdatesCommentsReply/:bpClientsUpdatesCommId')
.get(userControllers.updatesCommentsReplyGet)

router.route('/updatesCommentsReplyCount/:bpClientsUpdatesCommId')
.get(userControllers.updatesCommentsReplyGetCount)
 
router.route('/updatesPostesCommentsReply/:bpClientsUpdatesCommId')
.post(userControllers.updatesCommentsReplyPost);
// *******************************************************


// ******************* Clients Training Classes details ******************
router.route('/bpTrainingGet/:bpClientsId')
.get(userControllers.trainingGet)

router.route('/bpTrainingGetCounts/:bpClientsId')
.get(userControllers.trainingGetCounts)
 
router.route('/bpTrainingPosts/:bpClientsId')
.post(multipartMiddleware,userControllers.trainingGetPosts);
// *******************************************************

// ******************* Clients General Comments details ******************
router.route('/bpGeneralCommentsGet/:bpClientsId')
.get(userControllers.generalComments)

router.route('/bpGeneralCommentsGetCounts/:bpClientsId')
.get(userControllers.generalCommentsCounts)
 
router.route('/bpGeneralCommentsPosts/:bpClientsId')
.post(userControllers.generalCommentsPosts);
// *******************************************************
// ******************* Clients Fecilities ******************
router.route('/beaautyParlourecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/beaautyParlourecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/beaautyParlourecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// ******************* TotalCustomers ******************
router.route('/beaautyParlourTotalCustomersGet/:clientId')
.get(userControllers.TotalCustomersget)
router.route('/beaautyParloureTotalCustomersCounts/:clientId')
.get(userControllers.TotalCustomersgetCounts)
router.route('/beaautyParlourTotalCustomersPost/:clientId')
.post(multipartMiddleware,userControllers.TotalCustomerspost);

// *************************************** AddsOne ************************************* 
router.route('/beutyParlourAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/beutyParlourAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/beutyParlourAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/beutyParlourAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/beutyParlourAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/beutyParlourAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/beutyParlourAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/beutyParlourAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/beutyParlourAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/beutyParlourAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/beutyParlourAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/beutyParlourAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 

// *************************************** AddsOne ************************************* 
router.route('/beutyParlourAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/beutyParlourAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/beutyParlourAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/beutyParlourAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/beutyParlourAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/beutyParlourAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/beutyParlourAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/beutyParlourAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/beutyParlourAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/beutyParlourAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/beutyParlourAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/beutyParlourAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/beutyParlourAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/beutyParlourAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/beutyParlourAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 
module.exports = router;