const express= require('express');
var router = express.Router();
const userControllers = require('../danceInstutite/danceController');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

 


//  ************ Locations ***********

router.route('/danceLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/areasDacnce/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/danceClientsget/:AreaId')
.get(userControllers.index)

router.route('/danceClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/danceClientsAllCount') 
.get(userControllers.totalclients)


router.route('/danceClientsCountAreas')  
.get(userControllers.tuserscountAreas)


router.route('/danceClientsCountDist')  
.get(userControllers.tuserscountDist)


router.route('/danceClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/danceClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/danceClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/dancefecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/dancefecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/dancefecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************
 
// *******************  catageries******************
router.route('/danceCatageriesGets/:clientId')
.get(userControllers.danceCatageriesget)
router.route('/danceCatageriesGetCount/:clientId')
.get(userControllers.danceCatageriesgetCounts)
router.route('/danceCatageriesPosts/:clientId')
.post(multipartMiddleware,userControllers.danceCatageriespost);

// *******************************************************

// ******************* Clients Services ******************
router.route('/danceServicesGet/:clientId')
.get(userControllers.servicesget)
router.route('/danceServicesGetCounts/:clientId')
.get(userControllers.servicesgetCounts)
router.route('/danceServicesPost/:clientId')
.post(userControllers.servicesPost);

// *******************************************************

// ******************* Clients Services Two ******************
router.route('/danceServicesTwoGet/:ClientsId')
.get(userControllers.UsersTwoGet)

router.route('/danceupdatesTwopostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdatesTwo);


// ****************** only updates details ****************
router.route('/danceupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/danceupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/danceupdatespostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/danceupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/danceupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/danceupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/danceupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/danceupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/daneceuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/daneceuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)


// *************************************** AddsOne ************************************* 
router.route('/danceAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/danceAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/danceAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/danceAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/danceAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/danceAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/danceAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/danceAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/danceAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/danceAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/danceAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/danceAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 

// *************************************** AddsOne ************************************* 
router.route('/danceAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/danceAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/danceAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/danceAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/danceAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/danceAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/danceAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/danceAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/danceAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/danceAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/danceAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/danceAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/danceAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/danceAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/danceAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 
module.exports = router;