const mongoose=require('mongoose');

const SchemaAreas= new mongoose.Schema({

    name:{type:String},
    importantsKey:{type:String},
    descriptions:{type:String},
    images:{type:String},
 
    ClientsForFecilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'danceClient'
}
    
},
{timestamps:true});
const Area=mongoose.model('danceFecilities',SchemaAreas);

module.exports=Area;

