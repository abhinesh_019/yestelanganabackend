const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'danceLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('danceArea',areaSchema);
module.exports=areas;