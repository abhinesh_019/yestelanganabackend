const express= require('express');
var router = express.Router();
const userControllers = require('../restaurants/restaurantsControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

 
//  ************ catatgeries for restarants  ***********

router.route('/restaurantsCatageries')
.get(userControllers.catageriesGet)
 .post(multipartMiddleware,userControllers.catageriesPost);

 router.route('/restaurantsCatageriesInd/:CategoriesId')
 .get(userControllers.catageriesGetInd)

//  ************ Locations ***********

router.route('/restaurantsLocations/:catId')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/restaurantsAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/restaurantsClientsget/:AreaId')
.get(userControllers.index)

router.route('/restaurantsClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/restaurantsClientsMainAreas/:AreaId')
.get(userControllers.usersgetcoutareaMain)


router.route('/restaurantsClientsAllCount') 
.get(userControllers.totalclients)

router.route('/restaurantsClientsCount/:AreaId')  
.get(userControllers.tuserscount)


router.route('/restaurantsClientsGetind/:clientsId')
.get(userControllers.indexs)

router.route('/restaurantsUsersLocations') 
.get(userControllers.restaurantscountLocations)
 
router.route('/restaurantsUsersCatageries') 
.get(userControllers.restaurantscountCatageries)

router.route('/restaurantsClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* Clients Fecilities ******************
router.route('/restaurantsfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/restaurantsfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/restaurantsfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************

//  ************ Restarants Food Types One***********
router.route('/restaurantsFoodType/:clientId')
.get(userControllers.foodTypeget)
router.route('/restaurantsFoodTypeInd/:clientId')
.get(userControllers.foodTypegetInd)

router.route('/restaurantsFoodTypeC/:clientId')
.get(userControllers.foodTypegetC)
 .post(multipartMiddleware,userControllers.foodTypePosts);


//  ***********************

//  ************ Restarants Food Types DISH ***********
router.route('/restaurantsFoodTypeDish/:clientId')
.get(userControllers.foodTypegetDish)
 .post(multipartMiddleware,userControllers.foodTypePostsDish);

//  ***********************
//  ************ Restarants Food Types Two***********
router.route('/restaurantsFoodTypeTwo/:clientId')
.get(userControllers.FoodTypegetTwo)
.post(multipartMiddleware,userControllers.FoodTypePostsTwo);

router.route('/restaurantsFoodTypeTwoC/:clientId')
.get(userControllers.FoodTypegetTwoC)


//  ***********************
// ******************* Clients Services ONE ******************
router.route('/restaurantsServicesGetOne/:clientId')
.get(userControllers.servicesgetOne)
router.route('/restaurantsServicesGetCountsOne/:clientId')
.get(userControllers.servicesgetCountsOne)
router.route('/restaurantsServicesPostOne/:clientId')
.post(multipartMiddleware,userControllers.servicesPostOne);

// *******************************************************
 
// ****************** only updates details ****************
router.route('/restaurantsupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/restaurantsupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/restaurantsupdatesgetpostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/restaurantsupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/restaurantsupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/restaurantsupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/restaurantsupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/restaurantsupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/restaurantsuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/restaurantsuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

// *************************************** AddsOne ************************************* 
router.route('/restaurantsAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/restaurantsAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/restaurantsAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/restaurantsAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/restaurantsAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/restaurantsAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/restaurantsAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/restaurantsAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/restaurantsAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/restaurantsAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/restaurantsAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/restaurantsAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/restaurantsAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/restaurantsAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/restaurantsAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/restaurantsAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/restaurantsAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/restaurantsAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/restaurantsAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/restaurantsAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/restaurantsAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/restaurantsAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/restaurantsAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/restaurantsAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/restaurantsAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/restaurantsAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/restaurantsAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
