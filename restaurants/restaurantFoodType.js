const mongoose=require('mongoose');
const SchemaService= new mongoose.Schema({

    foodType1:{type:String},
    images:{type:String},
    ClientsForFoodType:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'restaurantClients'
}
    
},
{timestamps:true});
const  Service=mongoose.model('restaurantFoodTypeOne',SchemaService);

module.exports=Service;

