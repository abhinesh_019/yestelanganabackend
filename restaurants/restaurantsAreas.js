const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    LocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'restaurantsLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('restaurantAreas',areaSchema);
module.exports=areas;