const mongoose=require('mongoose');
const SchemaService= new mongoose.Schema({

    foodName:{type:String},
    foodCat:{type:String},
     images:{type:String},
     foodQuantity:{type:String},
     foodPrice:{type:String},
     chefMasterName:{type:String},
     YearsOfExp:{type:String},
     foodDescriptions:{type:String},
     foodAdvantages:{type:String},

     restaurantFoodTypesTwoForServices:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'restaurantFoodTypesTwo'
}
    
},
{timestamps:true});

const  Service=mongoose.model('restaurantMainServices',SchemaService);

module.exports=Service;

