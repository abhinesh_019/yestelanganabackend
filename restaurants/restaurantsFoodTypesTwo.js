const mongoose=require('mongoose');
const SchemaService= new mongoose.Schema({
   
    foodType1:{type:String},
     images:{type:String},
     
    restaurantFoodTypeDishForFoodTypeTwo:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'restaurantFoodTypeDish'
}
    
},
{timestamps:true});
const  Service=mongoose.model('restaurantFoodTypesTwo',SchemaService);

module.exports=Service;

