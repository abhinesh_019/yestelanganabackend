const mongoose=require('mongoose');
const locationSchema= new mongoose.Schema({

    locations:String,

    catageriesForLocations:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'restaurantFoodCatageries'
}
},
{timestamps:true});
const locations=mongoose.model('restaurantLocations',locationSchema);
module.exports=locations;