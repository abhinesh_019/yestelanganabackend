const mongoose=require('mongoose');
const SchemaService= new mongoose.Schema({

    foodDish:{type:String},
    images:{type:String},
    
    ClientsForFoodTypeDish:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'restaurantFoodTypeOne'
}
    
},
{timestamps:true});
const  Service=mongoose.model('restaurantFoodTypeDish',SchemaService);

module.exports=Service;

