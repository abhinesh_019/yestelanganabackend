const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,

    clientsForOnlyComments:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'restaurantClients'
    }
},
{timestamps:true});
const comm=mongoose.model('restaurantOnlyComments',commentsSchema);
module.exports=comm;