const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    fecilitiesName:String,
    fecilitiesImg:String,
    fecilitiesDescriptions:String,

    hostelsClientsForFecilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hostelsClients'
}
   
},
{timestamps:true});
const areas=mongoose.model('hostelsFecilities',areaSchema);
module.exports=areas;