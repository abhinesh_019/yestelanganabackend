const mongoose=require('mongoose');

const SchemaCommenlsOnly= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,
 
    hostelsClientsForCommenlsOnly:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hostelClients'
}
    
},
{timestamps:true});
const   commenlsOnly=mongoose.model('hostelsOnlyComments',SchemaCommenlsOnly);

module.exports=commenlsOnly;

