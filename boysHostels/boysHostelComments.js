const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,

    boysHostelDetails:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'boysHostel'
    }
});
const comm=mongoose.model('comment',commentsSchema);
module.exports=comm;