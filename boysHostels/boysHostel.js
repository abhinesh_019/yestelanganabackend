const mongoose=require('mongoose');
 
const HostelSchema= new mongoose.Schema(
    {
      
    name:{type:String},
    catageries:{type:String},
    shopname:{type:String},
      aboutusdescriptionone:{type:String},
      aboutusdescriptiontwo:{type:String},
      informationtopublic:{type:String},
      mon:{type:String},
      tue:{type:String},
      wed:{type:String},
      thu:{type:String},
      fri:{type:String},
      sat:{type:String},
      sun:{type:String},
      totaltiming:{type:String},
      hno:{type:String},
      area:{type:String},
      landmark:{type:String},
       city:{type:String},
      distict:{type:String},
      state:{type:String},
      pincode:{type:String},
      officeno:{type:String},
      mobileno:{type:String},
      whatsappno:{type:String},
      mainArea:{type:String},
      latitude:{type:String},
      longitude:{type:String},
      email:{type:String},
      images1:{type:String},
      images2:{type:String},
      images3:{type:String},
      images4:{type:String},
      images5:{type:String},
            
            hostelsAreasForClients:{
                type:mongoose.Schema.Types.ObjectId,
                ref:'hostelsArea'
        }
});

const BoysHostel=mongoose.model('hostelClients',HostelSchema);
module.exports=BoysHostel;