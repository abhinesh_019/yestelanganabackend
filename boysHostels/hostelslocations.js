const mongoose=require('mongoose');

const hostelsLocationsSchema= new mongoose.Schema({

    locations:String,
   
    hostelsCatagerie:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hostelCategories'
}
},
{timestamps:true});

const hostelsLocation=mongoose.model('hostelsLocations',hostelsLocationsSchema);
module.exports=hostelsLocation;