
const mongoose=require('mongoose');

const SchemaCUpdatesComments= new mongoose.Schema({
 
    descriptions:String,
   
    hostelsClientsForUpdatesComments:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hostelsUpdates'
}
    
},
{timestamps:true});
const   updatesComments=mongoose.model('hostelsUpdatesComments',SchemaCUpdatesComments);

module.exports=updatesComments;

