var boysHostels = require('../boysHostels/boysHostel')
var boysHostelCat = require('../boysHostels/hostelsCatageries')
var boysHostelsL = require('../boysHostels/hostelslocations')
var hostelA = require('../boysHostels/hostelsArea')
var comment = require('../boysHostels/boysHostelComments')
const fileService = require('../services/file-upload');
var clientsUpdates=require('../boysHostels/hostelsUpdates');
var clientsUpdatesComments=require('../boysHostels/hostelsUpdatesComments');
var clientsUpdatesCommentsReply=require('../boysHostels/hostelsUpdatesCommentsReply');
var clientsOnlyComments=require('../boysHostels/hostelsOnlyComments');
var fecilities=require('../boysHostels/hostelsFecilities');
var babycaresAddsOne=require('../boysHostels/boysHostelsAddsOnea');
var babycaresAddsTwo=require('../boysHostels/boysHostelsAddsTwoa');
var babycaresAddsThree=require('../boysHostels/boysHostelsAddsThreea');
var babycaresAddsFour=require('../boysHostels/boysHostelsAddsFoura');
 
var babycaresAddsOneLoc=require('../boysHostels/boysHostelsAddsOnel');
var babycaresAddsTwoLoc=require('../boysHostels/boysHostelsAddsTwol');
var babycaresAddsThreeLoc=require('../boysHostels/boysHostelsAddsThreel');
var babycaresAddsFourLoc=require('../boysHostels/boysHostelsAddsFourl');
var babycaresAddsFiveLoc=require('../boysHostels/boysHostelsAddsFivel');
var async = require('async');
var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 var nodemailer = require('../helpers/nodemailer');

module.exports = {
// ********************categories*****************
hostelsCategoriesposts:(req, res, next) => {

    let allData = req.body;
  
    async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },
    ], function (err, result) {
        if (err) {
            consoler.err(err);
            return;
        } else {
            boysHostelCat.create(allData, function (err, data) {
                console.log(this.allData, "this.alldata");
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
     });
},
 
hostelsCategoriesget: async(req,res,next)=>{
    const users = await boysHostelCat.find({})
       res.status(200).json(users);
   console.log(users,"users");
},
hostelsCategoriesidget:async (req, res, next) => {
    const { hostelsCategoriesId } = req.params;
     const usersposts = await boysHostelCat.findById(hostelsCategoriesId)
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},


// ******************************************

hostelslocationspost: async(req,res,next)=>{
    const { hostelCatId } = req.params;
    req.body.hostelsCatagerie = hostelCatId
     const userscomm = new boysHostelsL(req.body);
    await userscomm.save();
    console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
    
       res.status(201).json(userscomm);
},

hostelslocationsget:async (req, res, next) => {

    const { hostelCatId } = req.params;
    

    const usersposts = await boysHostelsL.find({hostelsCatagerie:hostelCatId}).sort({"area":-1})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},
    hostelsAreasposts : async(req,res,next)=>{
        const { hostelsLocationsId } = req.params;
        req.body.hostelsLocation = hostelsLocationsId
         const userscomm = new hostelA(req.body);
        await userscomm.save();
        console.log(userscomm,"userscomm userscomm userscomm userscommuserscomm 264");
        
           res.status(201).json(userscomm);
  },

   hostelsAreasget:async (req, res, next) => {
        const { hostelsLocationsId } = req.params;
         const usersposts = await hostelA.find({hostelsLocation:hostelsLocationsId}).sort({"area":-1})
        res.status(200).json(usersposts);
         console.log(usersposts,"usersposts");
    },

 
    // get data full (done)
   
    index:async (req, res, next) => {

        const { hostelsAreaId } = req.params;
        let search=req.query.search;

        const usersposts = await boysHostels.find({hostelsAreasForClients:hostelsAreaId,$or:[
            {area:new RegExp(search, "gi")},
            {pincode:new RegExp(search, "gi")}, 
            {shopname:new RegExp(search, "gi")},
            {place:new RegExp(search, "gi")},
        ]
    }).sort({"area":-1})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    indexind:async (req, res, next) => {
        const { hostelsAreaId } = req.params;
        const usersposts = await boysHostels.findById(hostelsAreaId) 
        res.status(200).json(usersposts);
        console.log(usersposts,"usersposts");
    },
    hostelCountSubAreas: async (req, res, next) => {
        const { hostelsAreaId } = req.params;
    
        const users = await boysHostels.aggregate([{$match:{hostelsAreasForClients:ObjectId(hostelsAreaId)}},
    
            {"$group" : {_id:{area:"$area"},count:{$sum:1}}},{$sort:{"area":1}}
         ])
        
        res.status(200).json(users);
       console.log("users****", users);
    
    },
     hostelCountDist: async (req, res, next) => {
        const users = await boysHostels.aggregate([ 
            
              {"$group" : {_id:{ distict:"$distict",hostelCat:"$catageries"},count:{$sum:1}}}
        ]).sort({"distict":1})
         res.status(200).json(users);
   },
    // ************************* get data full (done)*************************

 

    newUser: async (req, res, next) => {
 
        const { hostelsAreaId } = req.params;
        req.body.hostelsAreasForClients = hostelsAreaId
      
        let allData = req.body
 
        async.parallel([
    
            function (callback) {
    
                fileService.uploadImage(req.files.images1, function (err, data) {
                    allData.images1 = data.Location;
                    callback(null, data.Location);
                })
            },
    
            function (callback) {
                fileService.uploadImage(req.files.images2, function (err, data) {
                    allData.images2 = data.Location;
                    callback(null, data.Location);
                })
            },
            function (callback) {
                fileService.uploadImage(req.files.images3, function (err, data) {
                    allData.images3 = data.Location;
                    callback(null, data.Location);
                })
            },
            function (callback) {
                fileService.uploadImage(req.files.images4, function (err, data) {
                    allData.images4 = data.Location;
                    callback(null, data.Location);
                })
            },
            function (callback) {
                fileService.uploadImage(req.files.images5, function (err, data) {
                    allData.images5 = data.Location;
                    callback(null, data.Location);
                })
            },
     
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                boysHostels.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
    
            console.log(allData, "obj,result-----------------------------------");
    
        });
    },

    //  ************************** get element by ID done*****************************************************

    getUser: (req, res, next) => {
        console.log(req.params);

        const { boysHostelId } = req.params;
 
        boysHostels.findById(boysHostelId).then((doc) => {
             res.status(200).json(doc);

        }).catch((err) => {
            console.log("haii", err)
        });

    },

    //  *************************** update element by ID done*****************************************************

    replaceUser: async (req, res, next) => {
        const { boysHostelId } = req.params;
        const newUser = req.body;
        console.log('user id is', boysHostelId);
         const result = await boysHostels.findByIdAndUpdate(boysHostelId, newUser).then((result)=>{
            console.log('userBoysHostelsDetails', result);
            res.status(200).json({ success: true, result })


        }).catch((err) => {
            console.log("error in replace users admins", err)
        });
    },

    //  *************************** delete element by ID done*****************************************************
    deleteUsers: async (req, res, next) => {
        const { boysHostelId } = req.params;

        const result = await boysHostels.findByIdAndRemove(boysHostelId);

        res.status(200).json(result)
    },

    //  ***************************hostel get comments by ID done*****************************************************

    getIDcomments: async (req, res, next) => {
        const { boysHostelId } = req.params;
        const comment = await boysHostels.findById(boysHostelId).populate('comment')
        .then((comment)=>{
            console.log(comment, "userscomments ,userscomments  userscomments  userscomments  line  no 187");
            res.status(200).json(comment);

        }).catch((err) => {
         });
        },

    // *********************post hostel comments *******************
    newIDcomments: async (req, res, next) => {
        console.log("new cpmemt", req.params)

        const { boysHostelId } = req.params;

        //create a comment
        req.body.boysHostelId = boysHostelId
        const userscomm = new comment(req.body);

        await userscomm.save();
console.log(userscomm,"this is line no 208 userscomm-------------------");

        res.status(201).json(userscomm);
    },


    // ****************users updates*************

UsersUpdates: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.hostelsClientsForUpdates = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.viedoes,"req.files.viedoes 302");
            if(req.files.viedoes){
                fileService.uploadImage(req.files.viedoes, function (err, data) {
                    allData.viedoes = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clientsUpdates.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

UsersGet:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({hostelsClientsForUpdates:ClientsId}).sort({"createdAt:":-1})

        res.status(200).json(users);
       console.log("users****", users);
},

UsersGetCounts:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({hostelsClientsForUpdates:ClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
//admin users updates comments
updatesCommentsposts:async (req, res, next) => {

const { updatesId } = req.params;
  req.body.hostelsClientsForUpdatesComments = updatesId 
const userscomm = new clientsUpdatesComments(req.body);

await userscomm.save();
  res.status(201).json(userscomm);
},

updatesCommentsGet:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({hostelsClientsForUpdatesComments:updatesId})
    res.status(200).json(users); 
},
updatesCommentsGetcounts:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({hostelsClientsForUpdatesComments:updatesId}).count()
    res.status(200).json(users);
},

updatesCommentsReplyposts:async (req, res, next) => {
const { updatesCommId } = req.params;
  req.body.hostelsClientsForUpdatesCommentsReply = updatesCommId 
const userscomm = new clientsUpdatesCommentsReply(req.body);
await userscomm.save();
 res.status(201).json(userscomm);
},

updatesCommentsReplyGet:async (req, res, next) => {

const { updatesCommId } = req.params;

    const users = await clientsUpdatesCommentsReply.find({hostelsClientsForUpdatesCommentsReply:updatesCommId})

    res.status(200).json(users);
   console.log("users**** 399", users);
},

// ****************only comments *******************
usersCommentsposts : async (req,res) => {
const { ClientsId } = req.params;
req.body.hostelsClientsForCommenlsOnly = ClientsId
const generalcomm = new clientsOnlyComments(req.body);
await generalcomm.save();
res.status(201).json(generalcomm);
},
usersCommentsget:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await clientsOnlyComments.find({hostelsClientsForCommenlsOnly:ClientsId})
res.status(200).json(users);   
},
usersCommentsgetCounts:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await clientsOnlyComments.find({hostelsClientsForCommenlsOnly:ClientsId}).count()
    res.status(200).json(users);   
    },
// ************************************************ Clients fecilities ***************************************
 
hostelspost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.hostelsClientsForFecilities = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.fecilitiesImg, function (err, data) {
                allData.fecilitiesImg = data.Location;
                callback(null, data.Location);
            })
        },
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            fecilities.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
hostelsget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await fecilities.find({hostelsClientsForFecilities:clientId}).sort({"importantsKey":-1})
    res.status(200).json(usersposts);
 },
 hostelsgetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await fecilities.find({hostelsClientsForFecilities:clientId}).count()
    res.status(200).json(usersposts);
 },
// *************************************AddsOne*****************
 

babycareAddsOneP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycareAreaForAddsOnea = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsOneImg, function (err, data) {
                allData.addsOneImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsOne.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsOneGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsOne.find({babycareAreaForAddsOnea:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsOneDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsOne.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},

// *************************************AddsTwo*****************
 
 
babycareAddsTwoP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycareAreaForAddsTwoa = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsTwoImg, function (err, data) {
                allData.addsTwoImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsTwo.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

babycareAddsTwoGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsTwo.find({babycareAreaForAddsTwoa:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsTwoDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsTwo.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},

// *************************************AddsThree*****************


babycareAddsThreeP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycareAreaForAddsThreea = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsThreeImg, function (err, data) {
                allData.addsThreeImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsThree.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsThreeGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsThree.find({babycareAreaForAddsThreea:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsThreeDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsThree.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},

// *************************************AddsFour*****************

 
 
babycareAddsFourP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycareAreaForAddsFoura = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsFourImg, function (err, data) {
                allData.addsFourImg = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsFour.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsFourGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsFour.find({babycareAreaForAddsFoura:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsFourDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsFour.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},

 
// *************************************AddsOneLoc*****************


babycareAddsOneLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocOne = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsOneImgLoc, function (err, data) {
                allData.addsOneImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsOneLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsOneLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsOneLoc.find({babycaresAreaForAddsLocOne:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsOneLocDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsOneLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},
// *************************************AddsTwoLoc*****************


babycareAddsTwoLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocTwo = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsTwoImgLoc, function (err, data) {
                allData.addsTwoImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsTwoLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsTwoLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsTwoLoc.find({babycaresAreaForAddsLocTwo:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsTwoLocDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsTwoLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},
// *************************************AddsThreeLoc*****************


babycareAddsThreeLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocThree = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsThreeImgLoc, function (err, data) {
                allData.addsThreeImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsThreeLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsThreeLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsThreeLoc.find({babycaresAreaForAddsLocThree:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsThreeLocDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsThreeLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},
// *************************************AddsFourLoc*****************


babycareAddsFourLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocFour = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsFourImgLoc, function (err, data) {
                allData.addsFourImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsFourLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsFourLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsFourLoc.find({babycaresAreaForAddsLocFour:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsFourLocDelete: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsFourLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
},
// *************************************AddsFiveLoc*****************


babycareAddsFiveLocP: async (req, res, next) => {
 
    const { beutyParlourAreaId } = req.params;
    req.body.babycaresAreaForAddsLocFive = beutyParlourAreaId
 
 
    let allData = req.body
   async.parallel([
        function (callback) {
            fileService.uploadImage(req.files.addsFiveImgLoc, function (err, data) {
                allData.addsFiveImgLoc = data.Location;
                callback(null, data.Location);
            })
        },
      
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            babycaresAddsFiveLoc.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }
 });
},

 
babycareAddsFiveLocGet:async (req, res, next) => {

    const { beutyParlourAreaId } = req.params;
 
    const usersposts = await babycaresAddsFiveLoc.find({babycaresAreaForAddsLocFive:beutyParlourAreaId})
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},

babycareAddsFiveDeleteLoc: async (req, res, next) => {
    const { beutyParlourAreaId } = req.params;

    const result = await babycaresAddsFiveLoc.findByIdAndRemove(beutyParlourAreaId);

    res.status(200).json(result)
}, 
}








