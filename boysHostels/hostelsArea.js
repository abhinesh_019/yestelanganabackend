const mongoose=require('mongoose');

const hostelsSchemaAreas= new mongoose.Schema({

    area:String,
    
    hostelsLocation:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hostelsLocations'
}
    
},
{timestamps:true});

const hostelsArea=mongoose.model('hostelsArea',hostelsSchemaAreas);
module.exports=hostelsArea;