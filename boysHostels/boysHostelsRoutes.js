const express= require('express');
var router = express.Router();
const userControllers = require('../boysHostels/boysHostelControllers');
// const multer = require('multer');
// var nodemailer=require('../helpers/nodemailer');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
 


//  ************ categories ***********
router.route('/hostelsCategorie')
.get(userControllers.hostelsCategoriesget)
router.route('/hostelsCategoriesp')
 .post(multipartMiddleware,userControllers.hostelsCategoriesposts);

 router.route('/hostelsCategories/:hostelsCategoriesId')
 .get(userControllers.hostelsCategoriesidget)


//  ***********************

router.route('/hostelsLocations/:hostelCatId')
.get(userControllers.hostelslocationsget)
 .post(userControllers.hostelslocationspost);

 router.route('/hostelsAreas/:hostelsLocationsId')
 .get(userControllers.hostelsAreasget)
  .post(userControllers.hostelsAreasposts);


// *********** clients ***************
router.route('/boysHostelsg/:hostelsAreaId')
.get(userControllers.index)

router.route('/boysHostelsgInd/:hostelsAreaId')
.get(userControllers.indexind)

router.route('/boysHostelsCountsDisticts')
.get(userControllers.hostelCountDist)
router.route('/boysHostelsCountsAreas/:hostelsAreaId')
.get(userControllers.hostelCountSubAreas)
router.route('/boysHostelsp/:hostelsAreaId')
.post(multipartMiddleware,userControllers.newUser);

// ************************************************
 
router.route('/boysHostels/:boysHostelId')
.get(userControllers.getUser)
.put(userControllers.replaceUser)
.delete(userControllers.deleteUsers)

router.route('/:boysHostelId/comments')
.get(userControllers.getIDcomments)
.post(userControllers.newIDcomments)

// ****************** only updates details ****************
router.route('/hostelsupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/hostelsupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/hostelsupdatespostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/hostelsupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/hostelsupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/hostelsupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/hostelsupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/hostelsupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/hostelseuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/hostelseuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)

  // ******************* Clients Fecilities ******************
  
  
  router.route('/hostelsfecitiesGet/:clientId')
  .get(userControllers.hostelsget)
  

  router.route('/hostelsfecitiesGetCounts/:clientId')
  .get(userControllers.hostelsgetCounts)
  

  router.route('/hostelsfecitiesPost/:clientId')
  .post(multipartMiddleware,userControllers.hostelspost);
  

  // *******************************************************

// *************************************** AddsOne ************************************* 
router.route('/hostelsAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/hostelsAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/hostelsAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/hostelsAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/hostelsAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/hostelsAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/hostelsAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/hostelsAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/hostelsAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/hostelsAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/hostelsAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/hostelsAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 

// *************************************** AddsOne ************************************* 
router.route('/hostelsAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/hostelsAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/hostelsAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/hostelsAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/hostelsAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/hostelsAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/hostelsAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/hostelsAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/hostelsAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/hostelsAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/hostelsAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/hostelsAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/hostelsAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/hostelsAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/hostelsAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 
module.exports = router;