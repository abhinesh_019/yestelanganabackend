const mongoose=require('mongoose');

const SchemaCUpdatesCommentsReply= new mongoose.Schema({
 
    descriptions:String,
   
    hostelsClientsForUpdatesCommentsReply:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'hostelsUpdatesComments'
}
    
},
{timestamps:true});
const   updatesCommentsReply=mongoose.model('hostelsUpdatesCommentsReply',SchemaCUpdatesCommentsReply);

module.exports=updatesCommentsReply;

