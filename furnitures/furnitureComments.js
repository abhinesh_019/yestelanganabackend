const mongoose=require('mongoose');
const commentsSchema= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,

    furnitureUserData:{
             type:mongoose.Schema.Types.ObjectId,
             ref:'furnituresusersdata'
    }
},
{timestamps:true});
const comm=mongoose.model('furnitureComments',commentsSchema);
module.exports=comm;