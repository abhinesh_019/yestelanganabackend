const mongoose=require('mongoose');

const furnituresSchemaAreas= new mongoose.Schema({

    area:String,
    
    furnitureLocations:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'furnitureslocations'
}
    
},
{timestamps:true});

const furnitures=mongoose.model('furnituresAreas',furnituresSchemaAreas);
module.exports=furnitures;