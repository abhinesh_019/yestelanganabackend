const express= require('express');
var router = express.Router();
const userControllers = require('../furnitures/furnituresControllers');
var middleware = require('../middleware/middleware')

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
// *************************************** locations ************************************* 

router.route('/furnitureslocations')
.get(userControllers.furnitureslocationsget)
 .post(userControllers.furnitureslocationsposts);

 // *************************************** areas ************************************* 

 router.route('/furnituresAreas/:furnituresAreasId')
 .get(userControllers.furnituresAreasget)
  .post(userControllers.furnituresAreasposts);
 
  router.route('/furnituresAreasCount/:furnituresAreasId')
 .get(userControllers.furnituresAreasgetCount)

// *************************************** users ************************************* 
   router.route('/furnituresusers/:furnituresArea')
 .get(userControllers.furnituresusersget)
 
 router.route('/furnituresusersCountsByArea/:furnituresArea')
 .get(userControllers.furnituresusersgetCountsByArea)
 
 router.route('/furnituresCountAreas')  
 .get(userControllers.tuserscountAreas)

 router.route('/furnituresClientsCountDist')  
.get(userControllers.tuserscountDist)

router.route('/furnitures/:furnitureusersonly')
 .get(userControllers.furnituresusersgetonly)

router.route('/furnituresuserstotcout')
 .get(userControllers.furnituresusersgettotcout)

 router.route('/furnituresuserscountarea/:furnitureslocationId')
 .get(userControllers.furnituresusersgetcoutarea)

router.route('/furnituresuserscount/:furnitureslocationId')
.get(userControllers.furnituresusersgetcount)

   router.route('/furnituresuserspostsdata/:furnituresArea')
   .post(multipartMiddleware,userControllers.furnituresuserspost);

// **************************************************************************** 

// ****************** only comments ****************
router.route('/furnituresuserscomments/:furnitureslocationId')
.get(userControllers.furnituresusersCommentsget)
.post(userControllers.furnituresusersCommentsposts);

router.route('/furnituresuserscommentsCounts/:furnitureslocationId')
.get(userControllers.furnituresusersCommentsgetCounts)

// ****************** furnitureTypes ****************

router.route('/furnitureTypesGet/:furnitureslocationId')
.get(userControllers.furnitureTypesget)

router.route('/furnitureTypesCounts/:furnitureslocationId')
.get(userControllers.furnitureTypescounts)

router.route('/furnitureTypespostss/:furnitureslocationId')
.post(userControllers.furnitureTypesposts);



// ****************** only product details ****************
router.route('/furnituresproducts/:furnitureslocationId')
.get(userControllers.furnituresProductsget)

router.route('/furnituresproductsCounts/:furnitureslocationId')
.get(userControllers.furnituresProductsgetcounts)

router.route('/furnituresproductspostss/:furnitureslocationId')
.post(multipartMiddleware,userControllers.furnituresProductsposts);

// ****************** only updates details ****************
router.route('/furnituresupdatesget/:furnitureslocationId')
.get(userControllers.furnituresUsersGet)

router.route('/furnituresUpdatesGetCounts/:furnitureslocationId')
.get(userControllers.furnituresUsersGetCounts)

router.route('/furnituresupdatespostsdata/:furnituresUserId')
.post(multipartMiddleware,userControllers.furnituresUsersUpdates);

 
//updates comments

router.route('/furnituresupdatesCommentsget/:furnituresupdates')
.get(userControllers.furnituresupdatesCommentsGet)
router.route('/furnituresupdatesCommentsgetcounts/:furnituresupdates')
.get(userControllers.furnituresupdatesCommentsGetcounts)

router.route('/furnituresupdatesCommentspost/:furnituresupdates')
.post(userControllers.furnituresupdatesCommentsposts)

router.route('/furnituresupdatesCommentReplysPost/:furnituresupdates')
.post(userControllers.furnituresupdatesCommentsReplyposts)

 router.route('/furnituresupdatesCommentReplysGet/:furnituresupdates')
.get(userControllers.furnituresupdatesCommentsReplyGet)

 
 
// *******************************************************
 
 
  
 // ******************* Clients Fecilities ******************
router.route('/furnituresCustomerscitiesGets/:clientId')
.get(userControllers.fecilitiesget)
router.route('/furnituresCustomerscitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/furnituresCustomerscitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);


// *************************************** AddsOne ************************************* 
router.route('/furnituresAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/furnituresAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/furnituresAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/furnituresAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/furnituresAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/furnituresAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/furnituresAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/furnituresAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/furnituresAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/furnituresAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/furnituresAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/furnituresAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/furnituresAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/furnituresAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/furnituresAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/furnituresAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/furnituresAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/furnituresAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/furnituresAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/furnituresAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/furnituresAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/furnituresAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/furnituresAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/furnituresAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/furnituresAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/furnituresAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/furnituresAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
