const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    fightsLocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'fightsLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('fightsAreas',areaSchema);
module.exports=areas;