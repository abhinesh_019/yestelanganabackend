const express= require('express');
var router = express.Router();
const userControllers = require('../fights/fightsControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

 

//  ************ categories ***********
router.route('/fightsCategories')
.get(userControllers.fightssCategoriesget)
router.route('/fightsCategoriesp')
 .post(multipartMiddleware,userControllers.fightsCategoriesposts);

 router.route('/fightsCategorie/:fightssCategoriesId')
 .get(userControllers.fightsCategoriesidget)
//  ***********************
//  ************ Locations ***********

router.route('/fightsLocations/:fightsscatId')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/fightsAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/fightsClientsget/:fightsAreaId')
.get(userControllers.index)

router.route('/fightsClientsgetId/:fightsClientsId')
.get(userControllers.clientsId)

router.route('/fightsClientsgetCounts/:fightsAreaId')
.get(userControllers.fightsusersgetcoutarea)
 
router.route('/fightsClientscatAll') 
.get(userControllers.fightscatAll)

router.route('/fightsClientsgetAreaCounts/:fightsAreaId') 
.get(userControllers.fightsAreaCount)

router.route('/fightsUsersLocations') 
.get(userControllers.fightscountLocations)
 
 

router.route('/fightsClients/:fightsAreaId')
.post(multipartMiddleware,userControllers.newUser);
// *******************************************************

 // ******************* Clients services ******************
router.route('/fightServicesget/:clientsId')
.get(userControllers.ServicesGet)
 
router.route('/fightServicesCntget/:clientsId')
.get(userControllers.ServicesCntGet)
router.route('/fightsServicesClients/:clientsId')
.post(multipartMiddleware,userControllers.ServicesPost);
// *******************************************************
  // ******************* Clients Fecilities ******************
router.route('/fightsfecitiesGet/:clientId')
.get(userControllers.fecilitiesget)
router.route('/fightsfecitiesGetCounts/:clientId')
.get(userControllers.fecilitiesgetCounts)
router.route('/fightsfecitiesPost/:clientId')
.post(multipartMiddleware,userControllers.fecilitiespost);

// *******************************************************
// ****************** only updates details ****************
router.route('/fightsupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/fightsupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/fightsupdatespostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);



//updates comments and reply 

router.route('/fightsupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/fightsupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/fightsupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/fightsupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/fightsupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/fightseuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/fightseuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)


// *************************************** AddsOne ************************************* 
router.route('/fightsAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/fightsAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/fightsAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/fightsAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/fightsAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/fightsAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/fightsAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/fightsAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/fightsAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/fightsAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/fightsAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/fightsAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 

// *************************************** AddsOne ************************************* 
router.route('/fightsAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/fightsAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/fightsAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/fightsAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/fightsAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/fightsAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/fightsAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/fightsAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/fightsAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/fightsAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/fightsAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/fightsAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/fightsAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/fightsAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/fightsAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;