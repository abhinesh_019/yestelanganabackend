var fc = require('../fights/fightsCatageriesSelf')
var locations = require('../fights/fightsLocations')
var areas = require('../fights/fightsArea')
var clients = require('../fights/fightsClients')
var services = require('../fights/fightsServices')
var fecilities = require('../fights/fightsFecilities')
 var clientsUpdates = require('../fights/fightsUpdates')
var clientsUpdatesComments = require('../fights/fightsUpdatesComments')
var clientsUpdatesCommentsReply = require('../fights/fightsupdatesCommentsReply')
var clientsOnlyComments = require('../fights/fightsOnlyComments')
const fileService = require('../fights/fightsServer');

var babycaresAddsOne=require('../fights/fightsAddsOnea');
var babycaresAddsTwo=require('../fights/fightsAddsTwoa');
var babycaresAddsThree=require('../fights/fightsAddsThreea');
var babycaresAddsFour=require('../fights/fightsAddsFoura');
 
var babycaresAddsOneLoc=require('../fights/fightsAddsOnel');
var babycaresAddsTwoLoc=require('../fights/fightsAddsTwol');
var babycaresAddsThreeLoc=require('../fights/fightsAddsThreel');
var babycaresAddsFourLoc=require('../fights/fightsAddsFourl');
var babycaresAddsFiveLoc=require('../fights/fightsAddsFivel');

var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
 
var async = require('async');
  
module.exports = {

// ********************categories*****************
fightsCategoriesposts:(req, res, next) => {

    let allData = req.body;
   
    async.parallel([

        function (callback) {

            fileService.uploadImage(req.files.images, function (err, data) {
                allData.images = data.Location;
                callback(null, data.Location);
            })
        },

 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            fc.create(allData, function (err, data) {

                console.log(this.allData, "this.alldata");


                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
        console.log(allData, "obj,result-----------------------------------");

    });
},
 
fightssCategoriesget: async(req,res,next)=>{
    const users = await fc.find({})
       res.status(200).json(users);

   console.log(users,"users");
},
fightsCategoriesidget:async (req, res, next) => {

    const { fightssCategoriesId } = req.params;
    

    const usersposts = await fc.findById(fightssCategoriesId)
    res.status(200).json(usersposts);
 
    console.log(usersposts,"usersposts");
},


// ******************************************

locationspost: async(req,res,next)=>{
        const { fightsscatId } = req.params;
        req.body.fightsCatageriesForLocations = fightsscatId
         const userscomm = new locations(req.body);
        await userscomm.save();
         
           res.status(201).json(userscomm);
  },
 
  locationsget:async (req, res, next) => {

        const { fightsscatId } = req.params;
        

        const usersposts = await locations.find({fightsCatageriesForLocations:fightsscatId}).sort({"locations":1})
        res.status(200).json(usersposts);
     
     },

 
// *********************** Areas *******************

Areasposts : async(req,res,next)=>{
    const { LocationsId } = req.params;
    req.body.fightsLocationForAreas = LocationsId
     const userscomm = new areas(req.body);
    await userscomm.save();
        res.status(201).json(userscomm);
},
 
Areasget:async (req, res, next) => {
     const { LocationsId } = req.params;
     const usersposts = await areas.find({fightsLocationForAreas:LocationsId}).sort({"area":-1})
    res.status(200).json(usersposts);
 },

// ******************************************
// ************************* users details *************************

newUser: (req, res, next) => {

        
    const { fightsAreaId } = req.params;
    req.body.fightssAreaForClients = fightsAreaId
    let allData = req.body
 
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.images1, function (err, data) {
                allData.images1 = data.Location;
                callback(null, data.Location);
            })
        },

        function (callback) {
            fileService.uploadImage(req.files.images2, function (err, data) {
                allData.images2 = data.Location;
                callback(null, data.Location);
            })
        },

        function (callback) {
            fileService.uploadImage(req.files.images3, function (err, data) {
                allData.images3 = data.Location;
                callback(null, data.Location);
            })
        },

        function (callback) {
            fileService.uploadImage(req.files.images4, function (err, data) {
                allData.images4 = data.Location;
                callback(null, data.Location);
            })
        },
 
        function (callback) {
            fileService.uploadImage(req.files.images5, function (err, data) {
                allData.images5 = data.Location;
                callback(null, data.Location);
            })
        },
        function (callback) {
            fileService.uploadImage(req.files.images6, function (err, data) {
                allData.images6 = data.Location;
                callback(null, data.Location);
            })
        },

    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            allData.role = "admin"
            clients.create(allData, function (err, data) {

                console.log(this.allData, "this.alldata");


                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
 
                    res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }

            });
        }


    });
},

index:async (req, res, next) => {

    const { fightsAreaId } = req.params;
    let search=req.query.search;

    const usersposts = await clients.find({fightssAreaForClients:fightsAreaId,$or:[
        {subArea:new RegExp(search, "gi")},
        {pincode:new RegExp(search, "gi")}, 
        {fightsName:new RegExp(search, "gi")},
        {landmark:new RegExp(search, "gi")},
        {city:new RegExp(search, "gi")},
     ]
}).sort({"subArea":1})
    res.status(200).json(usersposts);
 
 },

 
 
clientsId:async (req, res, next) => {
    const { fightsClientsId } = req.params;
    const usersposts = await clients.findById(fightsClientsId) 
    res.status(200).json(usersposts);
    console.log(usersposts,"usersposts");
},
fightsusersgetcoutarea:async (req, res, next) => {

    const { fightsAreaId } = req.params;
 
    const usersposts = await clients.find({fightssAreaForClients:fightsAreaId}).count()
    res.status(200).json(usersposts);
 
 },


 fightsAreaCount: async (req, res, next) => {
    const { fightsAreaId } = req.params;

    const users = await clients.aggregate([{$match:{fightssAreaForClients:ObjectId(fightsAreaId)}},

        {"$group" : {_id:{subArea:"$subArea"},count:{$sum:1}}},{$sort:{"subArea":1}}
     ])
    
    res.status(200).json(users);
   console.log("users****", users);

},
fightscatAll: async (req, res, next) => {
    const { fightsAreaId } = req.params;

    const users = await clients.aggregate([

        {"$group" : {_id:{fightsCat:"$fightsCat"},count:{$sum:1}}},{$sort:{"fightsCat":1}}
     ])
    
    res.status(200).json(users);
   console.log("users****", users);

},
fightscountLocations: async (req, res, next) => {
     const users = await clients.aggregate([ 
         
           {"$group" : {_id:{ distict:"$distict",fightsCat:"$fightsCat"},count:{$sum:1}}}
     ]).sort({"distict":1})
      res.status(200).json(users);
},
 

 
 

// ******************************************************************************************
// ******************** servicces *****************
ServicesPost:(req, res, next) => {

     const { clientsId } = req.params;
    req.body.fightsClientsForServices = clientsId
    let allData = req.body
    async.parallel([

        function (callback) {
            if(req.files.traineerImg){
               fileService.uploadImage(req.files.traineerImg, function (err, data) {
                   allData.traineerImg = data.Location;
                   callback(null, data.Location);
               })
             }
           else{
               callback(null, null);   
           }
           
       },
       function (callback) {
        if(req.files.fightTypeImg1){
           fileService.uploadImage(req.files.fightTypeImg1, function (err, data) {
               allData.fightTypeImg1 = data.Location;
               callback(null, data.Location);
           })
         }
       else{
           callback(null, null);   
       }
       
   },
   function (callback) {
    if(req.files.fightTypeImg2){
       fileService.uploadImage(req.files.fightTypeImg2, function (err, data) {
           allData.fightTypeImg2 = data.Location;
           callback(null, data.Location);
       })
     }
   else{
       callback(null, null);   
   }
   
},
function (callback) {
    if(req.files.fightTypeImg3){
       fileService.uploadImage(req.files.fightTypeImg3, function (err, data) {
           allData.fightTypeImg3 = data.Location;
           callback(null, data.Location);
       })
     }
   else{
       callback(null, null);   
   }
   
},
function (callback) {
    if(req.files.fightTypeImg4){
       fileService.uploadImage(req.files.fightTypeImg4, function (err, data) {
           allData.fightTypeImg4 = data.Location;
           callback(null, data.Location);
       })
     }
   else{
       callback(null, null);   
   }
   
},
function (callback) {
    if(req.files.fightTypeImg5){
       fileService.uploadImage(req.files.fightTypeImg5, function (err, data) {
           allData.fightTypeImg5 = data.Location;
           callback(null, data.Location);
       })
     }
   else{
       callback(null, null);   
   }
   
},

function (callback) {
    if(req.files.fightTypeImgOthers){
       fileService.uploadImage(req.files.fightTypeImgOthers, function (err, data) {
           allData.fightTypeImgOthers = data.Location;
           callback(null, data.Location);
       })
     }
   else{
       callback(null, null);   
   }
   
},
function (callback) {
    if(req.files.viedoes){
       fileService.uploadImage(req.files.viedoes, function (err, data) {
           allData.viedoes = data.Location;
           callback(null, data.Location);
       })
     }
   else{
       callback(null, null);   
   }
   
},
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            services.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
        console.log(allData, "obj,result-----------------------------------");

    });
},
 
ServicesGet:async (req, res, next) => {
    const { clientsId } = req.params;
    const usersposts = await services.find({fightsClientsForServices:clientsId}).sort({"createdAt":1})
   res.status(200).json(usersposts);
},
ServicesCntGet:async (req, res, next) => {
    const { clientsId } = req.params;
    const usersposts = await services.find({fightsClientsForServices:clientsId}).count()
   res.status(200).json(usersposts);
},

// ******************************************
// ************************************************ Clients fecilities ***************************************
 
fecilitiespost: (req, res, next) => {
    const { clientId } = req.params;
    req.body.fightsClientsForFecilities = clientId
    let allData = req.body
  
    async.parallel([
        function (callback) { 
            fileService.uploadImage(req.files.fecilitiesImg, function (err, data) {
                allData.fecilitiesImg = data.Location;
                callback(null, data.Location);
            })
        },
 ],function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            fecilities.create(allData, function (err, data) {
            if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                     res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 });
},
 
fecilitiesget:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await fecilities.find({fightsClientsForFecilities:clientId}).sort({"importantsKey":-1})
    res.status(200).json(usersposts);
 },
 fecilitiesgetCounts:async (req, res, next) => {

    const { clientId } = req.params;
    
    const usersposts = await fecilities.find({fightsClientsForFecilities:clientId}).count()
    res.status(200).json(usersposts);
 },
// ***********************************************************************
// ****************users updates*************

UsersUpdates: async (req, res, next) => {
 
    const { ClientsId } = req.params;
    req.body.fightssClientsForUpdates = ClientsId
    let allData = req.body 

    async.parallel([


        function (callback) {
            console.log(req.files.images,"req.files.images 273");
            if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        function (callback) {
            console.log(req.files.viedoes,"req.files.viedoes 302");
            if(req.files.viedoes){
                fileService.uploadImage(req.files.viedoes, function (err, data) {
                    allData.viedoes = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
          },
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            clientsUpdates.create(allData, function (err, data) {
             console.log(this.allData, "this.alldata");

                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},

UsersGet:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({fightssClientsForUpdates:ClientsId}).sort({"createdAt:":-1})

        res.status(200).json(users);
       console.log("users****", users);
},

UsersGetCounts:async (req, res, next) => {

    const { ClientsId } = req.params;

        const users = await clientsUpdates.find({fightssClientsForUpdates:ClientsId}).count()

        res.status(200).json(users);
       console.log("users****", users);
},
//admin users updates comments
updatesCommentsposts:async (req, res, next) => {

const { updatesId } = req.params;
  req.body.fightssClientsForUpdatesComments = updatesId 
const userscomm = new clientsUpdatesComments(req.body);

await userscomm.save();
  res.status(201).json(userscomm);
},

updatesCommentsGet:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({fightssClientsForUpdatesComments:updatesId})
    res.status(200).json(users); 
},
updatesCommentsGetcounts:async (req, res, next) => {
 const { updatesId } = req.params;
   const users = await clientsUpdatesComments.find({fightssClientsForUpdatesComments:updatesId}).count()
    res.status(200).json(users);
},

updatesCommentsReplyposts:async (req, res, next) => {
const { updatesCommId } = req.params;
  req.body.fightssClientsForUpdatesCommentsReply = updatesCommId 
const userscomm = new clientsUpdatesCommentsReply(req.body);
await userscomm.save();
 res.status(201).json(userscomm);
},

updatesCommentsReplyGet:async (req, res, next) => {

const { updatesCommId } = req.params;

    const users = await clientsUpdatesCommentsReply.find({fightssClientsForUpdatesCommentsReply:updatesCommId})

    res.status(200).json(users);
   console.log("users**** 399", users);
},

// ****************only comments *******************
usersCommentsposts : async (req,res) => {
const { ClientsId } = req.params;
req.body.fightssClientsForCommenlsOnly = ClientsId
const generalcomm = new clientsOnlyComments(req.body);
await generalcomm.save();
res.status(201).json(generalcomm);
},
usersCommentsget:async (req, res, next) => {
const { ClientsId } = req.params;
const users = await clientsOnlyComments.find({fightssClientsForCommenlsOnly:ClientsId})
res.status(200).json(users);   
},
usersCommentsgetCounts:async (req, res, next) => {
    const { ClientsId } = req.params;
    const users = await clientsOnlyComments.find({fightssClientsForCommenlsOnly:ClientsId}).count()
    res.status(200).json(users);   
    },


    // *************************************AddsOne*****************
 

    babycareAddsOneP: async (req, res, next) => {
 
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsOnea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImg, function (err, data) {
                    allData.addsOneImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOne.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOne.find({babycareAreaForAddsOnea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOne.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsTwo*****************
     
     
    babycareAddsTwoP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsTwoa = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImg, function (err, data) {
                    allData.addsTwoImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwo.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
    babycareAddsTwoGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwo.find({babycareAreaForAddsTwoa:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwo.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsThree*****************
    
    
    babycareAddsThreeP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsThreea = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImg, function (err, data) {
                    allData.addsThreeImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThree.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThree.find({babycareAreaForAddsThreea:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThree.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
    // *************************************AddsFour*****************
    
     
     
    babycareAddsFourP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycareAreaForAddsFoura = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImg, function (err, data) {
                    allData.addsFourImg = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFour.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFour.find({babycareAreaForAddsFoura:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFour.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    
     
    // *************************************AddsOneLoc*****************
    
    
    babycareAddsOneLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocOne = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsOneImgLoc, function (err, data) {
                    allData.addsOneImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsOneLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsOneLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsOneLoc.find({babycaresAreaForAddsLocOne:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsOneLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsOneLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsTwoLoc*****************
    
    
    babycareAddsTwoLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocTwo = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsTwoImgLoc, function (err, data) {
                    allData.addsTwoImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsTwoLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsTwoLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsTwoLoc.find({babycaresAreaForAddsLocTwo:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsTwoLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsTwoLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsThreeLoc*****************
    
    
    babycareAddsThreeLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocThree = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsThreeImgLoc, function (err, data) {
                    allData.addsThreeImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsThreeLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsThreeLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsThreeLoc.find({babycaresAreaForAddsLocThree:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsThreeLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsThreeLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFourLoc*****************
    
    
    babycareAddsFourLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFour = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFourImgLoc, function (err, data) {
                    allData.addsFourImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFourLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFourLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFourLoc.find({babycaresAreaForAddsLocFour:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFourLocDelete: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFourLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    },
    // *************************************AddsFiveLoc*****************
    
    
    babycareAddsFiveLocP: async (req, res, next) => {
     
        const { beutyParlourAreaId } = req.params;
        req.body.babycaresAreaForAddsLocFive = beutyParlourAreaId
     
     
        let allData = req.body
       async.parallel([
            function (callback) {
                fileService.uploadImage(req.files.addsFiveImgLoc, function (err, data) {
                    allData.addsFiveImgLoc = data.Location;
                    callback(null, data.Location);
                })
            },
          
        ], function (err, result) {
    
            if (err) {
                consoler.err(err);
                return;
            } else {
                babycaresAddsFiveLoc.create(allData, function (err, data) {
                 console.log(this.allData, "this.alldata");
    
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                    } else {
                        console.log('User send succesful', allData);
                      res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                    }
    
                });
            }
     });
    },
    
     
    babycareAddsFiveLocGet:async (req, res, next) => {
    
        const { beutyParlourAreaId } = req.params;
     
        const usersposts = await babycaresAddsFiveLoc.find({babycaresAreaForAddsLocFive:beutyParlourAreaId})
        res.status(200).json(usersposts);
     
        console.log(usersposts,"usersposts");
    },
    
    babycareAddsFiveDeleteLoc: async (req, res, next) => {
        const { beutyParlourAreaId } = req.params;
    
        const result = await babycaresAddsFiveLoc.findByIdAndRemove(beutyParlourAreaId);
    
        res.status(200).json(result)
    }, 
    }