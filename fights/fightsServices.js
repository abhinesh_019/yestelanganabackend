const mongoose=require('mongoose');

const SchemaCUpdates= new mongoose.Schema({

    optionalTypeName:String,
    fightType:String,
    aboutFightType:String,
    traineerName:String,
    traineerYearOfExp:String,
    shifts:String,
    timming:String,
    fightType1:String,
    fightType2:String,
    fightType3:String,
    fightType4:String,
    fightType5:String,
    fightTypeOthers:String,
    fightTypeImg1:String,
    fightTypeImg2:String,
    fightTypeImg3:String,
    fightTypeImg4:String,
    fightTypeImg5:String,
    fightTypeImgOthers:String,
    traineerImg:String,
    viedoes:String,
   
 
    fightsClientsForServices:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'fightssClients'
}
    
},
{timestamps:true});
const   updates=mongoose.model('fightServices',SchemaCUpdates);

module.exports=updates;

