const mongoose=require('mongoose');

const SchemaCUpdatesComments= new mongoose.Schema({
 
    descriptions:String,
   
    fightssClientsForUpdatesComments:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'fightssUpdates'
}
    
},
{timestamps:true});
const   updatesComments=mongoose.model('fightssUpdatesComments',SchemaCUpdatesComments);

module.exports=updatesComments;

