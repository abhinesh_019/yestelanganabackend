const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    area:String,

    swimmingLocationForAreas:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'swimmingLocations'
}
   
},
{timestamps:true});
const areas=mongoose.model('swimmingAreas',areaSchema);
module.exports=areas;