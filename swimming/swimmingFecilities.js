const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    fecilitiesName:String,
    fecilitiesImg:String,
    fecilitiesDescriptions:String,

    swimmingClientsForFecilities:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'swimmingsClients'
}
   
},
{timestamps:true});
const areas=mongoose.model('swimmingFecilities',areaSchema);
module.exports=areas;