const mongoose=require('mongoose');
const areaSchema= new mongoose.Schema({

    genders:String,
    gendersimages:String,
    coachName:String,
    coachYearsOfExp:String,
    coachImg:String,
    coachNo:String,
    poolsImg1:String,
    poolsImg2:String,
    poolsImg3:String,
    poolsImg4:String,
    poolsImg5:String,
    descriptions:String,

    swimmingClientsForCreatingGenders:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'swimmingsClients'
}
   
},
{timestamps:true});
const areas=mongoose.model('swimmingGenders',areaSchema);
module.exports=areas;