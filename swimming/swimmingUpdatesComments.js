const mongoose=require('mongoose');

const SchemaCUpdatesComments= new mongoose.Schema({
 
    descriptions:String,
   
    swimmingsClientsForUpdatesComments:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'swimmingsUpdates'
}
    
},
{timestamps:true});
const   updatesComments=mongoose.model('swimmingsUpdatesComments',SchemaCUpdatesComments);

module.exports=updatesComments;

