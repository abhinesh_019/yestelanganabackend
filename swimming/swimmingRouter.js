const express= require('express');
var router = express.Router();
const userControllers = require('../swimming/swimmingControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

 


//  ************ Locations ***********

router.route('/swimmingLocations')
.get(userControllers.locationsget)
 .post(userControllers.locationspost);

//  ***********************

//  ************ Area ***********
router.route('/swimmingAreas/:LocationsId')
.get(userControllers.Areasget)
 .post(userControllers.Areasposts);

//  ***********************

// ******************* Clients details ******************
router.route('/swimmingClientsget/:AreaId')
.get(userControllers.index)

router.route('/swimmingClientsgetAreaSingleCnt/:AreaId')
.get(userControllers.indexAs)

router.route('/swimmingClientsAreas/:AreaId')
.get(userControllers.usersgetcoutarea)
 
router.route('/swimmingClientsAllCount') 
.get(userControllers.totalclients)

router.route('/swimmingClientsAllCountLocations') 
.get(userControllers.totalclientsLocations)

router.route('/swimmingClientsAllCountMainArea/:AreaId') 
.get(userControllers.totalclientsArea)


router.route('/swimmingClientsGetind/:clientsId')
.get(userControllers.indexs)


router.route('/swimmingClientsPost/:AreaId')
.post(multipartMiddleware,userControllers.newUser);

// *******************************************************

// ******************* Clients CreatingGenders ******************

router.route('/swimmingCreatingGendersGet/:clientId')
.get(userControllers.gendersget)
router.route('/swimmingCreatingGendersGetCounts/:clientId')
.get(userControllers.gendersgetCounts)
router.route('/swimmingCreatingGendersPost/:clientId')
.post(multipartMiddleware,userControllers.gendersPost);

// *******************************************************
// ******************* Clients Accessaories ******************

router.route('/swimmingAccessaoriesGet/:clientId')
.get(userControllers.Accessaoriesget)
router.route('/swimmingAccessaoriesCounts/:clientId')
.get(userControllers.AccessaoriesCounts)
router.route('/swimmingAccessaoriesPost/:clientId')
.post(multipartMiddleware,userControllers.AccessaoriesPost);

// *******************************************************
// ******************* Clients Fecilities ******************

router.route('/swimmingFecilitiesGet/:clientId')
.get(userControllers.Fecilitiesget)
router.route('/swimmingFecilitiesCounts/:clientId')
.get(userControllers.FecilitiesCounts)
router.route('/swimmingFecilitiesPost/:clientId')
.post(multipartMiddleware,userControllers.FecilitiesPost);

// *******************************************************
// ****************** only updates details ****************
router.route('/swimmingsupdatesget/:ClientsId')
.get(userControllers.UsersGet)

router.route('/swimmingsupdatesgetCounts/:ClientsId')
.get(userControllers.UsersGetCounts)

router.route('/swimmingsupdatesgetpostsdata/:ClientsId')
.post(multipartMiddleware,userControllers.UsersUpdates);

//updates comments and reply 

router.route('/swimmingsupdatesCommentsget/:updatesId')
.get(userControllers.updatesCommentsGet)
router.route('/swimmingsupdatesCommentsgetcounts/:updatesId')
.get(userControllers.updatesCommentsGetcounts)

router.route('/swimmingsupdatesCommentspost/:updatesId')
.post(userControllers.updatesCommentsposts)

router.route('/swimmingsupdatesCommentReplysPost/:updatesCommId')
.post(userControllers.updatesCommentsReplyposts)

 router.route('/swimmingsupdatesCommentReplysGet/:updatesCommId')
.get(userControllers.updatesCommentsReplyGet)

// ****************** only comments ****************
router.route('/swimmingseuserscomments/:ClientsId')
.get(userControllers.usersCommentsget)
.post(userControllers.usersCommentsposts);

router.route('/swimmingsCeuserscommentsCounts/:ClientsId')
.get(userControllers.usersCommentsgetCounts)


// *************************************** AddsOne ************************************* 
router.route('/swimmingsAddsOneGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneGet)

router.route('/swimmingsAddsOneDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneDelete)
 
  router.route('/swimmingsAddsOnePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneP);
// **************************************************************************** 
// *************************************** AddsTwo ************************************* 
router.route('/swimmingsAddsTwoGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoGet)

router.route('/swimmingsAddsTwoDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoDelete)
 
  router.route('/swimmingsAddsTwoPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoP);
// **************************************************************************** 
// *************************************** AddsThree ************************************* 
router.route('/swimmingsAddsThreeGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeGet)

router.route('/swimmingsAddsThreeDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeDelete)
 
  router.route('/swimmingsAddsThreePtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeP);
// ****************************************************************************
// *************************************** AddsFour ************************************* 
router.route('/swimmingsAddsFourGeta/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourGet)

router.route('/swimmingsAddsFourDeletea/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourDelete)
 
  router.route('/swimmingsAddsFourPtsa/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourP);
// ****************************************************************************
 


// *************************************** AddsOne ************************************* 
router.route('/swimmingsAddsOneLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsOneLocGet)

router.route('/swimmingsAddsOneLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsOneLocDelete)
 
  router.route('/swimmingsAddsOneLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsOneLocP);
// **************************************************************************** 
// *************************************** AddsTwoLoc ************************************* 
router.route('/swimmingsAddsTwoLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsTwoLocGet)

router.route('/swimmingsAddsTwoLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsTwoLocDelete)
 
  router.route('/swimmingsAddsTwoLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsTwoLocP);
// **************************************************************************** 
// *************************************** AddsThreeLoc ************************************* 
router.route('/swimmingsAddsThreeGetLoc/:beutyParlourAreaId')
.get(userControllers.babycareAddsThreeLocGet)

router.route('/swimmingsAddsThreeDeleteLoc/:beutyParlourAreaId')
.delete(userControllers.babycareAddsThreeLocDelete)
 
  router.route('/swimmingsAddsThreePtsLoc/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsThreeLocP);
// ****************************************************************************
// *************************************** AddsFourLoc ************************************* 
router.route('/swimmingsAddsFourLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFourLocGet)

router.route('/swimmingsAddsFourLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFourLocDelete)
 
  router.route('/swimmingsAddsFourLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFourLocP);
// ****************************************************************************
// *************************************** AddsFiveLoc ************************************* 
router.route('/swimmingsAddsFiveLocGet/:beutyParlourAreaId')
.get(userControllers.babycareAddsFiveLocGet)

router.route('/swimmingsAddsFiveLocDelete/:beutyParlourAreaId')
.delete(userControllers.babycareAddsFiveDeleteLoc)
 
  router.route('/swimmingsAddsFiveLocPts/:beutyParlourAreaId')
  .post(multipartMiddleware,userControllers.babycareAddsFiveLocP);
// ****************************************************************************
 

module.exports = router;
