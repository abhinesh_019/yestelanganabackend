const mongoose=require('mongoose');

const SchemaCommenlsOnly= new mongoose.Schema({

    name:String,
    email:String,
    contactNo:String,
    leaveComment:String,
 
    swimmingsClientsForCommenlsOnly:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'swimmingsClients'
}
    
},
{timestamps:true});
const   commenlsOnly=mongoose.model('swimmingsOnlyComments',SchemaCommenlsOnly);

module.exports=commenlsOnly;

